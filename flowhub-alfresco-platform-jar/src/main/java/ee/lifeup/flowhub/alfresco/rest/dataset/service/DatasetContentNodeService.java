/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.service;

import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetRequest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.codehaus.jackson.JsonNode;

/**
 * Service for dataset content node.
 */
public interface DatasetContentNodeService {

    /**
     * Create dataset content node.
     *
     * @param datasetNodeRef dataset node reference.
     * @param datasetRequest dataset request.
     */
    NodeRef create(NodeRef datasetNodeRef, DatasetRequest datasetRequest);

    /**
     * Return value of property "data" of dataset content node.
     *
     * @param datasetNodeRef dataset node reference.
     */
    JsonNode getDataPropertyValue(NodeRef datasetNodeRef);

    /**
     * Update dataset content node.
     *
     * @param datasetNodeRef dataset node reference.
     * @param updateRequest  dataset request.
     */
    void update(NodeRef datasetNodeRef, DatasetRequest updateRequest);

    /**
     * Return dataset content node reference.
     *
     * @param datasetNodeRef dataset node reference.
     */
    NodeRef getDatasetContentNodeRef(NodeRef datasetNodeRef);
}
