/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.service;

import com.google.common.base.Strings;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.category.model.TopCategory;
import ee.lifeup.flowhub.alfresco.rest.category.service.FlowhubCategoryService;
import ee.lifeup.flowhub.alfresco.rest.scheme.mapper.SchemeMapper;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.*;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created by Aleksandr Koltakov on 05.07.2018
 */
@Service
public class SchemeServiceImpl extends AbstractServiceImpl implements SchemeService {

    private static final String SCHEMES_FOLDER_PATH = "Store/%s/Schemes";

    @Value("${flowhub.scheme.root.path}")
    private String schemeRootFolderPath;

    @Autowired
    private FlowhubCategoryService flowhubCategoryService;
    @Autowired
    private SchemeMapper schemeMapper;

    @Override
    public NodeRef create(SchemeCreationRequest request) {
        assertMandatoryParametersExists(request);
        String organizationName = request.getSchemeOrganization();
        String schemeId = request.getSchemeId();
        String schemeName = request.getSchemeName();

        log.info("We've got new scheme creation request [" + schemeId + ":" + schemeName + "], organization " + organizationName);

        Map<QName, Serializable> schemeAspectProperties = createProperties(request);

        NodeRef schemeRootNodeRef = getRootSchemeNodeRef();
        FileInfo fileInfo = fileFolderService.create(schemeRootNodeRef, schemeId, ContentModel.TYPE_FOLDER);
        NodeRef schemeNodeRef = fileInfo.getNodeRef();

        nodeService.addAspect(schemeNodeRef, LifeupContentModel.ASPECT_SCHEME, schemeAspectProperties);
        nodeService.addAspect(schemeNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);
        nodeService.addAspect(schemeNodeRef, ContentModel.ASPECT_GEN_CLASSIFIABLE, MapUtils.EMPTY_MAP);
        return schemeNodeRef;
    }

    private void assertMandatoryParametersExists(SchemeCreationRequest request) {
        if (StringUtils.isEmpty(request.getSchemeName())
                || StringUtils.isEmpty(request.getSchemeResponsible())
                || StringUtils.isEmpty(request.getSchemeIdFolderSeparator())
                || StringUtils.isEmpty(request.getSchemeIdDocumentSeparator())) {
            throw new IllegalArgumentException("Not all mandatory parameters specified");
        }
    }

    @Override
    public Scheme get(NodeRef nodeRef) {
        log.info("Going to read scheme with nodeRef " + nodeRef);
        assertSchemeExists(nodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        Scheme scheme = new Scheme();
        scheme.setNodeRef(nodeRef.toString());
        scheme.setSchemeId(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_ID));
        scheme.setSchemeName(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_NAME));
        scheme.setSchemeOrganization(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_ORGANIZATION));
        scheme.setSchemeDescription(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_DESCRIPTION));
        scheme.setSchemeResponsible(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_RESPONSIBLE));
        scheme.setSchemeIdFolderSeparator(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_ID_FOLDER_SEPARATOR));
        scheme.setSchemeIdDocumentSeparator(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_ID_DOCUMENT_SEPARATOR));
        scheme.setSchemeStatus(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_STATUS));
        scheme.setSchemeOpenDate(ParseUtils.getFormattedDate(properties, LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE));
        scheme.setSchemeCloseDate(ParseUtils.getFormattedDate(properties, LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE));
        scheme.setSchemeNationalArchiveApprovalDate(ParseUtils.getFormattedDate(properties, LifeupContentModel.PROPERTY_SCHEME_NATIONAL_ARCHIVE_APPROVAL_DATE));
        fillAuditFields(properties, scheme);
        return scheme;
    }

    @Override
    public void update(NodeRef nodeRef, SchemeUpdateRequest request) {
        log.info("Going to update scheme with nodeRef " + nodeRef);
        assertSchemeExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        SchemeStatus status = SchemeStatus.valueOf(properties.get(LifeupContentModel.PROPERTY_SCHEME_STATUS).toString());
        if (status == SchemeStatus.DELETED) {
            throw new IllegalArgumentException("Scheme is in DELETED state, can not update");
        }

        if (StringUtils.isEmpty(request.getSchemeName()) || StringUtils.isEmpty(request.getSchemeResponsible())) {
            throw new IllegalArgumentException("Not all mandatory parameters specified");
        }

        properties.put(LifeupContentModel.PROPERTY_SCHEME_NAME, request.getSchemeName());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_RESPONSIBLE, request.getSchemeResponsible());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_DESCRIPTION, request.getSchemeDescription());
        updateSchemeDatesAndStatus(request, properties);
        nodeService.setProperties(nodeRef, properties);
    }

    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete scheme with nodeRef " + nodeRef);
        assertSchemeExists(nodeRef);

        SchemeStatus status = SchemeStatus.valueOf(nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS).toString());
        if (status != SchemeStatus.DRAFT) {
            throw new IllegalArgumentException("Scheme should be in a DRAFT state in order to be deleted");
        }

        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.DELETED);
    }

    @Override
    public NodeRef getScheme(NodeRef childNodeRef) {
        ChildAssociationRef childAssociationRef = nodeService.getPrimaryParent(childNodeRef);
        if (childAssociationRef == null) {
            return null;
        }

        NodeRef nodeRef = childAssociationRef.getParentRef();
        if (nodeRef == null) {
            return null;
        }

        return isScheme(nodeRef) ? nodeRef : getScheme(nodeRef);
    }

    @Override
    public boolean isScheme(NodeRef nodeRef) {
        return hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_SCHEME);
    }

    @Override
    public int getCounter(NodeRef nodeRef) {
        Integer counter = (Integer) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_ID_COUNTER);
        return counter == null ? 1 : counter;
    }

    @Override
    public void incrementCounter(NodeRef nodeRef) {
        int counter = getCounter(nodeRef);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_ID_COUNTER, ++counter);
    }

    @Override
    public String getFolderSeparator(NodeRef nodeRef) {
        return (String) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_ID_FOLDER_SEPARATOR);
    }

    @Override
    public Pair<Date, Date> getOpenCloseDates(NodeRef nodeRef) {
        Date openDate = (Date) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE);
        Date closeDate = (Date) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE);
        return new Pair<>(openDate, closeDate);
    }

    @Override
	public List<SchemeNodeDto> getSchemeNodes(NodeRef nodeRef) {
		if (nodeRef == null) {
			nodeRef = getRootSchemeNodeRef();
		}
		List<NodeRef> schemeNodeRefs = new ArrayList<>();
		List<ChildAssociationRef> childAssocsRefs = nodeService.getChildAssocs(nodeRef);
		childAssocsRefs.forEach(childAssociationRef -> {
			NodeRef childRef = childAssociationRef.getChildRef();
			if (nodeService.hasAspect(childRef, LifeupContentModel.ASPECT_FOLDER)
					|| nodeService.hasAspect(childRef, LifeupContentModel.ASPECT_SCHEME)) {
				schemeNodeRefs.add(childRef);
			}
		});
		return transformSchemeNodes(schemeNodeRefs);
	}

    @Override
    public List<SchemeNodeDto> getSchemeNodesByOrganization(NodeRef organizationRef) {
        if (organizationRef == null) {
            return getSchemeNodes(getRootSchemeNodeRef());
        }

        List<NodeRef> schemeNodeRefs = getSchemeNodeRefsByOrganization(organizationRef);
        return transformSchemeNodes(schemeNodeRefs);
    }

    private List<SchemeNodeDto> transformSchemeNodes(List<NodeRef> schemeNodeRefs) {
        List<SchemeNodeDto> result = new ArrayList<>();
        schemeNodeRefs.stream()
                .filter(schemeNodeRef -> fileFolderService.getFileInfo(schemeNodeRef).isFolder())
                .forEach(schemeNodeRef -> {
                    SchemeNodeDto schemeNodeDto = schemeMapper.mapToDto(schemeNodeRef);
                    result.add(schemeNodeDto);
                });
        return result;
    }

    private List<NodeRef> getSchemeNodeRefsByOrganization(NodeRef organizationRef) {
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addTypeMatchClause(query, ContentModel.TYPE_FOLDER);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, ContentModel.PROP_CATEGORIES, organizationRef.toString());
        return findNodeRefsByLuceneQuery(query);
    }

    private Map<QName, Serializable> createProperties(SchemeCreationRequest request) {
        String organizationName = request.getSchemeOrganization();
        NodeRef organizationNodeRef = flowhubCategoryService.findByNameWithinCategory(TopCategory.ORGANIZATIONS, organizationName);
        if (organizationNodeRef == null) {
            throw new IllegalArgumentException("Organization " + organizationName + " does not exists");
        }

        Map<QName, Serializable> properties = new HashMap<>(15);
        properties.put(LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.DRAFT);
        properties.put(LifeupContentModel.PROPERTY_SCHEME_ID, request.getSchemeId());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_NAME, request.getSchemeName());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_ORGANIZATION, organizationNodeRef);
        properties.put(LifeupContentModel.PROPERTY_SCHEME_RESPONSIBLE, request.getSchemeResponsible());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_DESCRIPTION, request.getSchemeDescription());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_ID_FOLDER_SEPARATOR, request.getSchemeIdFolderSeparator());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_ID_DOCUMENT_SEPARATOR, request.getSchemeIdDocumentSeparator());
        properties.put(LifeupContentModel.PROPERTY_SCHEME_ID_COUNTER, 1);

        properties.put(ContentModel.PROP_TITLE, request.getSchemeName());
        updateSchemeDatesAndStatus(request, properties);

        ArrayList<NodeRef> organizationNodeRefs = new ArrayList<>(1);
        organizationNodeRefs.add(organizationNodeRef);
        properties.put(ContentModel.PROP_CATEGORIES, organizationNodeRefs);
        return properties;
    }

    private void updateSchemeDatesAndStatus(SchemeUpdateRequest request, Map<QName, Serializable> properties) {
        SchemeStatus schemeStatus = SchemeStatus.valueOf(properties.get(LifeupContentModel.PROPERTY_SCHEME_STATUS).toString());

        Date now = new Date();
        Date schemeOpenDate = ParseUtils.parseDate(request.getSchemeOpenDate());
        if (schemeOpenDate == null) {
            throw new IllegalArgumentException("schemeOpenDate should not be empty");
        }

        if ((schemeStatus == SchemeStatus.OPENED || schemeStatus == SchemeStatus.CLOSED) && !Objects.equals(schemeOpenDate, properties.get(LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE))) {
            throw new IllegalArgumentException("You can not change schemeOpenDate of OPENED or CLOSED scheme");
        }
        properties.put(LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE, schemeOpenDate);

        if (schemeStatus == SchemeStatus.DRAFT && now.after(schemeOpenDate)) {
            schemeStatus = SchemeStatus.OPENED;
            properties.put(LifeupContentModel.PROPERTY_SCHEME_STATUS, schemeStatus);
        }

        updateCloseDate(request, properties, schemeOpenDate);
        updateApprovalDate(request, properties);
    }

    private void updateCloseDate(SchemeUpdateRequest request, Map<QName, Serializable> properties, Date schemeOpenDate) {
        SchemeStatus schemeStatus = SchemeStatus.valueOf(properties.get(LifeupContentModel.PROPERTY_SCHEME_STATUS).toString());
        Date currentClosedDate = (Date) properties.get(LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE);
        Date schemeCloseDate = ParseUtils.parseDate(request.getSchemeCloseDate());
        boolean closedDateChanged = !Objects.equals(schemeCloseDate, currentClosedDate);

        if (schemeStatus == SchemeStatus.CLOSED && closedDateChanged) {
            throw new IllegalArgumentException("You can not change schemeCloseDate of CLOSED scheme");
        }

        if (schemeCloseDate == null) {
            properties.remove(LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE);
        } else {
            if (schemeCloseDate.before(schemeOpenDate)) {
                throw new IllegalArgumentException("schemeCloseDate is before schemeOpenDate");
            }

            if (schemeCloseDate.before(new Date())) {
                throw new IllegalArgumentException("schemeCloseDate should be in the future");
            }

            properties.put(LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE, schemeCloseDate);
        }
    }

    private void updateApprovalDate(SchemeUpdateRequest request, Map<QName, Serializable> properties) {
        Date nationalArchiveApprovalDate = ParseUtils.parseDate(request.getSchemeNationalArchiveApprovalDate());
        if (nationalArchiveApprovalDate == null) {
            properties.remove(LifeupContentModel.PROPERTY_SCHEME_NATIONAL_ARCHIVE_APPROVAL_DATE);
        } else {
            properties.put(LifeupContentModel.PROPERTY_SCHEME_NATIONAL_ARCHIVE_APPROVAL_DATE, nationalArchiveApprovalDate);
        }
    }

    @Override
    public NodeRef getRootSchemeNodeRef() {
        return getRootNodeRef(schemeRootFolderPath);
    }

    private void assertSchemeExists(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_SCHEME)) {
            throw new IllegalArgumentException("NodeRef should be a scheme");
        }
    }

    @Scheduled(cron = "${flowhub.scheme.open.draft.job.cron}")
    public void openDraftSchemes() {
        log.info("Going to OPEN all DRAFT schemes with today's open date");

        Date now = new Date();
        String today = ParseUtils.getFormattedDate(now);

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_SCHEME);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.DRAFT.name());
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE, today);

        AuthenticationUtil.runAsSystem(() -> {
            List<NodeRef> nodeRefs = findNodeRefsByLuceneQuery(query);
            for (NodeRef nodeRef : nodeRefs) {
                log.info("Setting status OPENED of scheme " + nodeRef);
                doInTransaction(() -> nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.OPENED.name()));
            }

            return null;
        });

    }

    @Scheduled(cron = "${flowhub.scheme.close.open.job.cron}")
    public void closeOpenSchemes() {
        log.info("Going to CLOSE all OPENED schemes with yesterdays's close date");

        LocalDate localDate = LocalDate.now();
        localDate = localDate.minus(1, ChronoUnit.DAYS);
        String yesterday = localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_SCHEME);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.OPENED.name());
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE, yesterday);

        AuthenticationUtil.runAsSystem(() -> {
            List<NodeRef> nodeRefs = findNodeRefsByLuceneQuery(query);
            for (NodeRef nodeRef : nodeRefs) {
                log.info("Setting status CLOSED of scheme " + nodeRef);
                doInTransaction(() -> nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.CLOSED.name()));
            }

            return null;
        });
    }

    @Override
    public void close(NodeRef nodeRef) {
        log.info("Going to close scheme " + nodeRef);
        assertSchemeExists(nodeRef);
        SchemeStatus schemeStatus = SchemeStatus.valueOf((String) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS));
        if (schemeStatus != SchemeStatus.OPENED) {
            throw new IllegalArgumentException("Scheme should be in OPENED state");
        }

        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.CLOSED);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE, new Date());
    }

    @Override
    public void reopen(NodeRef nodeRef) {
        log.info("Going to reopen scheme " + nodeRef);
        assertSchemeExists(nodeRef);
        SchemeStatus schemeStatus = SchemeStatus.valueOf((String) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS));
        if (schemeStatus != SchemeStatus.CLOSED) {
            throw new IllegalArgumentException("Scheme should be in CLOSED state");
        }

        LocalDate localDate = LocalDate.now();
        localDate = localDate.plus(1, ChronoUnit.MONTHS);
        Date closeDate = ParseUtils.asDate(localDate);

        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_STATUS, SchemeStatus.OPENED);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE, closeDate);
        log.debug("Scheme close date is set to " + closeDate);
    }

    @Override
    public NodeRef getSchemesFolder(String organisationName) {
        assertStringEmpty(organisationName, "Organisation name");
        return getExistingRootNodeRef(String.format(SCHEMES_FOLDER_PATH, organisationName));
    }

    @Override
    public PageList<Scheme> list(ListRequest listRequest) {
        return getPageList(listRequest, this::get);
    }

    private void assertStringEmpty(String value, String fieldName) {
        if (Strings.isNullOrEmpty(value)) {
            throw new IllegalArgumentException(fieldName + " not specified");
        }
    }
}
