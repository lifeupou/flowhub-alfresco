/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.web;

import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.ContainerMetadata;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileParameter;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.extensions.webscripts.WebScriptRequest;

/**
 * Dataset file base web script.
 */
public abstract class DatasetFileBaseWebScript extends AbstractBaseWebScript {

    protected DatasetFileRequest createDatasetFileRequest(WebScriptRequest request) {
        DatasetFileRequest datasetFileRequest = new DatasetFileRequest();
        datasetFileRequest.setDatasetFileType(request.getParameter(DatasetFileParameter.DATASET_FILE_TYPE.getName()));
        datasetFileRequest.setDatasetFileName(request.getParameter(DatasetFileParameter.DATASET_FILE_NAME.getName()));
        datasetFileRequest.setDatasetFileTitle(request.getParameter(DatasetFileParameter.DATASET_FILE_TITLE.getName()));
        datasetFileRequest.setDatasetFileDescription(request.getParameter(DatasetFileParameter.DATASET_FILE_DESCRIPTION.getName()));
        addDatasetFileMetadata(datasetFileRequest, request);
        return datasetFileRequest;
    }

    private void addDatasetFileMetadata(DatasetFileRequest datasetFileRequest, WebScriptRequest request) {
        String datasetFileMetadata = request.getParameter(DatasetFileParameter.DATASET_FILE_METADATA.getName());
        if (StringUtils.isNotEmpty(datasetFileMetadata)) {
            datasetFileRequest.setDatasetFileMetadata(ParseUtils.getJsonBean(datasetFileMetadata, ContainerMetadata.class));
        }
    }
}
