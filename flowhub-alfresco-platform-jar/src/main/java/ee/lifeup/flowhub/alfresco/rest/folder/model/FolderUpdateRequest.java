/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.model;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */

public class FolderUpdateRequest {
    private String folderName;
    private String folderResponsible;
    private String folderDescription;
    private String folderCloseDate;
    private String folderArchiveMandate;
    private String folderContentType;
    private String folderRetentionPeriod;
    private String folderTransferToPublicArchive;
    private String folderRetentionPeriodTrigger;
    private String folderArchivalValueNote;
    private String folderArchiveValue;
    private String folderRetentionPeriodStartDate;
    private String folderExplanation;
    private String folderGlobalCounterRef;

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getFolderResponsible() {
        return folderResponsible;
    }

    public void setFolderResponsible(String folderResponsible) {
        this.folderResponsible = folderResponsible;
    }

    public String getFolderDescription() {
        return folderDescription;
    }

    public void setFolderDescription(String folderDescription) {
        this.folderDescription = folderDescription;
    }

    public String getFolderCloseDate() {
        return folderCloseDate;
    }

    public void setFolderCloseDate(String folderCloseDate) {
        this.folderCloseDate = folderCloseDate;
    }

    public String getFolderArchiveMandate() {
        return folderArchiveMandate;
    }

    public void setFolderArchiveMandate(String folderArchiveMandate) {
        this.folderArchiveMandate = folderArchiveMandate;
    }

    public String getFolderContentType() {
        return folderContentType;
    }

    public void setFolderContentType(String folderContentType) {
        this.folderContentType = folderContentType;
    }

    public String getFolderRetentionPeriod() {
        return folderRetentionPeriod;
    }

    public void setFolderRetentionPeriod(String folderRetentionPeriod) {
        this.folderRetentionPeriod = folderRetentionPeriod;
    }

    public String getFolderTransferToPublicArchive() {
        return folderTransferToPublicArchive;
    }

    public void setFolderTransferToPublicArchive(String folderTransferToPublicArchive) {
        this.folderTransferToPublicArchive = folderTransferToPublicArchive;
    }

    public String getFolderRetentionPeriodTrigger() {
        return folderRetentionPeriodTrigger;
    }

    public void setFolderRetentionPeriodTrigger(String folderRetentionPeriodTrigger) {
        this.folderRetentionPeriodTrigger = folderRetentionPeriodTrigger;
    }

    public String getFolderArchivalValueNote() {
        return folderArchivalValueNote;
    }

    public void setFolderArchivalValueNote(String folderArchivalValueNote) {
        this.folderArchivalValueNote = folderArchivalValueNote;
    }

    public String getFolderArchiveValue() {
        return folderArchiveValue;
    }

    public void setFolderArchiveValue(String folderArchiveValue) {
        this.folderArchiveValue = folderArchiveValue;
    }

    public String getFolderRetentionPeriodStartDate() {
        return folderRetentionPeriodStartDate;
    }

    public void setFolderRetentionPeriodStartDate(String folderRetentionPeriodStartDate) {
        this.folderRetentionPeriodStartDate = folderRetentionPeriodStartDate;
    }

    public String getFolderExplanation() {
        return folderExplanation;
    }

    public void setFolderExplanation(String folderExplanation) {
        this.folderExplanation = folderExplanation;
    }

    public String getFolderGlobalCounterRef() {
        return folderGlobalCounterRef;
    }

    public void setFolderGlobalCounterRef(String folderGlobalCounterRef) {
        this.folderGlobalCounterRef = folderGlobalCounterRef;
    }
}
