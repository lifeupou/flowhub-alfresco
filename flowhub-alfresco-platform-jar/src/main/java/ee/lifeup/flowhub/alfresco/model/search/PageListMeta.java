package ee.lifeup.flowhub.alfresco.model.search;

/**
 * Created by Aleksandr Koltakov on 09.08.2018
 */

public class PageListMeta {
    private int total;
    private int pages;
    private int offset;
    private int itemsPerPage;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }

    public int getItemsPerPage() {
        return itemsPerPage;
    }

    public void setItemsPerPage(int itemsPerPage) {
        this.itemsPerPage = itemsPerPage;
    }
}
