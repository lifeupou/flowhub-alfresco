package ee.lifeup.flowhub.alfresco.rest.document.model;

import java.util.Date;

/**
 * Created by Aleksandr Koltakov on 27.08.2018
 */

public class RegistrationDetails {
    private String registrationNumber;
    private Date registrationDate;

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }
}
