/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.web;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.Scheme;
import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

@Component("webscript.lifeup.flowhub.scheme.schemes.get")
public class SchemeListWebScript extends AbstractBaseWebScript {

    protected static final String PARAMETER_ORGANISATION_NAME = "organisationName";

    @Autowired
    private SchemeService schemeService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            NodeRef schemesFolder = schemeService.getSchemesFolder(request.getParameter(PARAMETER_ORGANISATION_NAME));
            if (schemesFolder == null) {
                throw new NodeRefNotFoundException();
            }

            ListRequest listRequest = new ListRequest(LifeupContentModel.ASPECT_SCHEME, schemesFolder);
            listRequest.addFields(
                    LifeupContentModel.PROPERTY_SCHEME_STATUS,
                    LifeupContentModel.PROPERTY_SCHEME_ID,
                    LifeupContentModel.PROPERTY_SCHEME_NAME,
                    LifeupContentModel.PROPERTY_SCHEME_ORGANIZATION,
                    LifeupContentModel.PROPERTY_SCHEME_RESPONSIBLE,
                    LifeupContentModel.PROPERTY_SCHEME_DESCRIPTION,
                    LifeupContentModel.PROPERTY_SCHEME_OPEN_DATE,
                    LifeupContentModel.PROPERTY_SCHEME_CLOSE_DATE,
                    LifeupContentModel.PROPERTY_SCHEME_NATIONAL_ARCHIVE_APPROVAL_DATE,
                    LifeupContentModel.PROPERTY_SCHEME_ID_COUNTER);
            listRequest.parse(request);
            PageList<Scheme> schemePageList = schemeService.list(listRequest);
            writeResponse(response, schemePageList);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to read schemes, invalid nodeRef specified", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to read schemes, folder not found", e);
        }
    }

}