/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.service;

import ee.lifeup.flowhub.alfresco.rest.dataset.model.Dataset;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetRequest;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * Service for dataset.
 */
public interface DatasetService {

    /**
     * Create dataset.
     *
     * @param caseInstanceRef case id in Flowable.
     * @param datasetRequest dataset request.
     */
    NodeRef create(String caseInstanceRef, DatasetRequest datasetRequest);


    NodeRef create(NodeRef caseInstanceNodeRef, DatasetRequest datasetRequest);

    /**
     * Return dataset.
     *
     * @param datasetNodeRef dataset node reference.
     */
    Dataset get(NodeRef datasetNodeRef);

    /**
     * Update dataset.
     *
     * @param datasetNodeRef dataset node reference.
     * @param updateRequest  dataset request.
     */
    void update(NodeRef datasetNodeRef, DatasetRequest updateRequest);

    /**
     * Return list of datasets.
     *
     * @param caseInstanceRef case id in Flowable.
     */
    List<Dataset> list(String caseInstanceRef);


    List<Dataset> list(NodeRef caseInstanceNodeRef);

    /**
     * Delete dataset.
     * Sets status to DELETED.
     *
     * @param datasetNodeRef dataset node reference.
     */
    void delete(NodeRef datasetNodeRef);

    /**
     * Return dataset node reference.
     *
     * @param datasetRef case dataset definition reference in MDM.
     */
    NodeRef getDatasetNodeRef(String datasetRef);


    NodeRef getDatasetNodeRef(NodeRef caseInstanceNodeRef, String datasetRef);
}
