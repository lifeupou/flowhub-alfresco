/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.globalcounter.web;

import ee.lifeup.flowhub.alfresco.rest.globalcounter.model.GlobalCounterRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;

import java.util.Map;

public abstract class GlobalCounterBaseWebScript extends AbstractBaseWebScript {

    protected static final String PARAMETER_SCHEME_REF = "schemeRef";
    protected static final String PARAMETER_GLOBAL_COUNTER_NAME = "name";
    protected static final String PARAMETER_GLOBAL_COUNTER_TITLE = "title";
    protected static final String PARAMETER_GLOBAL_COUNTER_DESCRIPTION = "description";
    protected static final String PARAMETER_GLOBAL_COUNTER_VALUE = "counterValue";
    protected static final String PARAMETER_GLOBAL_COUNTER_RESET = "resetWithYearChange";

    protected GlobalCounterRequest createGlobalCounterRequest(Map<String, Object> parameterJSONAsMap) {
        GlobalCounterRequest counterRequest = new GlobalCounterRequest();
        counterRequest.setName(parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_NAME).toString());
        counterRequest.setTitle(parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_TITLE).toString());
        counterRequest.setDescription(parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_DESCRIPTION).toString());
        counterRequest.setCounterValue(
                parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_VALUE) == null ?
                        "1" :
                        parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_VALUE).toString());
        counterRequest.setResetWithYearChange(parameterJSONAsMap.get(PARAMETER_GLOBAL_COUNTER_RESET).toString());


        return counterRequest;
    }

}