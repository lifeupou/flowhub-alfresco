/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.fileinstance.model;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

public class FileInstanceCreateRequest extends FileInstanceBaseRequest {

    private NodeRef destinationFolderNodeRef;

    private String fileInstanceName;

    public NodeRef getDestinationFolderNodeRef() {
        return destinationFolderNodeRef;
    }

    public String getFileInstanceName() {
        return fileInstanceName;
    }

    public FileInstanceCreateRequest(NodeRef fileTemplateNodeRef, NodeRef destinationFolderNodeRef, String fileInstanceName, List<RequestField> requestFields, List<RequestLink> links, List<String> templateFields) {
        this.fileTemplateNodeRef = fileTemplateNodeRef;
        this.destinationFolderNodeRef = destinationFolderNodeRef;
        this.requestFieldsData = new RequestFieldsData(requestFields);
        this.requestLinksData = new RequestLinksData(links);
        this.templateFields = templateFields;
        this.fileInstanceName = fileInstanceName;
    }
}