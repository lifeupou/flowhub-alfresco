/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.utils;

import org.alfresco.service.namespace.QName;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import javax.annotation.Nullable;
import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

/**
 * Created by Aleksandr Koltakov on 11.07.2018
 */

public class ParseUtils {

    private static final Logger log = Logger.getLogger(ParseUtils.class);
    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    private ParseUtils() {
        //do not call me
    }

    public static String getString(Object value) {
        try {
            return OBJECT_MAPPER.writeValueAsString(value);
        } catch (IOException e) {
            throw new IllegalArgumentException("Can not parse " + value + " to String");
        }
    }

    @Nullable
    public static String getString(Map<QName, Serializable> properties, QName key) {
        Serializable value = properties.get(key);
        return value == null ? null : value.toString();
    }

    @Nullable
    public static JsonNode getJsonNode(Map<QName, Serializable> properties, QName key) {
        try {
            Serializable value = properties.get(key);
            return value == null ? null : OBJECT_MAPPER.readTree(value.toString());
        } catch (IOException e) {
            throw new IllegalArgumentException("Can not parse " + key.getLocalName() + " to JsonNode");
        }
    }

    @Nullable
    public static JsonNode getJsonNode(Serializable value) {
        try {
            return value == null ? null : OBJECT_MAPPER.readTree(value.toString());
        } catch (IOException e) {
            throw new IllegalArgumentException("Can not parse " + value + " to JsonNode");
        }
    }

    public static <T> T getJsonBean(Serializable value, Class<T> type) {
        try {
            return OBJECT_MAPPER.readValue(value.toString(), type);
        } catch (IOException e) {
            throw new IllegalArgumentException("Can not parse " + value + " to " + type.getName());
        }
    }

    @Nullable
    public static  String getFormattedDate(Map<QName, Serializable> properties, QName key) {
        return getFormattedDateTime(properties, key, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    @Nullable
    public static  String getFormattedDateTime(Map<QName, Serializable> properties, QName key) {
        return getFormattedDateTime(properties, key, DateTimeFormatter.ISO_INSTANT);
    }

    @Nullable
    public static  String getFormattedDateTime(Map<QName, Serializable> properties, QName key, DateTimeFormatter formatter) {
        Serializable value = properties.get(key);
        if (value == null) {
            return null;
        }

        return getFormattedDateTime((Date) value, formatter);
    }

    public static String getFormattedDate(Date date) {
        ZonedDateTime localDateTime = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return localDateTime.format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static String getFormattedDateTime(Date date, DateTimeFormatter formatter) {
        ZonedDateTime localDateTime = ZonedDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
        return localDateTime.format(formatter);
    }

    @Nullable
    public static Date parseDate(Serializable serializable) {
        return serializable == null ? null : parseDate(serializable.toString());
    }

    @Nullable
    public static Date parseDate(String textDate) {
        if (StringUtils.isEmpty(textDate)) {
            return null;
        }

        try {
            LocalDate localDate = LocalDate.parse(textDate);
            return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (Exception e) {
            throw new IllegalArgumentException("Can not parse " + textDate + " to date");
        }
    }

    public static Date asDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
    }
}
