/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.mapper;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderNodeDto;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Map;

/**
 * Mapper for folder node.
 */
@Component
public class FolderNodeMapper {

    @Autowired
    private NodeService nodeService;

    public FolderNodeDto mapToDto(NodeRef folderNodeRef) {
        Map<QName, Serializable> properties = nodeService.getProperties(folderNodeRef);
        FolderNodeDto folderNodeDto = new FolderNodeDto();
        folderNodeDto.setFolderId(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ID));
        folderNodeDto.setFolderName(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_NAME));
        folderNodeDto.setOrganization(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_SCHEME_ORGANIZATION));
        folderNodeDto.setResponsiblePerson(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_RESPONSIBLE));
        return folderNodeDto;
    }
}
