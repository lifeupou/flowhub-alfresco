/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.service;

import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.repository.ContentWriter;
import org.alfresco.service.cmr.repository.MimetypeService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.io.Charsets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.InputStream;

@Service
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private FileFolderService fileFolderService;

    @Autowired
    private MimetypeService mimetypeService;

    @Override
    public void uploadFileToNodeRef(NodeRef targetNodeRef, String fileName, InputStream fileStream) {
        ContentWriter writer = fileFolderService.getWriter(targetNodeRef);
        writer.setEncoding(Charsets.UTF_8.displayName());
        String mimeType = mimetypeService.guessMimetype(fileName);
        if (mimeType != null) {
            writer.setMimetype(mimeType);
        }
        writer.putContent(fileStream);
    }

    @Override
    public void uploadFileToNodeRefWithMimeType(NodeRef targetNodeRef, String mimeType, InputStream fileStream) {
        ContentWriter writer = fileFolderService.getWriter(targetNodeRef);
        writer.setEncoding(Charsets.UTF_8.displayName());
        if (mimeType == null) {
            throw new IllegalArgumentException("MimeType not specified");
        }

        writer.setMimetype(mimeType);
        writer.putContent(fileStream);
    }

}