package ee.lifeup.flowhub.alfresco.rest.folder.service;

import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderAccessRestriction;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Created by Aleksandr Koltakov on 28.08.2018
 */

public interface FolderAccessRestrictionService {

    FolderAccessRestriction get(NodeRef datasetNodeRef);

    void update(NodeRef datasetNodeRef, FolderAccessRestriction updateRequest);
}
