/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.web;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileParameter;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileRequest;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.service.DatasetFileService;
import ee.lifeup.flowhub.alfresco.rest.utils.WebParameter;
import ee.lifeup.flowhub.alfresco.service.FileUploadService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Web script for updating a dataset file.
 */
@Component("webscript.lifeup.flowhub.datasetfile.datasetfile.put")
public class DatasetFileUpdateWebScript extends DatasetFileBaseWebScript {

    @Autowired
    private DatasetFileService datasetFileService;
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            NodeRef datasetFileNodeRef = extractNodeRefFromRequest(request, DatasetFileParameter.DATASET_FILE_REF.getName());
            String caseFileName = getParameter(request, DatasetFileParameter.DATASET_FILE_NAME.getName(), WebParameter.MANDATORY);
            DatasetFileRequest updateRequest = createDatasetFileRequest(request);
            datasetFileService.update(datasetFileNodeRef, updateRequest);
            Content content = getContentFromRequest(request, DatasetFileParameter.DATASET_FILE_DATA.getName());
            fileUploadService.uploadFileToNodeRef(datasetFileNodeRef, caseFileName, content.getInputStream());
            writeResponse(response, datasetFileNodeRef, HttpStatus.SC_OK);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to update case dataset file", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to update case dataset file, parent nodeRef not found", e);
        }
    }
}
