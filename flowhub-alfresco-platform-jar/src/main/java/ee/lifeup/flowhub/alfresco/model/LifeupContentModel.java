/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.model;

import org.alfresco.service.namespace.QName;

/**
 * Created by Aleksandr Koltakov on 05.07.2018
 */

public class LifeupContentModel {

    private LifeupContentModel() {
        //do not call me
    }

    public static final String NAMESPACE_URI = "http://www.lifeup.eu/model/content/1.0";

    public static final QName ASPECT_SCHEME = QName.createQName(NAMESPACE_URI, "scheme");
    public static final QName ASPECT_FOLDER = QName.createQName(NAMESPACE_URI, "folder");
    public static final QName ASPECT_DOCUMENT_PROFILE = QName.createQName(NAMESPACE_URI, "documentProfile");
    public static final QName ASPECT_CASE_DEFINITION = QName.createQName(NAMESPACE_URI, "caseDefinition");
    public static final QName ASPECT_CASE_INSTANCE = QName.createQName(NAMESPACE_URI, "caseInstance");
    public static final QName ASPECT_CASE_FILE = QName.createQName(NAMESPACE_URI, "caseFile");
    public static final QName ASPECT_CASE_DATASET = QName.createQName(NAMESPACE_URI, "caseDataSet");
    public static final QName ASPECT_CASE_DATASET_ACCESS_RESTRICTION = QName.createQName(NAMESPACE_URI, "customAccessRestrictionsMeta");
    public static final QName ASPECT_CASE_DATASET_CONTENT_NODE = QName.createQName(NAMESPACE_URI, "caseDatasetContentNode");
    public static final QName ASPECT_CASE_DATASET_FILE = QName.createQName(NAMESPACE_URI, "caseDatasetFile");
    public static final QName ASPECT_CASE_DATASET_FILE_METADATA = QName.createQName(NAMESPACE_URI, "caseDatasetFileMetadata");
    public static final QName ASPECT_CASE_DOCUMENT = QName.createQName(NAMESPACE_URI, "caseDocument");
    public static final QName ASPECT_CASE_DOCUMENT_REGISTRATION = QName.createQName(NAMESPACE_URI, "caseDocumentRegistration");
    public static final QName ASPECT_FILE_TEMPLATE = QName.createQName(NAMESPACE_URI, "fileTemplate");
    public static final QName ASPECT_GLOBAL_COUNTER = QName.createQName(NAMESPACE_URI, "globalCounter");
    public static final QName ASPECT_FOLDER_ACCESS_RESTRICTION = QName.createQName(NAMESPACE_URI, "folderAccessRestriction");

    public static final QName ASSOCIATION_DOCUMENT_PROFILE_FOLDER = QName.createQName(NAMESPACE_URI, "documentProfileFolder");
    public static final QName ASSOCIATION_CASE_INSTANCE_FILE = QName.createQName(NAMESPACE_URI, "caseFileAssoc");
    public static final QName ASSOCIATION_CASE_DATASET_FILE = QName.createQName(NAMESPACE_URI, "caseDatasetFileAssoc");

    public static final QName PROPERTY_SCHEME_STATUS = QName.createQName(NAMESPACE_URI,"schemeStatus");
    public static final QName PROPERTY_SCHEME_ID = QName.createQName(NAMESPACE_URI,"schemeId");
    public static final QName PROPERTY_SCHEME_NAME = QName.createQName(NAMESPACE_URI,"schemeName");
    public static final QName PROPERTY_SCHEME_ORGANIZATION = QName.createQName(NAMESPACE_URI,"schemeOrganization");
    public static final QName PROPERTY_SCHEME_RESPONSIBLE = QName.createQName(NAMESPACE_URI,"schemeResponsible");
    public static final QName PROPERTY_SCHEME_DESCRIPTION = QName.createQName(NAMESPACE_URI,"schemeDescription");
    public static final QName PROPERTY_SCHEME_ID_FOLDER_SEPARATOR = QName.createQName(NAMESPACE_URI,"schemeIdFolderSeparator");
    public static final QName PROPERTY_SCHEME_ID_DOCUMENT_SEPARATOR = QName.createQName(NAMESPACE_URI,"schemeIdDocumentSeparator");
    public static final QName PROPERTY_SCHEME_OPEN_DATE = QName.createQName(NAMESPACE_URI,"schemeOpenDate");
    public static final QName PROPERTY_SCHEME_CLOSE_DATE = QName.createQName(NAMESPACE_URI,"schemeCloseDate");
    public static final QName PROPERTY_SCHEME_NATIONAL_ARCHIVE_APPROVAL_DATE = QName.createQName(NAMESPACE_URI,"schemeNationalArchiveApprovalDate");
    public static final QName PROPERTY_SCHEME_ID_COUNTER = QName.createQName(NAMESPACE_URI,"schemeIdCounter");

    public static final QName PROPERTY_FOLDER_STATUS = QName.createQName(NAMESPACE_URI,"folderStatus");
    public static final QName PROPERTY_FOLDER_ID = QName.createQName(NAMESPACE_URI,"folderId");
    public static final QName PROPERTY_FOLDER_NAME = QName.createQName(NAMESPACE_URI,"folderName");
    public static final QName PROPERTY_FOLDER_DESCRIPTION = QName.createQName(NAMESPACE_URI,"folderDescription");
    public static final QName PROPERTY_FOLDER_RESPONSIBLE = QName.createQName(NAMESPACE_URI,"folderResponsible");
    public static final QName PROPERTY_FOLDER_OPEN_DATE = QName.createQName(NAMESPACE_URI,"folderOpenDate");
    public static final QName PROPERTY_FOLDER_CLOSE_DATE = QName.createQName(NAMESPACE_URI,"folderCloseDate");
    public static final QName PROPERTY_FOLDER_ARCHIVE_MANDATE = QName.createQName(NAMESPACE_URI,"folderArchiveMandate");
    public static final QName PROPERTY_FOLDER_CONTENT_TYPE = QName.createQName(NAMESPACE_URI,"folderContentType");
    public static final QName PROPERTY_FOLDER_RETENTION_PERIOD = QName.createQName(NAMESPACE_URI,"folderRetentionPeriod");
    public static final QName PROPERTY_FOLDER_RETENTION_PERIOD_TRIGGER = QName.createQName(NAMESPACE_URI,"folderRetentionPeriodTrigger");
    public static final QName PROPERTY_FOLDER_ARCHIVAL_VALUE_NOTE = QName.createQName(NAMESPACE_URI,"folderArchivalValueNote");
    public static final QName PROPERTY_FOLDER_TRANSFER_TO_PUBLIC_ARCHIVE = QName.createQName(NAMESPACE_URI,"folderTransferToPublicArchive");
    public static final QName PROPERTY_FOLDER_ARCHIVE_VALUE = QName.createQName(NAMESPACE_URI,"folderArchiveValue");
    public static final QName PROPERTY_FOLDER_STORED_CONTENT = QName.createQName(NAMESPACE_URI,"folderStoredContent");
    public static final QName PROPERTY_FOLDER_RETENTION_PERIOD_START_DATE = QName.createQName(NAMESPACE_URI,"folderRetentionPeriodStartDate");
    public static final QName PROPERTY_FOLDER_EXPLANATION = QName.createQName(NAMESPACE_URI,"folderExplanation");
    public static final QName PROPERTY_FOLDER_ID_COUNTER = QName.createQName(NAMESPACE_URI,"folderIdCounter");
    public static final QName PROPERTY_FOLDER_LINKED_DOCUMENT_PROFILES = QName.createQName(NAMESPACE_URI, "folderLinkedDocumentProfiles");
    public static final QName PROPERTY_FOLDER_GLOBAL_COUNTER_REF = QName.createQName(NAMESPACE_URI, "folderGlobalCounterRef");
    public static final QName PROPERTY_FOLDER_ACCESS_RIGHT = QName.createQName(NAMESPACE_URI, "folderAccessRight");
    public static final QName PROPERTY_FOLDER_ACCESS_DATA = QName.createQName(NAMESPACE_URI, "folderAccessData");

    public static final QName PROPERTY_DOCUMENT_PROFILE_DOCUMENT_CATEGORY = QName.createQName(NAMESPACE_URI,"documentProfileDocumentCategory");
    public static final QName PROPERTY_DOCUMENT_PROFILE_NAME = QName.createQName(NAMESPACE_URI,"documentProfileName");
    public static final QName PROPERTY_DOCUMENT_PROFILE_DESCRIPTION = QName.createQName(NAMESPACE_URI,"documentProfileDescription");
    public static final QName PROPERTY_DOCUMENT_PROFILE_DATA_SET = QName.createQName(NAMESPACE_URI,"documentProfileDataSet");
    public static final QName PROPERTY_DOCUMENT_PROFILE_VIEW_FORM = QName.createQName(NAMESPACE_URI,"documentProfileViewForm");
    public static final QName PROPERTY_DOCUMENT_PROFILE_EDIT_FORM = QName.createQName(NAMESPACE_URI,"documentProfileEditForm");
    public static final QName PROPERTY_DOCUMENT_PROFILE_FILE_TEMPLATES = QName.createQName(NAMESPACE_URI,"documentProfileFileTemplates");

    //case definition
    public static final QName PROPERTY_CASE_DEFINITION_DATA = QName.createQName(NAMESPACE_URI, "caseDefinitionData");
    public static final QName PROPERTY_CASE_DEFINITION_REF = QName.createQName(NAMESPACE_URI, "caseDefinitionRef");

    //case instance
    public static final QName PROPERTY_CASE_INSTANCE_REF = QName.createQName(NAMESPACE_URI, "caseInstanceRef");
    public static final QName PROPERTY_CASE_INSTANCE_DATA = QName.createQName(NAMESPACE_URI, "caseInstanceData");
    public static final QName PROPERTY_CASE_INSTANCE_STATUS = QName.createQName(NAMESPACE_URI, "caseContentStatus");

    //file template
    public static final QName PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_TYPE = QName.createQName(NAMESPACE_URI,"fileTemplateType");
    public static final QName PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_FIELDS = QName.createQName(NAMESPACE_URI,"fileTemplateFields");

    //case file
    public static final QName PROPERTY_CASE_FILE_TYPE = QName.createQName(NAMESPACE_URI,"caseFileType");

    public static final QName PROPERTY_GLOBAL_COUNTER_VALUE = QName.createQName(NAMESPACE_URI,"counterValue");
    public static final QName PROPERTY_GLOBAL_COUNTER_RESET = QName.createQName(NAMESPACE_URI,"resetWithYearChange");

    //case dataset
    public static final QName PROPERTY_CASE_DATASET_DEFINITION_REF = QName.createQName(NAMESPACE_URI,"caseDatasetDefinitionRef");
    public static final QName PROPERTY_CASE_DATASET_TYPE = QName.createQName(NAMESPACE_URI,"caseDatasetType");
    public static final QName PROPERTY_CASE_DATASET_DATA = QName.createQName(NAMESPACE_URI,"caseDatasetData");
    public static final QName PROPERTY_CASE_DATASET_STATUS = QName.createQName(NAMESPACE_URI, "caseDatasetStatus");
    public static final QName PROPERTY_CASE_DATASET_CONTENT_NODE_DATA = QName.createQName(NAMESPACE_URI, "caseDatasetContentData");
    public static final QName PROPERTY_CASE_DATASET_ACCESS_RIGHT = QName.createQName(NAMESPACE_URI, "datasetAccessRight");
    public static final QName PROPERTY_CASE_DATASET_ACCESS_DATA = QName.createQName(NAMESPACE_URI, "datasetAccessData");

    //dataset file
    public static final QName PROPERTY_CASE_DATASET_FILE_TYPE = QName.createQName(NAMESPACE_URI, "caseDatasetFileType");
    public static final QName PROPERTY_CASE_DATASET_FILE_BDOC_FILES = QName.createQName(NAMESPACE_URI, "caseDatasetFileBdocFiles");
    public static final QName PROPERTY_CASE_DATASET_FILE_BDOC_SIGNATURES = QName.createQName(NAMESPACE_URI, "caseDatasetFileBdocSignatures");

    public static final QName PROPERTY_CASE_DOCUMENT_PROFILE_REF = QName.createQName(NAMESPACE_URI, "caseDocumentProfileRef");
    public static final QName PROPERTY_CASE_DOCUMENT_REGISTRATION_FOLDER_REF = QName.createQName(NAMESPACE_URI, "caseDocumentRegistrationFolderRef");
    public static final QName PROPERTY_CASE_DOCUMENT_REGISTRATION_NUMBER = QName.createQName(NAMESPACE_URI, "caseDocumentRegistrationNumber");
    public static final QName PROPERTY_CASE_DOCUMENT_REGISTRATION_DATE = QName.createQName(NAMESPACE_URI, "caseDocumentRegistrationDate");
}
