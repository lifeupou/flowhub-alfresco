/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.behaviour.pdf;

import ee.lifeup.flowhub.alfresco.behaviour.LifeupAbstractBaseBehaviour;
import org.alfresco.repo.content.ContentServicePolicies;
import org.alfresco.repo.policy.Behaviour;
import org.alfresco.repo.policy.JavaBehaviour;
import org.alfresco.repo.thumbnail.ThumbnailDefinition;
import org.alfresco.repo.thumbnail.ThumbnailHelper;
import org.alfresco.repo.thumbnail.ThumbnailRegistry;
import org.alfresco.service.cmr.action.Action;
import org.alfresco.service.cmr.repository.ContentData;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.thumbnail.ThumbnailService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;

import javax.xml.ws.WebServiceException;

/**
 * Base class for behavior policies of PDF files.
 */
public abstract class PdfAbstractBaseBehaviour extends LifeupAbstractBaseBehaviour implements ContentServicePolicies.OnContentUpdatePolicy {

    private static final String PDF_THUMBNAIL_NAME = "pdf";

    @Autowired
    protected ThumbnailRegistry registry;
    @Autowired
    protected ThumbnailService thumbnailService;

    @Override
    protected void init() {
        QName aspect = getAspect();
        policyComponent.bindClassBehaviour(
                ContentServicePolicies.OnContentUpdatePolicy.QNAME,
                aspect,
                new JavaBehaviour(this, ContentServicePolicies.OnContentUpdatePolicy.QNAME.getLocalName(), Behaviour.NotificationFrequency.EVERY_EVENT));

    }

    /**
     * Get marking aspect.
     */
    protected abstract QName getAspect();

    @Override
    public void onContentUpdate(NodeRef nodeRef, boolean newContent) {
        if (!nodeService.exists(nodeRef)) {
            throw new WebServiceException("NodeRef is not exist " + nodeRef);
        }
        createPdf(nodeRef);
    }

    private void createPdf(NodeRef fileNodeRef) {
        ThumbnailDefinition pdfThumbnailDefinition = registry.getThumbnailDefinition(PDF_THUMBNAIL_NAME);
        if (pdfThumbnailDefinition == null) {
            throw new WebServiceException("The thumbnail name '" + PDF_THUMBNAIL_NAME + "' is not registered");
        }

        ContentData contentData = fileFolderService.getFileInfo(fileNodeRef).getContentData();
        String nodeMimeType = contentData.getMimetype();
        if (!ContentData.hasContent(contentData)) {
            if (log.isDebugEnabled())
                log.debug("Unable to create thumbnail '" + pdfThumbnailDefinition.getName() + "' as there is no content");
        }
        if (!registry.isThumbnailDefinitionAvailable(contentData.getContentUrl(), nodeMimeType, contentData.getSize(), fileNodeRef, pdfThumbnailDefinition)) {
            log.info("Unable to create thumbnail '" + pdfThumbnailDefinition.getName() + "' for " +
                    nodeMimeType + " as no transformer is currently available.");
        }

        Action action = ThumbnailHelper.createCreateThumbnailAction(pdfThumbnailDefinition, serviceRegistry);
        actionService.executeAction(action, fileNodeRef, false, true);
    }

}
