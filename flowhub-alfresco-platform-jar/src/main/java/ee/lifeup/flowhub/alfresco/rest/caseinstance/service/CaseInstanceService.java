/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.caseinstance.service;

import ee.lifeup.flowhub.alfresco.rest.caseinstance.model.CaseInstance;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.model.CaseInstanceRequest;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * Service for case instance.
 */
public interface CaseInstanceService {

    /**
     * Create case instance.
     *
     * @param caseDefinitionRef   case definition reference in Flowable.
     * @param caseInstanceRequest case instance request.
     */
    NodeRef create(String caseDefinitionRef, CaseInstanceRequest caseInstanceRequest);

    /**
     * Return case instance.
     *
     * @param nodeRef case instance reference.
     */
    CaseInstance get(NodeRef nodeRef);

    CaseInstance get(String caseInstanceRef);

    /**
     * Update case instance.
     *
     * @param nodeRef       case instance reference.
     * @param updateRequest case instance request.
     */
    void update(NodeRef nodeRef, CaseInstanceRequest updateRequest);

    /**
     * Delete case instance.
     * Sets status to DELETED.
     *
     * @param nodeRef case instance reference.
     */
    void delete(NodeRef nodeRef);

    /**
     * Close case instance.
     * Sets status to CLOSED.
     *
     * @param nodeRef case instance reference.
     */
    void close(NodeRef nodeRef);

    /**
     * Return list of case instances.
     *
     * @param caseDefinitionRef case definition reference in Flowable.
     */
    List<CaseInstance> list(String caseDefinitionRef);

    /**
     * Return case instance node reference.
     *
     * @param caseRef case instance reference in Flowable.
     */
    NodeRef getCaseInstanceNodeRef(String caseRef);

    /**
     * Return node reference of case instance attachments.
     *
     * @param caseInstanceNodeRef case instance node reference.
     */
    NodeRef getCaseInstanceAttachmentsNodeRef(NodeRef caseInstanceNodeRef);

    /**
     * Return node reference of case instance datasets.
     *
     * @param caseInstanceRef case instance id in Flowable.
     */
    NodeRef getCaseInstanceDatasetsNodeRef(String caseInstanceRef);

    /**
     * Return node reference of case instance dataSets
     * @param caseInstanceNodeRef
     * @return
     */
    NodeRef getCaseInstanceDatasetsNodeRef(NodeRef caseInstanceNodeRef);
}
