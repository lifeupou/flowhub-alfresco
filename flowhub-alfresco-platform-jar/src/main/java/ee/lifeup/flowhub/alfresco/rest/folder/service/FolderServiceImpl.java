/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;
import ee.lifeup.flowhub.alfresco.exception.IllegalSearchQuerySizeException;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.folder.mapper.FolderNodeMapper;
import ee.lifeup.flowhub.alfresco.rest.folder.model.*;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.Criteria;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.FolderToDocumentProfileBindRequest;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.Rule;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.RuleCheckUtil;
import ee.lifeup.flowhub.alfresco.rest.profile.service.DocumentProfileService;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.SchemeStatus;
import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.util.Pair;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */
@Service
public class FolderServiceImpl extends AbstractServiceImpl implements FolderService {

    private static final int FOLDER_NODES_SEARCH_LIMIT = 10;

    @Autowired
    private SchemeService schemeService;
    @Autowired
    private FolderNodeMapper folderNodeMapper;

    @Autowired
    private DocumentProfileService documentProfileService;

    @Override
    public NodeRef create(NodeRef parentNodeRef, FolderCreationRequest creationRequest) {
        log.info("We've got a folder creation request");
        assertNodeIsSchemeOrFolder(parentNodeRef);
        assertNodeIsEligibleToAddFolder(parentNodeRef);

        boolean parentIsScheme = schemeService.isScheme(parentNodeRef);

        assertNodeIsAuditable(parentNodeRef);
        assertFolderCanStoreFolders(parentNodeRef);

        Map<QName, Serializable> properties = createProperties(creationRequest);
        Pair<Date, Date> parentOpenCloseDates = parentIsScheme ? schemeService.getOpenCloseDates(parentNodeRef) : getOpenCloseDates(parentNodeRef);
        assertOpenDateInRange(parentOpenCloseDates.getFirst(), creationRequest);

        Date folderOpenDate = (Date) properties.get(LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE);
        Date folderCloseDate = (Date) properties.get(LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE);
        assertCloseDateInRange(folderOpenDate, folderCloseDate, parentOpenCloseDates.getSecond());

        FileInfo fileInfo = fileFolderService.create(parentNodeRef, creationRequest.getFolderId(), ContentModel.TYPE_FOLDER);
        NodeRef folderNodeRef = fileInfo.getNodeRef();

        nodeService.addAspect(folderNodeRef, LifeupContentModel.ASPECT_FOLDER, properties);
        nodeService.addAspect(folderNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);
        nodeService.addAspect(folderNodeRef, LifeupContentModel.ASPECT_FOLDER_ACCESS_RESTRICTION, MapUtils.EMPTY_MAP);

        if (parentIsScheme) {
            schemeService.incrementCounter(parentNodeRef);
        } else {
            incrementCounter(parentNodeRef);
        }

        return folderNodeRef;
    }

    @Override
    public Folder get(NodeRef nodeRef) {
        assertFolderExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);

        Folder folder = new Folder();
        folder.setNodeRef(nodeRef.toString());
        folder.setFolderStatus(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_STATUS));
        folder.setFolderId(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ID));
        folder.setFolderName(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_NAME));
        folder.setFolderResponsible(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_RESPONSIBLE));
        folder.setFolderDescription(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_DESCRIPTION));
        folder.setFolderOpenDate(ParseUtils.getFormattedDate(properties, LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE));
        folder.setFolderCloseDate(ParseUtils.getFormattedDate(properties, LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE));
        folder.setFolderArchiveMandate(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ARCHIVE_MANDATE));
        folder.setFolderContentType(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_CONTENT_TYPE));

        folder.setFolderRetentionPeriod(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD));
        folder.setFolderStoredContent(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_STORED_CONTENT));
        folder.setFolderTransferToPublicArchive(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_TRANSFER_TO_PUBLIC_ARCHIVE));
        folder.setFolderRetentionPeriodTrigger(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD_TRIGGER));
        folder.setFolderArchivalValueNote(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ARCHIVAL_VALUE_NOTE));
        folder.setFolderArchiveValue(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ARCHIVE_VALUE));
        folder.setFolderRetentionPeriodStartDate(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD_START_DATE));
        folder.setFolderExplanation(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_EXPLANATION));
        folder.setFolderIdCounter(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ID_COUNTER));

        fillAuditFields(properties, folder);
        return folder;
    }

    @Override
    public void update(NodeRef nodeRef, FolderUpdateRequest updateRequest) {
        log.info("We've got a folder update request");
        assertFolderExists(nodeRef);
        assertFolderIsEditable(nodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        updateFolderCloseDate(updateRequest, properties);
        updateProperties(updateRequest, properties);

        NodeRef parentNodeRef = nodeService.getPrimaryParent(nodeRef).getParentRef();
        boolean parentIsScheme = schemeService.isScheme(parentNodeRef);
        Pair<Date, Date> parentOpenCloseDates = parentIsScheme ? schemeService.getOpenCloseDates(parentNodeRef) : getOpenCloseDates(parentNodeRef);
        Date folderOpenDate = (Date) properties.get(LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE);
        Date folderCloseDate = (Date) properties.get(LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE);
        assertCloseDateInRange(folderOpenDate, folderCloseDate, parentOpenCloseDates.getSecond());

        nodeService.addProperties(nodeRef, properties);
    }

    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete folder " + nodeRef);
        assertFolderExists(nodeRef);
        assertFolderIsDeletable(nodeRef);

        List<ChildAssociationRef> childAssociationRefs = nodeService.getChildAssocs(nodeRef);
        log.debug("Marking " + nodeRef + " as DELETED");
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.DELETED);

        if (CollectionUtils.isNotEmpty(childAssociationRefs)) {
            log.debug("Going to iterate children...");
            childAssociationRefs.forEach(childAssociationRef -> delete(childAssociationRef.getChildRef()));
        }
    }

    private Map<QName, Serializable> createProperties(FolderCreationRequest request) {
        Map<QName, Serializable> properties = new HashMap<>();
        properties.put(LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.DRAFT);
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ID, request.getFolderId());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_NAME, request.getFolderName());

        if (StringUtils.isEmpty(request.getFolderStoredContent())) {
            throw new IllegalArgumentException("Folder storedContent should be specified");
        }
        properties.put(LifeupContentModel.PROPERTY_FOLDER_STORED_CONTENT, request.getFolderStoredContent());

        properties.put(ContentModel.PROP_TITLE, request.getFolderId());
        setFolderOpenDateAndStatus(request, properties);
        updateFolderCloseDate(request, properties);
        updateProperties(request, properties);
        return properties;
    }

    private void setFolderOpenDateAndStatus(FolderCreationRequest request, Map<QName, Serializable> properties) {
        FolderStatus folderStatus = FolderStatus.valueOf(properties.get(LifeupContentModel.PROPERTY_FOLDER_STATUS).toString());

        Date now = new Date();
        Date folderOpenDate = ParseUtils.parseDate(request.getFolderOpenDate());
        if (folderOpenDate == null) {
            throw new IllegalArgumentException("folderOpenDate should not be empty");
        }

        if (folderStatus == FolderStatus.DRAFT && now.after(folderOpenDate)) {
            folderStatus = FolderStatus.OPENED;
        }

        properties.put(LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE, folderOpenDate);
        properties.put(LifeupContentModel.PROPERTY_FOLDER_STATUS, folderStatus);
    }

    private void updateFolderCloseDate(FolderUpdateRequest request, Map<QName, Serializable> properties) {
        Date folderOpenDate = (Date) properties.get(LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE);
        Date folderCloseDate = ParseUtils.parseDate(request.getFolderCloseDate());
        if (folderCloseDate != null) {
            if (folderCloseDate.before(folderOpenDate)) {
                throw new IllegalArgumentException("folderCloseDate is before folderOpenDate");
            }

            if (folderCloseDate.before(new Date())) {
                throw new IllegalArgumentException("folderCloseDate should be in the future");
            }

            properties.put(LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE, folderCloseDate);
        }
    }

    private void updateProperties(FolderUpdateRequest request, Map<QName, Serializable> properties) {
        assertMandatoryParametersExists(request);
        properties.put(LifeupContentModel.PROPERTY_FOLDER_NAME, request.getFolderName());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_RESPONSIBLE, request.getFolderResponsible());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_DESCRIPTION, request.getFolderDescription());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ARCHIVE_MANDATE, request.getFolderArchiveMandate());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_CONTENT_TYPE, request.getFolderContentType());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD, request.getFolderRetentionPeriod());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_TRANSFER_TO_PUBLIC_ARCHIVE, request.getFolderTransferToPublicArchive());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD_TRIGGER, request.getFolderRetentionPeriodTrigger());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ARCHIVAL_VALUE_NOTE, request.getFolderArchivalValueNote());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ARCHIVE_VALUE, request.getFolderArchiveValue());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_RETENTION_PERIOD_START_DATE, ParseUtils.parseDate(request.getFolderRetentionPeriodStartDate()));
        properties.put(LifeupContentModel.PROPERTY_FOLDER_EXPLANATION, request.getFolderExplanation());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_GLOBAL_COUNTER_REF, request.getFolderGlobalCounterRef());
    }

    private void assertMandatoryParametersExists(FolderUpdateRequest folderUpdateRequest) {
        if (
            StringUtils.isEmpty(folderUpdateRequest.getFolderResponsible())
            || StringUtils.isEmpty(folderUpdateRequest.getFolderRetentionPeriod())
            || StringUtils.isEmpty(folderUpdateRequest.getFolderRetentionPeriodStartDate())
            || StringUtils.isEmpty(folderUpdateRequest.getFolderExplanation())
            || StringUtils.isEmpty(folderUpdateRequest.getFolderContentType())) {
            throw new IllegalArgumentException("Not all mandatory parameters exists");
        }
    }

    @Override
    public String generateFolderIdentifier(NodeRef parentNodeRef) {
        assertNodeIsSchemeOrFolder(parentNodeRef);
        assertNodeIsEligibleToAddFolder(parentNodeRef);

        if (schemeService.isScheme(parentNodeRef)) {
            return String.valueOf(schemeService.getCounter(parentNodeRef));
        }

        assertFolderCanStoreFolders(parentNodeRef);

        NodeRef parentSchemeNodeRef = schemeService.getScheme(parentNodeRef);
        return new StringBuilder()
                .append(nodeService.getProperty(parentNodeRef, LifeupContentModel.PROPERTY_FOLDER_ID))
                .append(schemeService.getFolderSeparator(parentSchemeNodeRef))
                .append(getCounter(parentNodeRef))
                .toString();
    }

    private void assertFolderCanStoreFolders(NodeRef nodeRef) {
        Serializable value = nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STORED_CONTENT);
        if (value == null) {
            return;
        }

        String folderStoredContent = (String) value;
        if (!StringUtils.equalsIgnoreCase(folderStoredContent, FolderStoredContent.FOLDER.name())) {
            throw new IllegalArgumentException("Parent folder should have FOLDER stored content");
        }
    }

    @Override
    public boolean isFolder(NodeRef nodeRef) {
        return hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_FOLDER);
    }

    private void assertFolderExists(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!isFolder(nodeRef)) {
            throw new IllegalArgumentException("NodeRef should be a Folder");
        }
    }

    private void assertNodeIsSchemeOrFolder(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_SCHEME, LifeupContentModel.ASPECT_FOLDER)) {
            throw new IllegalArgumentException("NodeRef should be either Scheme or Folder");
        }
    }

    @Override
    public Pair<Date, Date> getOpenCloseDates(NodeRef nodeRef) {
        Date openDate = (Date) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE);
        Date closeDate = (Date) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE);
        return new Pair<>(openDate, closeDate);
    }

    @Override
    public int getCounter(NodeRef nodeRef) {
        Integer counter = (Integer) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_ID_COUNTER);
        return counter == null ? 1 : counter;
    }

    @Override
    public void incrementCounter(NodeRef nodeRef) {
        int counter = getCounter(nodeRef);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_ID_COUNTER, ++counter);
    }

    @Override
    public int getCounterAndIncrement(NodeRef nodeRef) {
        assertFolderExists(nodeRef);
        int value = getCounter(nodeRef);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_ID_COUNTER, value + 1);
        return value;
    }

    @Override
    public List<FolderNodeDto> search(NodeRef parentRef, String query) {
        if (parentRef == null) {
            parentRef = schemeService.getRootSchemeNodeRef();
        }
        List<FolderNodeDto> folderNodesDto = Lists.newArrayList();
        List<NodeRef> folderNodeRefs = getFolderNodeRefsByQuery(query, getPathByNodeRef(parentRef));
        folderNodeRefs.forEach(folderNodeRef -> folderNodesDto.add(folderNodeMapper.mapToDto(folderNodeRef)));
        return folderNodesDto;
    }

    @Override
    public List<NodeRef> getFolderNodeRefsByQuery(String query, String parentFolderPath) {
        if (!StringUtils.isEmpty(query) && query.length() < 2) {
            throw new IllegalSearchQuerySizeException();
        }

        if (StringUtils.isEmpty(parentFolderPath)) {
            parentFolderPath = getPathByNodeRef(schemeService.getRootSchemeNodeRef());
        }

        StringBuilder searchQuery = createFolderNodeRefsLuceneQuery(query, parentFolderPath);
        return findNodeRefsByLuceneQuery(searchQuery, FOLDER_NODES_SEARCH_LIMIT);
    }

    private StringBuilder createFolderNodeRefsLuceneQuery(String query, String parentFolderPath) {
        StringBuilder searchQuery = new StringBuilder();
        LuceneQueryUtils.addPathMatchClause(searchQuery, parentFolderPath + "//*");
        LuceneQueryUtils.addAndClause(searchQuery);
        LuceneQueryUtils.addTypeMatchClause(searchQuery, ContentModel.TYPE_FOLDER);

        if (!StringUtils.isEmpty(query)) {
            LuceneQueryUtils.addAndClause(searchQuery);
            searchQuery.append(" (");
            LuceneQueryUtils.addPropertyMatchClause(searchQuery, LifeupContentModel.PROPERTY_FOLDER_NAME, query + "*");
            LuceneQueryUtils.addOrClause(searchQuery);
            LuceneQueryUtils.addPropertyMatchClause(searchQuery, LifeupContentModel.PROPERTY_FOLDER_ID, query + "*");
            searchQuery.append(")");
        }

        return searchQuery;
    }

    private void assertNodeIsEligibleToAddFolder(NodeRef nodeRef) {
        boolean schemeEditable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_SCHEME_STATUS,
                SchemeStatus.class,
                SchemeStatus.DRAFT, SchemeStatus.OPENED);
        boolean folderEditable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_FOLDER_STATUS,
                FolderStatus.class,
                FolderStatus.DRAFT, FolderStatus.OPENED);
        if (!schemeEditable && !folderEditable) {
            throw new IllegalArgumentException("Folder should be in the following states [DRAFT, OPENED]");
        }
    }

    private void assertFolderIsEditable(NodeRef nodeRef) {
        boolean editable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_FOLDER_STATUS,
                FolderStatus.class,
                FolderStatus.DRAFT, FolderStatus.OPENED, FolderStatus.CLOSED);
        if (!editable) {
            throw new IllegalArgumentException("Folder should be in the following states [DRAFT, OPENED, CLOSED]");
        }
    }

    private void assertFolderIsDeletable(NodeRef nodeRef) {
        boolean editable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_FOLDER_STATUS,
                FolderStatus.class,
                FolderStatus.DRAFT, FolderStatus.OPENED);
        if (!editable) {
            throw new IllegalArgumentException("Folder should be in the following states [DRAFT, OPENED]");
        }
    }

    private void assertOpenDateInRange(Date parentOpenDate, FolderCreationRequest request) {
        Date folderOpenDate = ParseUtils.parseDate(request.getFolderOpenDate());

        if (folderOpenDate.before(parentOpenDate)) {
            throw new IllegalArgumentException("Folder open date " + folderOpenDate + " is before parent open date " + parentOpenDate);
        }
    }

    private void assertCloseDateInRange(Date folderOpenDate, Date folderCloseDate, Date parentCloseDate) {
        if (parentCloseDate == null) {
            return;
        }

        if (folderOpenDate.after(parentCloseDate)) {
            throw new IllegalArgumentException("Folder open date " + folderOpenDate + " is after parent close date " + parentCloseDate);
        }

        if (folderCloseDate != null && folderCloseDate.after(parentCloseDate)) {
            throw new IllegalArgumentException("Folder close date " + folderCloseDate + " is after parent close date " + parentCloseDate);
        }
    }

    @Scheduled(cron = "${flowhub.folder.open.draft.job.cron}")
    public void openDraftFolders() {
        log.info("Going to OPEN all DRAFT folders with today's open date");

        Date now = new Date();
        String today = ParseUtils.getFormattedDate(now);

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_FOLDER);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.DRAFT.name());
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_FOLDER_OPEN_DATE, today);

        AuthenticationUtil.runAsSystem(() -> {
            List<NodeRef> nodeRefs = findNodeRefsByLuceneQuery(query);
            for (NodeRef nodeRef : nodeRefs) {
                log.info("Setting status OPENED of folder " + nodeRef);
                doInTransaction(() -> nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.OPENED.name()));
            }

            return null;
        });

    }

    @Scheduled(cron = "${flowhub.folder.close.open.job.cron}")
    public void closeOpenFolders() {
        log.info("Going to CLOSE all OPENED folders with yesterdays's close date");

        LocalDate localDate = LocalDate.now();
        localDate = localDate.minus(1, ChronoUnit.DAYS);
        String yesterday = localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_FOLDER);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.OPENED.name());
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE, yesterday);

        AuthenticationUtil.runAsSystem(() -> {
            List<NodeRef> nodeRefs = findNodeRefsByLuceneQuery(query);
            for (NodeRef nodeRef : nodeRefs) {
                log.info("Setting status CLOSED of folder " + nodeRef);
                doInTransaction(() -> nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.CLOSED.name()));
            }

            return null;
        });
    }

    @Override
    public void close(NodeRef nodeRef) {
        log.info("Going to close folder " + nodeRef);
        assertFolderExists(nodeRef);
        FolderStatus folderStatus = FolderStatus.valueOf((String) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS));
        if (folderStatus != FolderStatus.OPENED) {
            throw new IllegalArgumentException("Folder should be in OPENED state");
        }

        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.CLOSED);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE, new Date());
    }

    @Override
    public void reopen(NodeRef nodeRef) {
        log.info("Going to reopen folder " + nodeRef);
        assertFolderExists(nodeRef);
        FolderStatus folderStatus = FolderStatus.valueOf((String) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS));
        if (folderStatus != FolderStatus.CLOSED) {
            throw new IllegalArgumentException("Folder should be in CLOSED state");
        }

        LocalDate localDate = LocalDate.now();
        localDate = localDate.plus(1, ChronoUnit.MONTHS);
        Date closeDate = ParseUtils.asDate(localDate);

        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_STATUS, FolderStatus.OPENED);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_FOLDER_CLOSE_DATE, closeDate);
        log.debug("Folder close date is set to " + closeDate);
    }

    @Override
    public void bind(NodeRef folderNodeRef, FolderToDocumentProfileBindRequest bindRequest) {
        NodeRef profileNodeRef = new NodeRef(bindRequest.getDocumentProfileRef());
        List<Rule> existingRules = getExistingRulesExceptGivenFolder(profileNodeRef, folderNodeRef);
        List<Rule> rules = Arrays.asList(bindRequest.getRules());
        if (RuleCheckUtil.isRulesConflict(existingRules, rules)) {
            throw new IllegalArgumentException("Unable to add profile to the folder because of rules conflict");
        }

        Criteria criteria = getFolderProfileCriteria(folderNodeRef);
        if (criteria == null) {
            criteria = new Criteria();
            criteria.setCriterionMap(new HashMap<>());
        }

        criteria.addRulesByProfile(profileNodeRef, rules);
        try {
            String json = OBJECT_MAPPER.writeValueAsString(criteria);
            nodeService.setProperty(folderNodeRef, LifeupContentModel.PROPERTY_FOLDER_LINKED_DOCUMENT_PROFILES, json);
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("Unable to create JSON form criteria, rules " + rules);
        }

        nodeService.createAssociation(profileNodeRef, folderNodeRef, LifeupContentModel.ASSOCIATION_DOCUMENT_PROFILE_FOLDER);
    }

    private List<Rule> getExistingRulesExceptGivenFolder(NodeRef profileNodeRef, NodeRef folderNodeRef) {
        List<Rule> existingRules = new ArrayList<>();

        List<NodeRef> profileBindFolders = documentProfileService.getAssociatedFolders(profileNodeRef);
        for (NodeRef nodeRef : profileBindFolders) {
            if (Objects.equals(nodeRef, folderNodeRef)) {
                continue;//skip specified folder
            }

            Criteria criteria = getFolderProfileCriteria(nodeRef);
            if (criteria != null) {
                List<Rule> profileRules = criteria.getRulesByProfile(profileNodeRef);
                if (CollectionUtils.isNotEmpty(profileRules)) {
                    existingRules.addAll(profileRules);
                }
            }
        }
        log.debug("Profile's existing rules : " + existingRules);
        return existingRules;
    }

    private Criteria getFolderProfileCriteria(NodeRef folderNodeRef) {
        String json = (String) nodeService.getProperty(folderNodeRef, LifeupContentModel.PROPERTY_FOLDER_LINKED_DOCUMENT_PROFILES);
        if (StringUtils.isEmpty(json)) {
            return null;//no criteria exists
        }

        try {
            return OBJECT_MAPPER.readValue(json, Criteria.class);
        } catch (IOException e) {
            log.warn("Exception parsing json " + json + " of folder nodeRef " + folderNodeRef);
            return null;
        }
    }

    @Override
    public NodeRef getMatchedFolder(List<NodeRef> folderNodeRefs, NodeRef profileNodeRef, Map<String, Object> dataSetVariables) {
        for (NodeRef folderNodeRef : folderNodeRefs) {
            if (isMatch(folderNodeRef, profileNodeRef, dataSetVariables)) {
                return folderNodeRef;
            }
        }

        return null;
    }

    private boolean isMatch(NodeRef folderNodeRef, NodeRef profileNodeRef, Map<String, Object> dataSetVariables) {
        if (log.isDebugEnabled()) {
            log.debug("Going to check if folder " + folderNodeRef + " is suitable for document of profile " + profileNodeRef);
            log.debug("DataSet variables " + dataSetVariables);
        }

        Criteria criteria = getFolderProfileCriteria(folderNodeRef);
        if (criteria == null) {
            log.debug("Folder has not criteria");
            return false;
        }

        List<Rule> rules = criteria.getRulesByProfile(profileNodeRef);
        if (CollectionUtils.isEmpty(rules)) {
            log.debug("Folder has no rules defined for this profile");
            return false;
        }

        for (Rule rule : rules) {
            if (log.isDebugEnabled()) {
                log.debug("Checking rule " + rule);
            }

            String element = rule.getElement();
            Object value = dataSetVariables.get(element);
            String counterValue = value == null ? null : String.valueOf(value);

            if (!rule.isMatch(counterValue)) {
                return false;
            }
        }

        return true;
    }
}
