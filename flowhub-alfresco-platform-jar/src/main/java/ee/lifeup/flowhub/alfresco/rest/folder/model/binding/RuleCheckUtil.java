/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.model.binding;

import java.util.*;

/**
 * Created by Aleksandr Koltakov on 07.08.2018
 */

public class RuleCheckUtil {
    private RuleCheckUtil() {
        //do not call me
    }

    public static boolean isRulesConflict(List<Rule> existingRules, List<Rule> rules) {
        Map<String, List<Rule>> existingRulesMap = getRulesMap(existingRules);
        Map<String, List<Rule>> additionalRulesMap = getRulesMap(rules);

        // Looking for matching keys
        Set<String> keys = new HashSet<>();
        for (String key : existingRulesMap.keySet()) {
            if (additionalRulesMap.containsKey(key)) {
                keys.add(key);
            }
        }

        if (keys.isEmpty()) {
            return !existingRulesMap.isEmpty();
        }

        for (String key : keys) {
            if (!isIntersects(existingRulesMap.get(key), additionalRulesMap.get(key))) {
                return false;
            }
        }

        return true;
    }

    private static Map<String, List<Rule>> getRulesMap(List<Rule> rules) {
        Map<String, List<Rule>> rulesMap = new HashMap<>();
        for (Rule rule : rules) {
            String element = rule.getElement();
            if (!rulesMap.containsKey(element)) {
                rulesMap.put(element, new ArrayList<>());
            }

            rulesMap.get(element).add(rule);
        }

        return rulesMap;
    }

    private static boolean isIntersects(List<Rule> rulesOne, List<Rule> rulesTwo) {
        RuleSet ruleSetOne = RuleSet.build(rulesOne);
        RuleSet ruleSetTwo = RuleSet.build(rulesTwo);
        return ruleSetOne.isIntersect(ruleSetTwo);
    }
}
