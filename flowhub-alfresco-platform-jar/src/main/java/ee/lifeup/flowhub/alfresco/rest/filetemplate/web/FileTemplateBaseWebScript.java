/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.filetemplate.web;

import ee.lifeup.flowhub.alfresco.rest.filetemplate.model.FileTemplateRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import org.springframework.extensions.webscripts.WebScriptRequest;

public abstract class FileTemplateBaseWebScript extends AbstractBaseWebScript {

    protected static final String PARAMETER_ORGANISATION_NAME = "organisationName";
    protected static final String PARAMETER_FILE_TEMPLATE_TYPE = "fileTemplateType";
    protected static final String PARAMETER_FILE_TEMPLATE_NAME = "name";
    protected static final String PARAMETER_FILE_TEMPLATE_TITLE = "title";
    protected static final String PARAMETER_FILE_TEMPLATE_DESCRIPTION = "description";
    protected static final String PARAMETER_FILE_TEMPLATE_FIELDS = "fileTemplateFields";
    protected static final String PARAMETER_FILE_TEMPLATE_FILENAME = "filename";

    protected static FileTemplateRequest createFileTemplateRequest(WebScriptRequest request) {
        FileTemplateRequest fileTemplateRequest = new FileTemplateRequest();
        fileTemplateRequest.setOrganisationName(request.getParameter(PARAMETER_ORGANISATION_NAME));
        fileTemplateRequest.setFileTemplateType(request.getParameter(PARAMETER_FILE_TEMPLATE_TYPE));
        fileTemplateRequest.setName(request.getParameter(PARAMETER_FILE_TEMPLATE_NAME));
        fileTemplateRequest.setTitle(request.getParameter(PARAMETER_FILE_TEMPLATE_TITLE));
        fileTemplateRequest.setDescription(request.getParameter(PARAMETER_FILE_TEMPLATE_DESCRIPTION));
        fileTemplateRequest.setFileTemplateFields(request.getParameter(PARAMETER_FILE_TEMPLATE_FIELDS));
        return fileTemplateRequest;
    }
}