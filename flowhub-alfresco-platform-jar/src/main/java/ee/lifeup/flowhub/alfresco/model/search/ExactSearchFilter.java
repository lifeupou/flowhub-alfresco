/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.model.search;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.ResultSet;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */

public abstract class ExactSearchFilter {

    protected NodeService nodeService;

    protected abstract boolean isMatch(NodeRef nodeRef);

    public List<NodeRef> filter(NodeService nodeService, ResultSet resultSet) {
        this.nodeService = nodeService;

        List<NodeRef> result = new ArrayList<NodeRef>();

        for (NodeRef nodeRef : resultSet.getNodeRefs()) {
            if (nodeService.exists(nodeRef) && isMatch(nodeRef)) {
                result.add(nodeRef);
            }
        }

        return result;
    }
}
