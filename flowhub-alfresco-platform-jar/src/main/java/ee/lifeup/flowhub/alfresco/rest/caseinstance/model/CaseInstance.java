/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.caseinstance.model;

import ee.lifeup.flowhub.alfresco.model.Auditable;

import java.util.Objects;

/**
 * Case instance model.
 */
public class CaseInstance extends CaseInstanceRequest implements Auditable {
    private String caseIdentifier;
    private String caseTitle;
    private String state;
    private String nodeRef;
    private String created;
    private String creator;
    private String modified;
    private String modifier;

    public String getCaseIdentifier() {
        return caseIdentifier;
    }

    public void setCaseIdentifier(String caseIdentifier) {
        this.caseIdentifier = caseIdentifier;
    }

    public String getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(String nodeRef) {
        this.nodeRef = nodeRef;
    }

    public String getCreated() {
        return created;
    }

    @Override
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModified() {
        return modified;
    }

    @Override
    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getCaseTitle() {
        return caseTitle;
    }

    public void setCaseTitle(String caseTitle) {
        this.caseTitle = caseTitle;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseInstance)) return false;
        CaseInstance that = (CaseInstance) o;
        return Objects.equals(getCaseIdentifier(), that.getCaseIdentifier()) &&
                Objects.equals(getCaseTitle(), that.getCaseTitle()) &&
                Objects.equals(getState(), that.getState()) &&
                Objects.equals(getNodeRef(), that.getNodeRef()) &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getCreator(), that.getCreator()) &&
                Objects.equals(getModified(), that.getModified()) &&
                Objects.equals(getModifier(), that.getModifier());
    }

    @Override
    public int hashCode() {

        return Objects.hash(getCaseIdentifier(), getCaseTitle(), getState(), getNodeRef(), getCreated(), getCreator(), getModified(), getModifier());
    }
}
