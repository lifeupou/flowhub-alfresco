/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.model.search;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */

public abstract class ExactPagingSearchFilter {

    protected abstract boolean isMatch(NodeRef nodeRef);

    public PageListNodeRef filter(NodeService nodeService, ResultSet resultSet, PagingDefinition pagingDefinition) {
        List<NodeRef> nodeRefs = new ArrayList<>();

        PageListNodeRef pageList = new PageListNodeRef();
        pageList.setNodeRefs(nodeRefs);

        int resultLength = resultSet.length();
        if (resultLength == 0) {
            return pageList;
        }

        PageListMeta meta = pageList.getMeta();
        meta.setTotal(resultLength);

        Pair<Integer, Integer> limits = getStartAndFinish(meta, pagingDefinition);

        int start = limits.getFirst();
        int end = limits.getSecond();

        if (end > resultLength) {
            end = resultLength;
        }

        for (int idx = start; idx <= end; idx++) {
            NodeRef nodeRef = resultSet.getNodeRef(idx - 1);
            if (nodeService.exists(nodeRef) && isMatch(nodeRef)) {
                nodeRefs.add(nodeRef);
            }
        }

        meta.setOffset(start - 1);
        return pageList;
    }

    Pair<Integer, Integer> getStartAndFinish(PageListMeta meta, PagingDefinition pagingDefinition) {
        return pagingDefinition.isPagingEnabled()
                ? getPagingLimits(meta, pagingDefinition.getPage(), pagingDefinition.getPageSize())
                : getSeamlessLimits(pagingDefinition.getOffset(), pagingDefinition.getLimit());
    }

    Pair<Integer, Integer> getPagingLimits(PageListMeta meta, int pageNumber, int pageSize) {
        int start = 1;
        int total = meta.getTotal();
        int end = total;

        meta.setItemsPerPage(pageSize);
        int pages = total / pageSize;
        if (total % pageSize != 0) {
            pages = pages + 1;
        }
        meta.setPages(pages);

        if (pageNumber > 0 && pageSize > 0) {
            start = (pageNumber - 1) * pageSize + 1;
            end = start + pageSize - 1;
        }

        return new Pair<>(start, end);
    }

    Pair<Integer, Integer> getSeamlessLimits(int offset, int limit) {
        return new Pair<>(offset + 1, offset + limit);
    }
}
