/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.category.service;

import ee.lifeup.flowhub.alfresco.rest.category.model.LocalizedNode;
import ee.lifeup.flowhub.alfresco.rest.category.model.MultiLanguageCategory;
import ee.lifeup.flowhub.alfresco.rest.category.model.TopCategory;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;
import java.util.Locale;

/**
 * Created by Aleksandr Koltakov on 02.07.2018
 */

public interface FlowhubCategoryService {

    NodeRef getRootCategoryNodeRef();

    NodeRef create(NodeRef parent, MultiLanguageCategory category);

    NodeRef create(String parentNodeName, MultiLanguageCategory category);

    void update(NodeRef nodeRef, MultiLanguageCategory category);

    void delete(NodeRef nodeRef);

    MultiLanguageCategory get(NodeRef nodeRef);

    MultiLanguageCategory get(String nodeName);

    List<LocalizedNode> list(NodeRef parent, Locale locale);

    List<LocalizedNode> list(String parentNodeName, Locale locale);

    NodeRef findByNameWithinCategory(TopCategory category, String name);
}
