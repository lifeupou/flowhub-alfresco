/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.globalcounter.service;

import com.google.common.base.Strings;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.globalcounter.model.GlobalCounter;
import ee.lifeup.flowhub.alfresco.rest.globalcounter.model.GlobalCounterRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class GlobalCounterServiceImpl extends AbstractServiceImpl implements GlobalCounterService {
    @Override
    public NodeRef createGlobalCounter(GlobalCounterRequest globalCounterRequest, NodeRef schemeRef) {
        assertObjectEmpty(schemeRef, "Scheme ref");
        checkFileTemplateRequest(globalCounterRequest);
        FileInfo globalCounter = fileFolderService.create(schemeRef, globalCounterRequest.getName(), ContentModel.TYPE_CONTENT);
        NodeRef globalCounterNodeRef = globalCounter.getNodeRef();
        Map<QName, Serializable> properties = new HashMap<>();
        fillProperties(globalCounterRequest, properties);
        nodeService.addAspect(globalCounterNodeRef, LifeupContentModel.ASPECT_GLOBAL_COUNTER, properties);
        nodeService.addAspect(globalCounterNodeRef, ContentModel.ASPECT_AUDITABLE, Collections.emptyMap());
        return globalCounterNodeRef;
    }

    @Override
    public GlobalCounter get(NodeRef nodeRef) {
        assertGlobalCounterExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        GlobalCounter globalCounter = new GlobalCounter();
        globalCounter.setNodeRef(nodeRef.toString());
        globalCounter.setNodeUuid(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        globalCounter.setName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        globalCounter.setTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        globalCounter.setDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));
        globalCounter.setCounterValue(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_GLOBAL_COUNTER_VALUE));
        globalCounter.setResetWithYearChange(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_GLOBAL_COUNTER_RESET));

        fillAuditFields(properties, globalCounter);
        return globalCounter;
    }

    @Override
    public void updateGlobalCounter(GlobalCounterRequest globalCounterRequest, NodeRef counterNodeRef) {
        assertGlobalCounterExists(counterNodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(counterNodeRef);
        fillProperties(globalCounterRequest, properties);
        nodeService.setProperties(counterNodeRef, properties);
    }

    @Override
    public PageList<GlobalCounter> list(ListRequest listRequest) {
        return getPageList(listRequest, this::get);
    }

    @Override
    public int getAndIncrement(NodeRef nodeRef) {
        assertGlobalCounterExists(nodeRef);
        Integer value = (Integer) nodeService.getProperty(nodeRef, LifeupContentModel.PROPERTY_GLOBAL_COUNTER_VALUE);
        if (value == null) {
            value = 1;
        }
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_GLOBAL_COUNTER_VALUE, value + 1);
        return value;
    }

    private void assertGlobalCounterExists(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_GLOBAL_COUNTER)) {
            throw new IllegalArgumentException("NodeRef should be a global counter");
        }
    }

    private void fillProperties(GlobalCounterRequest globalCounterRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_NAME, globalCounterRequest.getName());
        properties.put(ContentModel.PROP_TITLE, globalCounterRequest.getTitle());
        properties.put(ContentModel.PROP_DESCRIPTION, globalCounterRequest.getDescription());

        properties.put(LifeupContentModel.PROPERTY_GLOBAL_COUNTER_VALUE, globalCounterRequest.getCounterValue());
        properties.put(LifeupContentModel.PROPERTY_GLOBAL_COUNTER_RESET, globalCounterRequest.getResetWithYearChange());
    }


    private void checkFileTemplateRequest(GlobalCounterRequest globalCounterRequest) {
        assertStringEmpty(globalCounterRequest.getName(), "Organisation name");
    }

    private void assertStringEmpty(String value, String fieldName) {
        if (Strings.isNullOrEmpty(value)) {
            throw new IllegalArgumentException(fieldName + " not specified");
        }
    }

    private void assertObjectEmpty(Object value, String fieldName) {
        if (value == null) {
            throw new IllegalArgumentException(fieldName + " not specified");
        }
    }
}