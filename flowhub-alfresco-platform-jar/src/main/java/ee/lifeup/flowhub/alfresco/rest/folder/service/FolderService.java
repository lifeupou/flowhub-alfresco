/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.service;

import ee.lifeup.flowhub.alfresco.rest.folder.model.Folder;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderCreationRequest;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderNodeDto;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderUpdateRequest;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.FolderToDocumentProfileBindRequest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.Pair;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */

public interface FolderService {

    String generateFolderIdentifier(NodeRef parentNodeRef);

    NodeRef create(NodeRef parentNodeRef, FolderCreationRequest creationRequest);

    Folder get(NodeRef nodeRef);

    void update(NodeRef nodeRef, FolderUpdateRequest updateRequest);

    void delete(NodeRef nodeRef);

    boolean isFolder(NodeRef nodeRef);

    Pair<Date, Date> getOpenCloseDates(NodeRef nodeRef);

    int getCounter(NodeRef nodeRef);

    void incrementCounter(NodeRef nodeRef);

    int getCounterAndIncrement(NodeRef nodeRef);

    /**
     * Return folders by query.
     *
     * @param parentRef parent scheme node reference.
     * @param query     query for search.
     */
    List<FolderNodeDto> search(NodeRef parentRef, String query);

    /**
     * Return folder node references by query.
     *
     * @param query            query for search.
     * @param parentFolderPath path to parent folder.
     */
    List<NodeRef> getFolderNodeRefsByQuery(String query, String parentFolderPath);

    void close(NodeRef nodeRef);

    void reopen(NodeRef nodeRef);

    void bind(NodeRef folderNodeRef, FolderToDocumentProfileBindRequest bindRequest);

    NodeRef getMatchedFolder(List<NodeRef> folderNodeRefs, NodeRef profileNodeRef, Map<String, Object> dataSetVariables);
}
