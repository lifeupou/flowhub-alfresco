/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.filetemplate.service;

import com.google.common.base.Strings;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.model.FileTemplate;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.model.FileTemplateRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

@Service
public class FileTemplateServiceImpl extends AbstractServiceImpl implements FileTemplateService {

    private static final String FILE_TEMPLATES_FOLDER_PATH = "Store/%s/File Templates";

    @Override
    public NodeRef createFileTemplate(FileTemplateRequest fileTemplateRequest) {
        checkFileTemplateRequest(fileTemplateRequest);

        NodeRef organisationRootNodeRef = getRootNodeRef(String.format(FILE_TEMPLATES_FOLDER_PATH, fileTemplateRequest.getOrganisationName()));

        FileInfo fileInfo = fileFolderService.create(organisationRootNodeRef, fileTemplateRequest.getName(), ContentModel.TYPE_CONTENT);
        NodeRef fileTemplateNodeRef = fileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        fillProperties(fileTemplateRequest, properties);
        nodeService.addAspect(fileTemplateNodeRef, LifeupContentModel.ASPECT_FILE_TEMPLATE, properties);
        nodeService.addAspect(fileTemplateNodeRef, ContentModel.ASPECT_AUDITABLE, Collections.emptyMap());

        return fileTemplateNodeRef;
    }

    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete document profile " + nodeRef);
        assertFileTemplateExists(nodeRef);
        nodeService.deleteNode(nodeRef);
    }

    @Override
    public NodeRef updateFileTemplate(NodeRef nodeRef, FileTemplateRequest fileTemplateRequest) {
        assertFileTemplateExists(nodeRef);
        Map<QName, Serializable> properties = new HashMap<>();
        fillProperties(fileTemplateRequest, properties);
        nodeService.addProperties(nodeRef, properties);
        return nodeRef;
    }

    @Override
    public FileTemplate get(NodeRef nodeRef) {
        assertFileTemplateExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);

        FileTemplate result = new FileTemplate();
        result.setNodeRef(nodeRef.toString());
        result.setNodeUuid(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        result.setName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        result.setTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        result.setDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));

        result.setFileTemplateType(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_TYPE));
        result.setFileTemplateFields(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_FIELDS));

        fillAuditFields(properties, result);
        return result;
    }

    @Override
    public PageList<FileTemplate> list(ListRequest listRequest) {
        return getPageList(listRequest, this::get);
    }

    @Override
    public NodeRef getFileTemplatesFolder(String organisationName) {
        assertStringEmpty(organisationName, "Organisation name");
        return getExistingRootNodeRef(String.format(FILE_TEMPLATES_FOLDER_PATH, organisationName));
    }


    private void assertFileTemplateExists(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_FILE_TEMPLATE)) {
            throw new IllegalArgumentException("NodeRef should be a file template");
        }
    }

    private void fillProperties(FileTemplateRequest fileTemplateRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_NAME, fileTemplateRequest.getName());
        properties.put(ContentModel.PROP_TITLE, fileTemplateRequest.getTitle());
        properties.put(ContentModel.PROP_DESCRIPTION, fileTemplateRequest.getDescription());

        properties.put(LifeupContentModel.PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_TYPE, fileTemplateRequest.getFileTemplateType());
        properties.put(LifeupContentModel.PROPERTY_FILE_TEMPLATE_FILE_TEMPLATE_FIELDS, fileTemplateRequest.getFileTemplateFields());
    }

    private void checkFileTemplateRequest(FileTemplateRequest fileTemplateRequest) {
        assertStringEmpty(fileTemplateRequest.getOrganisationName(), "Organisation name");
    }

    private void assertStringEmpty(String value, String fieldName) {
        if (Strings.isNullOrEmpty(value)) {
            throw new IllegalArgumentException(fieldName + " not specified");
        }
    }
}