/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.fileinstance.web;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.fileinstance.model.FileInstanceUpdateRequest;
import ee.lifeup.flowhub.alfresco.rest.fileinstance.model.RequestField;
import ee.lifeup.flowhub.alfresco.rest.fileinstance.model.RequestLink;
import ee.lifeup.flowhub.alfresco.rest.fileinstance.service.FileInstanceGenerateService;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.model.FileTemplate;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.service.FileTemplateService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.codehaus.jackson.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.Map;


@Component("webscript.lifeup.flowhub.fileinstance.fileinstance.put")
public class FileInstanceUpdateWebScript extends AbstractBaseWebScript {

    @Autowired
    private FileInstanceGenerateService fileInstanceGenerateService;

    @Autowired
    private FileTemplateService fileTemplateService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            FileInstanceUpdateRequest generateRequest = getUpdateRequest(request);
            fileInstanceGenerateService.updateFileInstance(generateRequest);
            writeResponse(response, generateRequest.getDestinationNodeRef().toString(), HttpStatus.SC_OK);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Invalid nodeRef specified", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "NodeRef not found", e);
        } catch (IOException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to read docx template", e);
        }
    }

    private FileInstanceUpdateRequest getUpdateRequest(WebScriptRequest request) {
        JsonNode jsonNode = getParameterJSONAsNode(request);
        Map<String, Object> map = getParameterJSONAsMap(jsonNode);
        String fileTemplateNodeRef = (String) map.get("fileTemplateNodeRef");
        String destinationNodeRef = (String) map.get("destinationNodeRef");
        List<RequestField> fields = getParameterJSONAsList(jsonNode, "fields", RequestField.class, true);
        List<RequestLink> links = getParameterJSONAsList(jsonNode, "links", RequestLink.class, false);

        NodeRef templateNodeRef = getNodeRefFromString(fileTemplateNodeRef);
        FileTemplate fileTemplate = fileTemplateService.get(templateNodeRef);
        List<String> templateFields = getParameterJSONAsList(fileTemplate.getFileTemplateFields(), "fields", String.class, true);
        return new FileInstanceUpdateRequest(
                templateNodeRef,
                getNodeRefFromString(destinationNodeRef),
                fields,
                links,
                templateFields);
    }

}