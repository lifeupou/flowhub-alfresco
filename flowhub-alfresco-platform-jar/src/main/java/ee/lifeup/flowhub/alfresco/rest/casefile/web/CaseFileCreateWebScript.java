/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casefile.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFileParameter;
import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFileRequest;
import ee.lifeup.flowhub.alfresco.rest.casefile.service.CaseFileService;
import ee.lifeup.flowhub.alfresco.rest.utils.WebParameter;
import ee.lifeup.flowhub.alfresco.service.FileUploadService;
import org.alfresco.service.cmr.repository.DuplicateChildNodeNameException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Web script for creating a case file.
 */
@Component("webscript.lifeup.flowhub.casefile.casefile.post")
public class CaseFileCreateWebScript extends CaseFileBaseWebScript {

    @Autowired
    private CaseFileService caseFileService;
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            String caseRef = getParameter(request, CaseFileParameter.CASE_REF.getName(), WebParameter.MANDATORY);
            String caseFileName = getParameter(request, CaseFileParameter.NAME.getName(), WebParameter.MANDATORY);
            CaseFileRequest createRequest = createCaseFileRequest(request);
            NodeRef caseFileRef = caseFileService.create(caseRef, createRequest);
            Content content = getContentFromRequest(request, LifeupConst.FILE_DATA_PARAMETER);
            fileUploadService.uploadFileToNodeRef(caseFileRef, caseFileName, content.getInputStream());
            writeResponse(response, caseFileRef.toString(), HttpStatus.SC_CREATED);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create case file", e);
        } catch (DuplicateChildNodeNameException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to case file, duplicate name", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to case file, parent nodeRef not found", e);
        }
    }
}
