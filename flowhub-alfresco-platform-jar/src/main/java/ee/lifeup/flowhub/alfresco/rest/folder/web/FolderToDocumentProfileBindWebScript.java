/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.web;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.exception.PreconditionFailedException;
import ee.lifeup.flowhub.alfresco.rest.folder.model.binding.FolderToDocumentProfileBindRequest;
import ee.lifeup.flowhub.alfresco.rest.folder.service.FolderService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Created by Aleksandr Koltakov on 20.07.2018
 */
@Component("webscript.lifeup.flowhub.folder.documentProfile.post")
public class FolderToDocumentProfileBindWebScript extends AbstractBaseWebScript {

    @Autowired
    private FolderService folderService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            NodeRef nodeRef = getNodeRefFromRequest(request);
            FolderToDocumentProfileBindRequest bindRequest = getParameterJSONBean(request, FolderToDocumentProfileBindRequest.class);
            folderService.bind(nodeRef, bindRequest);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to add profile to folder, folder nodeRef not found", e);

        } catch (PreconditionFailedException e) {
            throw new WebScriptException(HttpStatus.SC_PRECONDITION_FAILED, "Unable to add profile to folder, parameters are missing", e);
        }
    }
}
