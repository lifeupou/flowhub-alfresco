/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.utils;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PagingDefinition;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.util.*;

/**
 * Created by Aleksandr Koltakov on 16.07.2018
 */

public class ListRequest {
    private static final String PARAMETER_ORDER = "sort";
    private static final String PARAMETER_PAGE = "page";
    private static final String PARAMETER_PAGE_SIZE = "itemsPerPage";

    private static final String PARAMETER_LIMIT = "limit";
    private static final String PARAMETER_OFFSET = "offset";

    private final QName aspect;
    private final Map<QName, List<String>> requestFilter = new HashMap<>();

    private List<SearchParameters.SortDefinition> sortDefinitions;

    private NodeRef parentNodeRef;

    private int page = 1;
    private int pageSize = 20;

    private int limit = 10;
    private int offset;

    private boolean pagingEnabled = true;

    public ListRequest(QName aspect) {
        this.aspect = aspect;
        addFields(ContentModel.PROP_NAME);
        addFields(ContentModel.PROP_TITLE);
        addFields(ContentModel.PROP_DESCRIPTION);
        addFields(ContentModel.PROP_CREATOR);
        addFields(ContentModel.PROP_CREATED);
        addFields(ContentModel.PROP_MODIFIER);
        addFields(ContentModel.PROP_MODIFIED);
    }

    public ListRequest(QName aspect, NodeRef parentNodeRef) {
        this(aspect);
        this.parentNodeRef = parentNodeRef;
    }

    public void addFields(QName... fields) {
        for (QName field : fields) {
            requestFilter.put(field, new ArrayList<>());
        }
    }

    public void parse(WebScriptRequest webScriptRequest) {
        parseFilter(webScriptRequest);
        parseOrder(webScriptRequest);
        parsePage(webScriptRequest);
        parsePageSize(webScriptRequest);
        parseLimit(webScriptRequest);
        parseOffset(webScriptRequest);
    }

    private void parseFilter(WebScriptRequest webScriptRequest) {
        for (Map.Entry<QName, List<String>> entry : requestFilter.entrySet()) {
            String localName = entry.getKey().getLocalName();

            String[] values = webScriptRequest.getParameterValues(localName);
            if (values != null && values.length > 0) {
                entry.getValue().addAll(Arrays.asList(values));
            }
        }
    }

    private void parseOrder(WebScriptRequest webScriptRequest) {
        String ordering = webScriptRequest.getParameter(PARAMETER_ORDER);
        if (StringUtils.isEmpty(ordering)) {
            return;
        }

        String[] orderingItems = ordering.split(",");
        sortDefinitions = new ArrayList<>(orderingItems.length);

        for (String orderingItem : orderingItems) {
            parseOrderingItem(orderingItem);
        }
    }

    private void parseOrderingItem(String orderingItem) {
        boolean ascending = true;
        String orderingField = orderingItem;

        if (orderingItem.startsWith("-")) {
            ascending = false;
            orderingField = orderingItem.substring(1);
        }

        QName orderBy = detectOrderingField(orderingField);
        sortDefinitions.add(new SearchParameters.SortDefinition(SearchParameters.SortDefinition.SortType.FIELD, "@" + orderBy, ascending));
    }

    private QName detectOrderingField(String orderingField) {
        for (QName field : requestFilter.keySet()) {
            if (StringUtils.equalsIgnoreCase(field.getLocalName(), orderingField)) {
                return field;
            }
        }

       return QName.createQName(LifeupContentModel.NAMESPACE_URI, orderingField);
    }

    private void parsePage(WebScriptRequest webScriptRequest) {
        Integer parsedValue = parseNumericValue(webScriptRequest, PARAMETER_PAGE);
        if (parsedValue != null) {
            page = parsedValue;
        }
    }

    private void parsePageSize(WebScriptRequest webScriptRequest) {
        Integer parsedValue = parseNumericValue(webScriptRequest, PARAMETER_PAGE_SIZE);
        if (parsedValue != null) {
            pageSize = parsedValue;
        }
    }

    private void parseLimit(WebScriptRequest webScriptRequest) {
        Integer parsedValue = parseNumericValue(webScriptRequest, PARAMETER_LIMIT);
        if (parsedValue != null) {
            limit = parsedValue;
            pagingEnabled = false;
        }
    }

    private void parseOffset(WebScriptRequest webScriptRequest) {
        Integer parsedValue = parseNumericValue(webScriptRequest, PARAMETER_OFFSET);
        if (parsedValue != null) {
            offset = parsedValue;
            pagingEnabled = false;
        }
    }

    private Integer parseNumericValue(WebScriptRequest webScriptRequest, String parameterName) {
        String parameterValue  = webScriptRequest.getParameter(parameterName);
        if (StringUtils.isBlank(parameterValue)) {
            return null;
        }

        try {
            return Integer.valueOf(parameterValue);
        } catch (Exception e) {
            return null;
        }
    }

    public List<SearchParameters.SortDefinition> getSortDefinitions() {
        return CollectionUtils.isEmpty(sortDefinitions)
                ? Arrays.asList(new SearchParameters.SortDefinition(SearchParameters.SortDefinition.SortType.FIELD, "@" + ContentModel.PROP_NAME, true))
                : sortDefinitions;
    }


    public StringBuilder getQuery() {
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, aspect);

        for (Map.Entry<QName, List<String>> entry : requestFilter.entrySet()) {
            addPropertyFilter(query, entry.getKey(), entry.getValue());
        }

        if (parentNodeRef != null) {
            LuceneQueryUtils.addAndClause(query);
            LuceneQueryUtils.addParentNodeRefClause(query, parentNodeRef);
        }

        return query;
    }

    private void addPropertyFilter(StringBuilder query, QName property, List<String> values) {
        if (CollectionUtils.isEmpty(values)) {
            return;
        }
        boolean needOrClause = false;

        LuceneQueryUtils.addAndClause(query);
        query.append('(');

        for (String value : values) {
            if (needOrClause) {
                LuceneQueryUtils.addOrClause(query);
            } else {
                needOrClause = true;
            }
            LuceneQueryUtils.addPropertyMatchClause(query, property, value);
            query.append('*');
        }
        query.append(')');
    }

    public PagingDefinition getPagingDefinition() {
        PagingDefinition pagingDefinition = new PagingDefinition();
        pagingDefinition.setPagingEnabled(pagingEnabled);
        if (pagingEnabled) {
            pagingDefinition.setPage(page);
            pagingDefinition.setPageSize(pageSize);
        } else {
            pagingDefinition.setLimit(limit);
            pagingDefinition.setOffset(offset);
        }
        return pagingDefinition;
    }
}
