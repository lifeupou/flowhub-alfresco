/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casefile.service;

import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFile;
import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFileRequest;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * Service for case file.
 */
public interface CaseFileService {

    /**
     * Create case file.
     *
     * @param caseRef         case id in Flowable.
     * @param caseFileRequest case file request.
     */
    NodeRef create(String caseRef, CaseFileRequest caseFileRequest);

    /**
     * Return case file.
     *
     * @param fileNodeRef case file node reference.
     */
    CaseFile get(NodeRef fileNodeRef);

    /**
     * Update case file.
     *
     * @param fileNodeRef   case file node reference.
     * @param updateRequest case file request.
     */
    void update(NodeRef fileNodeRef, CaseFileRequest updateRequest);

    /**
     * Return list of case files.
     *
     * @param caseRef case id in Flowable.
     */
    List<CaseFile> list(String caseRef);

}
