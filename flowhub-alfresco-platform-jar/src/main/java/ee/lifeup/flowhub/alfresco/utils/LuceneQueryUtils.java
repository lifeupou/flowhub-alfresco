/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.utils;

import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;

/**
 * Created by Aleksandr Koltakov on 09.07.2018
 */

public class LuceneQueryUtils {

    private LuceneQueryUtils() {
        //do not call me
    }

    public static void addTypeMatchClause(StringBuilder query, QName type) {
        query.append("TYPE:\"");
        query.append(escape(type));
        query.append("\"");
    }

    public static void addAspectMatchClause(StringBuilder query, QName type) {
        query.append("ASPECT:\"");
        query.append(escape(type));
        query.append("\"");
    }

    public static void addPathMatchClause(StringBuilder query, String path) {
        query.append("PATH:\"");
        query.append(escape(path));
        query.append("\"");
    }

    public static void addParentNodeRefClause(StringBuilder query, NodeRef nodeRef) {
        query.append("PARENT:\"");
        query.append(escape(nodeRef.toString()));
        query.append("\"");
    }

    public static void addPropertyMatchClause(StringBuilder query, QName property, String value) {
        query.append("@");
        query.append(escape(property));
        query.append(":");
        query.append(escape(value));
    }

    public static void addAndClause(StringBuilder query) {
        query.append(" AND ");
    }

    public static void addOrClause(StringBuilder query) {
        query.append(" OR ");
    }

    private static String escape(final QName qName) {
        return escape(qName.toString());
    }

    private static String escape(String value) {
        final StringBuilder builder = new StringBuilder(value.length() + 4);
        for (int i = 0; i < value.length(); i++) {
            char c = value.charAt(i);
            if ((c == '{') || (c == '}') || (c == ':') || (c == '-')) {
                builder.append('\\');
            }
            builder.append(c);
        }

        return builder.toString();
    }
}
