/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderNodeDto;
import ee.lifeup.flowhub.alfresco.rest.folder.service.FolderService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.rest.utils.RestDataSourceResponse;
import ee.lifeup.flowhub.alfresco.rest.utils.WebParameter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Web script for searching folders.
 */
@Component("webscript.lifeup.flowhub.folder.folder.search.get")
public class FolderSearchGetWebScript extends AbstractBaseWebScript {

    @Autowired
    private FolderService folderService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) {
        try {
            NodeRef parentNodeRef = extractNodeRefFromRequest(request, LifeupConst.PARENT_NODE_REF_PARAMETER);
            String query = getParameter(request, LifeupConst.QUERY_PARAMETER, WebParameter.NON_MANDATORY);
            List<FolderNodeDto> folderNodesDto = folderService.search(parentNodeRef, query);
            writeResponse(response, new RestDataSourceResponse<>(folderNodesDto), HttpStatus.SC_OK);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to get folder, invalid nodeRef specified", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to get folder, nodeRef not found", e);
        }
    }
}
