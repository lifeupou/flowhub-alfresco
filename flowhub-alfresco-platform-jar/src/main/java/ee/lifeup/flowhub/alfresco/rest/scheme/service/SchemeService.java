/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.service;

import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.*;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.util.Pair;

import java.util.Date;
import java.util.List;

/**
 * Created by Aleksandr Koltakov on 05.07.2018
 */

public interface SchemeService {

    NodeRef getRootSchemeNodeRef();

    NodeRef create(SchemeCreationRequest request);

    Scheme get(NodeRef nodeRef);

    void update(NodeRef nodeRef, SchemeUpdateRequest request);

    void delete(NodeRef nodeRef);

    /**
     * Return parent scheme of given nodeRef (if any, otherwise null)
     */
    NodeRef getScheme(NodeRef childNodeRef);

    boolean isScheme(NodeRef nodeRef);

    int getCounter(NodeRef nodeRef);

    void incrementCounter(NodeRef nodeRef);

    String getFolderSeparator(NodeRef nodeRef);

    Pair<Date, Date> getOpenCloseDates(NodeRef nodeRef);

    /**
     * Return scheme nodes.
     * @param nodeRef folder node reference.
     */
    List<SchemeNodeDto> getSchemeNodes(NodeRef nodeRef);

    /**
     * Return scheme nodes by organization.
     * @param organizationRef organization node reference.
     */
    List<SchemeNodeDto> getSchemeNodesByOrganization(NodeRef organizationRef);

    void close(NodeRef nodeRef);

    void reopen(NodeRef nodeRef);

    NodeRef getSchemesFolder(String parameter);

    PageList<Scheme> list(ListRequest listRequest);
}
