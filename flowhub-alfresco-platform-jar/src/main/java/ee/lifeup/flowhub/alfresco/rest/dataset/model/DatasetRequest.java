/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.model;

import org.codehaus.jackson.JsonNode;

import java.util.Objects;

/**
 * Dataset request.
 */
public class DatasetRequest {
    protected String datasetDefinitionRef;
    protected String datasetType;
    protected String datasetName;
    protected String datasetTitle;
    protected String datasetDescription;
    protected JsonNode datasetData;

    public String getDatasetDefinitionRef() {
        return datasetDefinitionRef;
    }

    public void setDatasetDefinitionRef(String datasetDefinitionRef) {
        this.datasetDefinitionRef = datasetDefinitionRef;
    }

    public String getDatasetType() {
        return datasetType;
    }

    public void setDatasetType(String datasetType) {
        this.datasetType = datasetType;
    }

    public String getDatasetName() {
        return datasetName;
    }

    public void setDatasetName(String datasetName) {
        this.datasetName = datasetName;
    }

    public String getDatasetTitle() {
        return datasetTitle;
    }

    public void setDatasetTitle(String datasetTitle) {
        this.datasetTitle = datasetTitle;
    }

    public String getDatasetDescription() {
        return datasetDescription;
    }

    public void setDatasetDescription(String datasetDescription) {
        this.datasetDescription = datasetDescription;
    }

    public JsonNode getDatasetData() {
        return datasetData;
    }

    public void setDatasetData(JsonNode datasetData) {
        this.datasetData = datasetData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatasetRequest)) return false;
        DatasetRequest that = (DatasetRequest) o;
        return Objects.equals(getDatasetDefinitionRef(), that.getDatasetDefinitionRef()) &&
                Objects.equals(getDatasetType(), that.getDatasetType()) &&
                Objects.equals(getDatasetName(), that.getDatasetName()) &&
                Objects.equals(getDatasetTitle(), that.getDatasetTitle()) &&
                Objects.equals(getDatasetDescription(), that.getDatasetDescription()) &&
                Objects.equals(getDatasetData(), that.getDatasetData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDatasetDefinitionRef(), getDatasetType(), getDatasetName(), getDatasetTitle(), getDatasetDescription(), getDatasetData());
    }
}
