/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.web;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileParameter;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileRequest;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.service.DatasetFileService;
import ee.lifeup.flowhub.alfresco.rest.utils.WebParameter;
import ee.lifeup.flowhub.alfresco.service.FileUploadService;
import org.alfresco.service.cmr.repository.DuplicateChildNodeNameException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Web script for creating a dataset file.
 */
@Component("webscript.lifeup.flowhub.datasetfile.datasetfile.post")
public class DatasetFileCreateWebScript extends DatasetFileBaseWebScript {

    @Autowired
    private DatasetFileService datasetFileService;
    @Autowired
    private FileUploadService fileUploadService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            NodeRef datasetNodeRef = extractNodeRefFromRequest(request, DatasetFileParameter.DATASET_NODE_REF.getName());
            String datasetFileName = getParameter(request, DatasetFileParameter.DATASET_FILE_NAME.getName(), WebParameter.MANDATORY);
            DatasetFileRequest createRequest = createDatasetFileRequest(request);
            NodeRef datasetFileRef = datasetFileService.create(datasetNodeRef, createRequest);
            Content content = getContentFromRequest(request, DatasetFileParameter.DATASET_FILE_DATA.getName());
            fileUploadService.uploadFileToNodeRef(datasetFileRef, datasetFileName, content.getInputStream());
            writeResponse(response, datasetFileRef.toString(), HttpStatus.SC_CREATED);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create case dataset file", e);
        } catch (DuplicateChildNodeNameException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create case dataset file, duplicate name", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to create case dataset file, parent nodeRef not found", e);
        }
    }
}
