/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casedefinition.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.model.CaseDefinition;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.model.CaseDefinitionRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LocalizationUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Case service implementation.
 */
@Service
public class CaseDefinitionServiceImpl extends AbstractServiceImpl implements CaseDefinitionService {

    @Value("${flowhub.case.definition.root.path}")
    private String caseDefinitionRootFolderPath;

    @Override
    public NodeRef create(String caseDefinitionRef, CaseDefinitionRequest caseDefinitionRequest) {
        log.info("We've got a case definition creation request");
        Preconditions.checkNotNull(caseDefinitionRequest, "case definition request must not be null");
        Preconditions.checkArgument(StringUtils.isNotBlank(caseDefinitionRef), "case definiton reference must not be null or empty");

        Map<String, String> caseDefinitionNames = caseDefinitionRequest.getNames();
        String caseDefinitionName = caseDefinitionNames.get(LocalizationUtils.DEFAULT_LOCALE.getLanguage());

        NodeRef parentNodeRef = getRootNodeRef(caseDefinitionRootFolderPath);
        FileInfo fileInfo = fileFolderService.create(parentNodeRef, caseDefinitionName, ContentModel.TYPE_FOLDER);
        NodeRef caseDefinitionNodeRef = fileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        addOrUpdateProperties(caseDefinitionNames, caseDefinitionRef, caseDefinitionRequest, properties);

        nodeService.addAspect(caseDefinitionNodeRef, LifeupContentModel.ASPECT_CASE_DEFINITION, properties);
        nodeService.addAspect(caseDefinitionNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);
        return caseDefinitionNodeRef;
    }

    @Override
    public CaseDefinition get(NodeRef nodeRef) {
        log.info("Going to read case definition " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case definition node reference must not be null");
        assertNodeIsCaseDefinition(nodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);

        CaseDefinition caseDefinition = new CaseDefinition();
        caseDefinition.setCaseDefinitionIdentifier(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        caseDefinition.setNodeRef(nodeRef.toString());
        caseDefinition.setCaseDefinitionName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        caseDefinition.setCaseDefinitionTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        caseDefinition.setCaseDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));
        caseDefinition.setCaseDefinitionReference(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DEFINITION_REF));
        caseDefinition.setCaseDefinitionData(ParseUtils.getJsonNode(properties, LifeupContentModel.PROPERTY_CASE_DEFINITION_DATA));
        fillAuditFields(properties, caseDefinition);
        return caseDefinition;
    }

    @Override
    public void update(NodeRef nodeRef, String caseDefinitionRef, CaseDefinitionRequest updateRequest) {
        log.info("Going to update case definition " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case definition node reference must not be null");
        Preconditions.checkArgument(StringUtils.isNotBlank(caseDefinitionRef), "case definiton reference must not be null or empty");
        Preconditions.checkNotNull(updateRequest, "case definition request must not be null");
        assertNodeIsCaseDefinition(nodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        addOrUpdateProperties(updateRequest.getNames(), caseDefinitionRef, updateRequest, properties);
        nodeService.setProperties(nodeRef, properties);
    }

    @Override
    public PageList<CaseDefinition> list(ListRequest listRequest) {
        log.info("Going to read case definitions");
        return getPageList(listRequest, this::get);
    }

    private void addOrUpdateProperties(Map<String, String> caseDefinitionNames, String caseDefinitionRef, CaseDefinitionRequest caseDefinitionRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_TITLE, LocalizationUtils.createMultiLanguageText(caseDefinitionNames));
        properties.put(ContentModel.PROP_DESCRIPTION, caseDefinitionRequest.getDescription());
        properties.put(LifeupContentModel.PROPERTY_CASE_DEFINITION_DATA, caseDefinitionRequest.getData().toString());
        properties.put(LifeupContentModel.PROPERTY_CASE_DEFINITION_REF, caseDefinitionRef);
    }

    private void assertNodeIsCaseDefinition(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_CASE_DEFINITION)) {
            throw new IllegalArgumentException("NodeRef should be a case definition");
        }
    }
}
