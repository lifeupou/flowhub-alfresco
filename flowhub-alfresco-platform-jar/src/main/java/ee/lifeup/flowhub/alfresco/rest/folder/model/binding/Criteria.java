/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.model.binding;

import org.alfresco.service.cmr.repository.NodeRef;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Aleksandr Koltakov on 20.07.2018
 */

public class Criteria {
    private Map<NodeRef, List<Rule>> criterionMap;

    public Map<NodeRef, List<Rule>> getCriterionMap() {
        return criterionMap;
    }

    public void setCriterionMap(Map<NodeRef, List<Rule>> criterionMap) {
        this.criterionMap = criterionMap;
    }

    public List<Rule> getRulesByProfile(NodeRef nodeRef) {
        return criterionMap.get(nodeRef);
    }

    public void addRulesByProfile(NodeRef nodeRef, List<Rule> rules) {
        if (!criterionMap.containsKey(nodeRef)) {
            criterionMap.put(nodeRef, new ArrayList<>());
        }

        criterionMap.get(nodeRef).addAll(rules);
    }
}
