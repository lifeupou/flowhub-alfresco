/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casedefinition.model;

import ee.lifeup.flowhub.alfresco.model.Auditable;
import org.codehaus.jackson.JsonNode;

import java.util.Objects;

/**
 * Case Definition Model.
 */
public class CaseDefinition implements Auditable {
    private String caseDefinitionReference;
    private String caseDefinitionIdentifier;
    private String caseDefinitionTitle;
    private String caseDefinitionName;
    private String caseDescription;
    private JsonNode caseDefinitionData;
    private String nodeRef;
    private String created;
    private String creator;
    private String modified;
    private String modifier;

    public String getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(String nodeRef) {
        this.nodeRef = nodeRef;
    }

    public String getCreated() {
        return created;
    }

    @Override
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModified() {
        return modified;
    }

    @Override
    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getCaseDefinitionReference() {
        return caseDefinitionReference;
    }

    public void setCaseDefinitionReference(String caseDefinitionReference) {
        this.caseDefinitionReference = caseDefinitionReference;
    }

    public String getCaseDefinitionTitle() {
        return caseDefinitionTitle;
    }

    public void setCaseDefinitionTitle(String caseDefinitionTitle) {
        this.caseDefinitionTitle = caseDefinitionTitle;
    }

    public String getCaseDefinitionIdentifier() {
        return caseDefinitionIdentifier;
    }

    public void setCaseDefinitionIdentifier(String caseDefinitionIdentifier) {
        this.caseDefinitionIdentifier = caseDefinitionIdentifier;
    }

    public String getCaseDefinitionName() {
        return caseDefinitionName;
    }

    public void setCaseDefinitionName(String caseDefinitionName) {
        this.caseDefinitionName = caseDefinitionName;
    }

    public String getCaseDescription() {
        return caseDescription;
    }

    public void setCaseDescription(String caseDescription) {
        this.caseDescription = caseDescription;
    }

    public JsonNode getCaseDefinitionData() {
        return caseDefinitionData;
    }

    public void setCaseDefinitionData(JsonNode caseDefinitionData) {
        this.caseDefinitionData = caseDefinitionData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseDefinition)) return false;
        CaseDefinition that = (CaseDefinition) o;
        return Objects.equals(getCaseDefinitionReference(), that.getCaseDefinitionReference()) &&
                Objects.equals(getCaseDefinitionIdentifier(), that.getCaseDefinitionIdentifier()) &&
                Objects.equals(getCaseDefinitionTitle(), that.getCaseDefinitionTitle()) &&
                Objects.equals(getCaseDefinitionName(), that.getCaseDefinitionName()) &&
                Objects.equals(getCaseDescription(), that.getCaseDescription()) &&
                Objects.equals(getCaseDefinitionData(), that.getCaseDefinitionData()) &&
                Objects.equals(getNodeRef(), that.getNodeRef()) &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getCreator(), that.getCreator()) &&
                Objects.equals(getModified(), that.getModified()) &&
                Objects.equals(getModifier(), that.getModifier());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCaseDefinitionReference(), getCaseDefinitionIdentifier(), getCaseDefinitionTitle(), getCaseDefinitionName(), getCaseDescription(), getCaseDefinitionData(), getNodeRef(), getCreated(), getCreator(), getModified(), getModifier());
    }
}
