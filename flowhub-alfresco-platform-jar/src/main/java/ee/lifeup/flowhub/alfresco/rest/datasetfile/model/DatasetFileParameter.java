/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.model;

/**
 * Dataset file parameter enum.
 */
public enum DatasetFileParameter {
    DATASET_NODE_REF("datasetNodeRef"),
    DATASET_FILE_REF("datasetFileRef"),
    DATASET_FILE_DATA("datasetFileData"),
    DATASET_FILE_TYPE("datasetFileType"),
    DATASET_FILE_NAME("datasetFileName"),
    DATASET_FILE_TITLE("datasetFileTitle"),
    DATASET_FILE_DESCRIPTION("datasetFileDescription"),
    DATASET_FILE_METADATA("datasetFileMetadata");

    private String name;

    DatasetFileParameter(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
