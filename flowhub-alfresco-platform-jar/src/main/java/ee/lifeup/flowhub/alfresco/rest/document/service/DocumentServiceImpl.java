package ee.lifeup.flowhub.alfresco.rest.document.service;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.dataset.service.DatasetContentNodeService;
import ee.lifeup.flowhub.alfresco.rest.dataset.service.DatasetService;
import ee.lifeup.flowhub.alfresco.rest.document.model.RegistrationDetails;
import ee.lifeup.flowhub.alfresco.rest.folder.service.FolderService;
import ee.lifeup.flowhub.alfresco.rest.globalcounter.service.GlobalCounterService;
import ee.lifeup.flowhub.alfresco.rest.profile.model.DocumentProfile;
import ee.lifeup.flowhub.alfresco.rest.profile.service.DocumentProfileService;
import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Aleksandr Koltakov on 17.08.2018
 */
@Service
public class DocumentServiceImpl extends AbstractServiceImpl implements DocumentService {

    @Autowired
    private DocumentProfileService documentProfileService;

    @Autowired
    private FolderService folderService;

    @Autowired
    private DatasetService datasetService;

    @Autowired
    private DatasetContentNodeService datasetContentNodeService;

    @Autowired
    private SchemeService schemeService;

    @Autowired
    private GlobalCounterService globalCounterService;

    @Override
    public NodeRef createDocument(String documentProfileName, NodeRef caseInstanceNodeRef) {
        DocumentProfile documentProfile = documentProfileService.get(documentProfileName);
        String dataSetRef = documentProfile.getDocumentProfileDataSet();
        NodeRef dataSetNodeRef = datasetService.getDatasetNodeRef(caseInstanceNodeRef, dataSetRef);

        if (hasAnyAspect(dataSetNodeRef, LifeupContentModel.ASPECT_CASE_DOCUMENT)) {
            throw new IllegalArgumentException("Dataset with nodeRef " + dataSetRef + " already has DOCUMENT aspect");
        }

        JsonNode jsonNode = datasetContentNodeService.getDataPropertyValue(dataSetNodeRef);
        NodeRef documentProfileNodeRef = new NodeRef(documentProfile.getNodeRef());

        List<NodeRef> folderNodeRefs = documentProfileService.getAssociatedFolders(documentProfileNodeRef);
        if (CollectionUtils.isEmpty(folderNodeRefs)) {
            throw new IllegalArgumentException("No folder bound with this profile");
        }

        // this is NOT object mapper used in AbstractService, it's another implementation
        Map<String, Object> datasetData = new ObjectMapper().convertValue(jsonNode, Map.class);
        NodeRef targetFolderNodeRef = folderService.getMatchedFolder(folderNodeRefs, documentProfileNodeRef, datasetData);
        if (targetFolderNodeRef == null) {
            throw new IllegalArgumentException("No matched folder found");
        }

        Map<QName, Serializable> aspectProperties = new HashMap<>(2);
        aspectProperties.put(LifeupContentModel.PROPERTY_CASE_DOCUMENT_PROFILE_REF, documentProfileNodeRef);
        aspectProperties.put(LifeupContentModel.PROPERTY_CASE_DOCUMENT_REGISTRATION_FOLDER_REF, targetFolderNodeRef);
        nodeService.addAspect(dataSetNodeRef, LifeupContentModel.ASPECT_CASE_DOCUMENT, aspectProperties);

        inheritPermissions(targetFolderNodeRef, dataSetNodeRef);
        return dataSetNodeRef;
    }


    @Override
    public RegistrationDetails registerDocument(String documentProfileName, NodeRef caseInstanceNodeRef) {
        DocumentProfile documentProfile = documentProfileService.get(documentProfileName);
        String dataSetRef = documentProfile.getDocumentProfileDataSet();
        NodeRef dataSetNodeRef = datasetService.getDatasetNodeRef(caseInstanceNodeRef, dataSetRef);

        if (dataSetNodeRef == null) {
            throw new IllegalArgumentException("Dataset does not exists any more within specified case instance. Document already has been registered?");
        }

        if (!hasAnyAspect(dataSetNodeRef, LifeupContentModel.ASPECT_CASE_DOCUMENT)) {
            throw new IllegalArgumentException("Dataset with nodeRef " + dataSetRef + " does not have DOCUMENT aspect");
        }

        NodeRef targetFolderNodeRef = new NodeRef((String) nodeService.getProperty(dataSetNodeRef, LifeupContentModel.PROPERTY_CASE_DOCUMENT_REGISTRATION_FOLDER_REF));
        String dataSetName = (String) nodeService.getProperty(dataSetNodeRef, ContentModel.PROP_NAME);
        dataSetName = dataSetName + '-' + System.currentTimeMillis();
        nodeService.setProperty(dataSetNodeRef, ContentModel.PROP_NAME, dataSetName);

        NodeRef schemeNodeRef = schemeService.getScheme(targetFolderNodeRef);
        if (schemeNodeRef == null) {
            throw new IllegalArgumentException("Unable to find parent scheme of folder " + targetFolderNodeRef);
        }

        String documentNumber = new StringBuilder()
                .append(nodeService.getProperty(targetFolderNodeRef, LifeupContentModel.PROPERTY_FOLDER_ID))
                .append(nodeService.getProperty(schemeNodeRef, LifeupContentModel.PROPERTY_SCHEME_ID_DOCUMENT_SEPARATOR))
                .append(getCounterAndIncrement(targetFolderNodeRef))
                .toString();


        Map<QName, Serializable> aspectProperties = new HashMap<>(2);
        Date now = new Date();
        aspectProperties.put(LifeupContentModel.PROPERTY_CASE_DOCUMENT_REGISTRATION_DATE, now);
        aspectProperties.put(LifeupContentModel.PROPERTY_CASE_DOCUMENT_REGISTRATION_NUMBER, documentNumber);

        RegistrationDetails registrationDetails = new RegistrationDetails();
        registrationDetails.setRegistrationDate(now);
        registrationDetails.setRegistrationNumber(documentNumber);

        inheritPermissions(targetFolderNodeRef, dataSetNodeRef);
        ChildAssociationRef childAssociationRef = this.nodeService.getPrimaryParent(dataSetNodeRef);
        nodeService.moveNode(dataSetNodeRef, targetFolderNodeRef, ContentModel.ASSOC_CONTAINS, childAssociationRef.getQName());
        nodeService.addAspect(dataSetNodeRef, LifeupContentModel.ASPECT_CASE_DOCUMENT_REGISTRATION, aspectProperties);

        return registrationDetails;
    }

    private int getCounterAndIncrement(NodeRef targetFolderNodeRef) {
        String folderGlobalCounterRefProperty = (String) nodeService.getProperty(targetFolderNodeRef, LifeupContentModel.PROPERTY_FOLDER_GLOBAL_COUNTER_REF);
        return StringUtils.isEmpty(folderGlobalCounterRefProperty)
                ? folderService.getCounterAndIncrement(targetFolderNodeRef)
                : globalCounterService.getAndIncrement(new NodeRef(folderGlobalCounterRefProperty));
    }
}
