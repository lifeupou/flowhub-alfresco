/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.model;

import ee.lifeup.flowhub.alfresco.model.Auditable;

import java.util.Objects;

/**
 * Dataset model.
 */
public class Dataset extends DatasetRequest implements Auditable {
    private String datasetIdentifier;
    private String nodeRef;
    private String status;
    private String created;
    private String creator;
    private String modified;
    private String modifier;

    public String getDatasetIdentifier() {
        return datasetIdentifier;
    }

    public void setDatasetIdentifier(String datasetIdentifier) {
        this.datasetIdentifier = datasetIdentifier;
    }

    public String getNodeRef() {
        return nodeRef;
    }

    public void setNodeRef(String nodeRef) {
        this.nodeRef = nodeRef;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreated() {
        return created;
    }

    @Override
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModified() {
        return modified;
    }

    @Override
    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Dataset)) return false;
        if (!super.equals(o)) return false;
        Dataset dataset = (Dataset) o;
        return Objects.equals(getDatasetIdentifier(), dataset.getDatasetIdentifier()) &&
                Objects.equals(getNodeRef(), dataset.getNodeRef()) &&
                Objects.equals(getStatus(), dataset.getStatus()) &&
                Objects.equals(getCreated(), dataset.getCreated()) &&
                Objects.equals(getCreator(), dataset.getCreator()) &&
                Objects.equals(getModified(), dataset.getModified()) &&
                Objects.equals(getModifier(), dataset.getModifier());
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), getDatasetIdentifier(), getNodeRef(), getStatus(), getCreated(), getCreator(), getModified(), getModifier());
    }
}
