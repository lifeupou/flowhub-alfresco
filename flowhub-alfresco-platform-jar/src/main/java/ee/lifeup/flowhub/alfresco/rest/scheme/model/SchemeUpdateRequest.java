/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.model;

/**
 * Created by Aleksandr Koltakov on 06.07.2018
 */

public class SchemeUpdateRequest {
    private String schemeName;
    private String schemeResponsible;
    private String schemeDescription;
    private String schemeOpenDate;
    private String schemeCloseDate;
    private String schemeNationalArchiveApprovalDate;

    public String getSchemeName() {
        return schemeName;
    }

    public void setSchemeName(String schemeName) {
        this.schemeName = schemeName;
    }

    public String getSchemeResponsible() {
        return schemeResponsible;
    }

    public void setSchemeResponsible(String schemeResponsible) {
        this.schemeResponsible = schemeResponsible;
    }

    public String getSchemeDescription() {
        return schemeDescription;
    }

    public void setSchemeDescription(String schemeDescription) {
        this.schemeDescription = schemeDescription;
    }

    public String getSchemeOpenDate() {
        return schemeOpenDate;
    }

    public void setSchemeOpenDate(String schemeOpenDate) {
        this.schemeOpenDate = schemeOpenDate;
    }

    public String getSchemeCloseDate() {
        return schemeCloseDate;
    }

    public void setSchemeCloseDate(String schemeCloseDate) {
        this.schemeCloseDate = schemeCloseDate;
    }

    public String getSchemeNationalArchiveApprovalDate() {
        return schemeNationalArchiveApprovalDate;
    }

    public void setSchemeNationalArchiveApprovalDate(String schemeNationalArchiveApprovalDate) {
        this.schemeNationalArchiveApprovalDate = schemeNationalArchiveApprovalDate;
    }
}
