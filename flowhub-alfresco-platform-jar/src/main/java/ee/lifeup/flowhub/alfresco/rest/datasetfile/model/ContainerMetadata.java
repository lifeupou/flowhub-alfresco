/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.model;

import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * BDOC container metadata.
 */
public class ContainerMetadata {

    /** Container file names. */
    private List<String> fileNames;
    /** Signature metadata. */
    private List<SignatureMetadata> signatureMetadata;

    public ContainerMetadata() {
    }

    public ContainerMetadata(List<String> fileNames, List<SignatureMetadata> signatureMetadata) {
        this.fileNames = fileNames;
        this.signatureMetadata = signatureMetadata;
    }

    public List<String> getFileNames() {
        return fileNames;
    }

    public void setFileNames(List<String> fileNames) {
        this.fileNames = fileNames;
    }

    public List<SignatureMetadata> getSignatureMetadata() {
        return signatureMetadata;
    }

    public void setSignatureMetadata(List<SignatureMetadata> signatureMetadata) {
        this.signatureMetadata = signatureMetadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ContainerMetadata)) return false;
        ContainerMetadata that = (ContainerMetadata) o;
        return Objects.equals(getFileNames(), that.getFileNames()) &&
                Objects.equals(getSignatureMetadata(), that.getSignatureMetadata());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFileNames(), getSignatureMetadata());
    }

    public static class SignatureMetadata {

        /** Certificate subject. */
        private String certificate;
        /** Certificate issuer. */
        private String certificateIssuer;
        /** Signer's role or resolution. */
        private String resolution;
        /** OSCP time. */
        private Date oscpTime;

        public SignatureMetadata() {
        }

        public SignatureMetadata(String certificate, String certificateIssuer, String resolution, Date oscpTime) {
            this.certificate = certificate;
            this.certificateIssuer = certificateIssuer;
            this.resolution = resolution;
            this.oscpTime = oscpTime;
        }

        public String getCertificate() {
            return certificate;
        }

        public void setCertificate(String certificate) {
            this.certificate = certificate;
        }

        public String getCertificateIssuer() {
            return certificateIssuer;
        }

        public void setCertificateIssuer(String certificateIssuer) {
            this.certificateIssuer = certificateIssuer;
        }

        public String getResolution() {
            return resolution;
        }

        public void setResolution(String resolution) {
            this.resolution = resolution;
        }

        public Date getOscpTime() {
            return oscpTime;
        }

        public void setOscpTime(Date oscpTime) {
            this.oscpTime = oscpTime;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof SignatureMetadata)) return false;
            SignatureMetadata that = (SignatureMetadata) o;
            return Objects.equals(getCertificate(), that.getCertificate()) &&
                    Objects.equals(getCertificateIssuer(), that.getCertificateIssuer()) &&
                    Objects.equals(getResolution(), that.getResolution()) &&
                    Objects.equals(getOscpTime(), that.getOscpTime());
        }

        @Override
        public int hashCode() {

            return Objects.hash(getCertificate(), getCertificateIssuer(), getResolution(), getOscpTime());
        }
    }
}
