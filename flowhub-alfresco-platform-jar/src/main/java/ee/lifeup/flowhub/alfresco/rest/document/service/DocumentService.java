package ee.lifeup.flowhub.alfresco.rest.document.service;

import ee.lifeup.flowhub.alfresco.rest.document.model.RegistrationDetails;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Created by Aleksandr Koltakov on 17.08.2018
 */

public interface DocumentService {

    NodeRef createDocument(String documentProfileName, NodeRef caseInstanceNodeRef);

    RegistrationDetails registerDocument(String documentProfileName, NodeRef caseInstanceNodeRef);
}
