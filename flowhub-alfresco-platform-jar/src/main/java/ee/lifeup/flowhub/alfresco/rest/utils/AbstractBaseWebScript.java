/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.utils;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.exception.PreconditionFailedException;
import ee.lifeup.flowhub.alfresco.exception.WebScriptParameterException;
import org.alfresco.service.ServiceRegistry;
import org.alfresco.service.cmr.repository.MalformedNodeRefException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.StoreRef;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.AbstractWebScript;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.extensions.webscripts.servlet.FormData;

import javax.annotation.Nullable;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Abstract base web script.<br>
 * Provides common handling for authentication, execution and errors.
 * legacy
 */
// NOSONAR
public abstract class AbstractBaseWebScript extends AbstractWebScript {
    public static final String DEFAULT_RESPONSE_ENCODING = "UTF-8";

    protected static final String PARAMETER_NODE_REF = "nodeRef";

    protected static final Logger LOGGER = Logger.getLogger(AbstractBaseWebScript.class);

    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    protected NodeService nodeService;

    @Autowired
    protected ServiceRegistry serviceRegistry;

    /**
     * {@inheritDoc}
     */
    @Override
    public final void execute(WebScriptRequest req, WebScriptResponse res) throws IOException {

        String webscriptName = getClass().getSimpleName();
        long startTime = 0;
        String errorDescription = "unknown cause";

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("-----------------------------------------");
            LOGGER.debug(webscriptName + " webscript started...");
            LOGGER.debug("Template vars: " + req.getServiceMatch().getTemplateVars());
            LOGGER.debug("Parameter names: " + Arrays.asList(req.getParameterNames()));
            startTime = System.nanoTime();
        }

        try {

            // check for proxied user request
            handleAuthentication(req, res);

            res.setContentEncoding(DEFAULT_RESPONSE_ENCODING);

            // call for subclass logic
            executeImpl(req, res);
            errorDescription = StringUtils.EMPTY;

        } catch (Exception t) {
            LOGGER.error(requestToString(req));
            errorDescription = t.getClass().getSimpleName();
            String msg = "Error executing webscript " + webscriptName + ": " + t.getLocalizedMessage();

            if (t instanceof WebScriptException) {
                errorDescription = getErrorDescription(new StringBuilder(), t).toString();

                WebScriptException wsException = (WebScriptException) t;
                switch (wsException.getStatus()) {
                    case HttpStatus.SC_NOT_FOUND:
                    case HttpStatus.SC_BAD_REQUEST:
                    case HttpStatus.SC_FORBIDDEN:
                        errorDescription = errorDescription + wsException.getStatus();
                        LOGGER.warn(msg);
                        break;
                    default:
                        // For all other codes we log an error with stacktrace
                        LOGGER.error(msg, t);
                        break;
                }

                throw new WebScriptException(((WebScriptException) t).getStatus(), errorDescription);
            } else if (t instanceof WebScriptParameterException) {
                LOGGER.warn(msg);
                WebScriptParameterException wsException = (WebScriptParameterException) t;
                writeResponse(res, wsException.getResponseObjects(), wsException.getResponseStatus());

            } else {
                LOGGER.error(msg, t);
                throw new WebScriptException(msg, t);
            }
        } finally {
            if (LOGGER.isDebugEnabled()) {
                long stopTime = System.nanoTime();
                double seconds = ((double) stopTime - startTime) / 1000000000.0;
                LOGGER.debug("Execution time of webscript: " + seconds + " sec");
                LOGGER.debug(webscriptName + " webscript ended"
                        + (errorDescription == null ? " successfully" : " with " + errorDescription));
                LOGGER.debug("-----------------------------------------");
            }
        }
    }

    private StringBuilder getErrorDescription(StringBuilder builder, Throwable throwable) {
        return getErrorDescription(builder, throwable, true);
    }

    private StringBuilder getErrorDescription(StringBuilder builder, Throwable throwable, boolean addInner) {
        builder
                .append(" -> ")
                .append(throwable.getMessage());
        Throwable cause = throwable.getCause();
        return cause == null || !addInner ? builder : getErrorDescription(builder, cause, cause instanceof WebScriptException);
    }

    /**
     * Transform webscript request to string.
     *
     * @param req webscript request.
     * @return request as string
     */
    public static String requestToString(WebScriptRequest req) {
        StrBuilder strBuilder = new StrBuilder();
        strBuilder.appendln("WebScript execution error. Request info:");
        strBuilder.appendln("url = " + req.getPathInfo() + "?" + req.getQueryString());
        strBuilder.appendln("content-type = " + req.getContentType());
        strBuilder.appendln("Params: ");
        for (String name : req.getParameterNames()) {
            String value = req.getParameter(name);
            strBuilder.append(name).append("='").append(value).append("' ");
        }

        return strBuilder.toString();
    }

    /**
     * Provides possibility to execute the script under specified user account -
     * used as lightweight alternative to Alfresco external authentication.
     *
     * @param aRequest  web script request
     * @param aResponse web script response
     * @throws WebScriptException attempt to authenticate admin or System was made
     */
    protected void handleAuthentication(WebScriptRequest aRequest, WebScriptResponse aResponse) {
        // Void
    }

    protected abstract void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception;

    /**
     * Create Repository Node ref by id
     *
     * @param nodeId node id value
     * @return node ref of node being processed
     */
    protected NodeRef getNodeRefByID(String nodeId) {

        return new NodeRef(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE, nodeId);
    }

    /**
     * Returns repository NodeRef by id if it exists. If it doesn't, exception will be thrown.
     *
     * @return repository NodeRef.
     * @throws WebScriptException if NodeRef does not exist.
     */
    protected NodeRef getExistingNodeRefById(String nodeRefId) {
        NodeRef nodeRef = getNodeRefByID(nodeRefId);
        if (!nodeService.exists(nodeRef)) {
            LOGGER.warn("NodeRef is not exist." + nodeRef);
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, String.format("NodeRef with id %s is not exist", nodeRefId));
        }
        return nodeRef;
    }

    /**
     * Check input as Repository Node ref representation
     *
     * @param nodeValue node id value
     * @return Boolean true , if value is full Repository node ref representation
     */
    protected boolean isFullNodeRef(String nodeValue) {
        return StringUtils.isNotEmpty(nodeValue)
                && nodeValue.contains(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE.toString());
    }

    /**
     * Extract Web request parameter by name as not mandatory parameter
     *
     * @param aRequest      Web request
     * @param parameterName Web request Parameter
     * @return String parameter value
     */
    public String getParameter(WebScriptRequest aRequest, String parameterName) {

        return getParameter(aRequest, parameterName, WebParameter.NON_MANDATORY);
    }

    /**
     * Extract Web request parameter by name
     *
     * @param aRequest      Web request
     * @param parameterName Web request Parameter
     * @param presence      Parameter presence at Request
     * @return String parameter value
     */
    public String getParameter(WebScriptRequest aRequest, String parameterName, WebParameter presence) {

        return WebScriptUtils.getParameterValue(aRequest, parameterName, presence);
    }

    /**
     * Create Repository Node ref by Web request content
     *
     * @param aRequest      Web request
     * @param parameterName Web request Parameter
     * @param checkExists   allowed to check NodeRef for presence at Repository
     * @param presence      Parameter presence at Request
     * @return node ref of node being processed
     */
    public NodeRef getNodeRefByParameterName(WebScriptRequest aRequest, String parameterName, boolean checkExists,
                                             WebParameter presence) {

        NodeRef result = null;
        String nodeRefStr = WebScriptUtils.getParameterValue(aRequest, parameterName, presence);

        if (StringUtils.isNotEmpty(nodeRefStr)) {
            try {
                result = isFullNodeRef(nodeRefStr) ? new NodeRef(nodeRefStr) : getNodeRefByID(nodeRefStr);
                if (checkExists && !serviceRegistry.getNodeService().exists(result)) {
                    throw new WebScriptParameterException("Node not exists", nodeRefStr,
                            "webscripts.common.node-not-found", HttpStatus.SC_NOT_FOUND);
                }
            } catch (MalformedNodeRefException ex) {
                throw new WebScriptParameterException("Node has invalid format", nodeRefStr,
                        "webscripts.common.invalid-node-format", HttpStatus.SC_BAD_REQUEST);
            }
        }
        return result;
    }

    /**
     * De-Serializes sent JSON object.
     *
     * @param request web-script request
     * @param type    Type of the value
     * @param <T>     Class of the return type
     * @return instance of parameter type
     */
    protected <T> T getParameterJSONBean(WebScriptRequest request, Class<T> type) {
        return WebScriptUtils.getParameterJSONBean(request, type, OBJECT_MAPPER);
    }

    /**
     * De-Serializes sent JSON object.
     *
     * @param request web-script request
     * @return instance of parameter type
     */
    protected Map<String, Object> getParameterJSONAsMap(WebScriptRequest request) {
        return WebScriptUtils.getParameterJSONAsMap(request, OBJECT_MAPPER);
    }

    /**
     * De-Serializes sent JSON object.
     *
     * @param jsonNode jsonNode
     * @return instance of parameter type
     */
    protected Map<String, Object> getParameterJSONAsMap(JsonNode jsonNode) {
        return WebScriptUtils.getParameterJSONAsMap(jsonNode, OBJECT_MAPPER);
    }

    /**
     * De-Serializes JSON object as List of properties.
     *
     * @param request      web-script request.
     * @param type         type of properties.
     * @param filedName    field name in json object.
     * @return metadata properties list.
     */
    protected <T> List<T> getParameterJSONAsList(WebScriptRequest request, String filedName, Class<T> type) {
        return WebScriptUtils.getParameterJSONAsList(request, filedName, type, OBJECT_MAPPER);
    }

    /**
     * De-Serializes JSON object as List of properties.
     *
     * @param jsonNode     jsonNode.
     * @param type         type of properties.
     * @param filedName    field name in json object.
     * @return metadata properties list.
     */
    protected <T> List<T> getParameterJSONAsList(JsonNode jsonNode, String filedName, Class<T> type, boolean mandatory) {
        return WebScriptUtils.getParameterJSONAsList(jsonNode, filedName, type, OBJECT_MAPPER, mandatory);
    }

    /**
     * De-Serializes JSON object as List of properties.
     *
     * @param content      JSON content in a string.
     * @param type         type of properties.
     * @param filedName    field name in json object.
     * @return metadata properties list.
     */
    protected <T> List<T> getParameterJSONAsList(String content, String filedName, Class<T> type, boolean mandatory) {
        return WebScriptUtils.getParameterJSONAsList(content, filedName, type, OBJECT_MAPPER, mandatory);
    }

    protected JsonNode getParameterJSONAsNode(WebScriptRequest request) {
        return WebScriptUtils.getParameterJSONAsNode(request, OBJECT_MAPPER);
    }

    /**
     * Create Repository Node ref by Web request content with checking for node
     * presence at Repository
     *
     * @param aRequest      Web request
     * @param parameterName Web request Parameter
     * @return node ref of node being processed
     */
    public NodeRef getNodeRefByParameterName(WebScriptRequest aRequest, String parameterName) {
        return getNodeRefByParameterName(aRequest, parameterName, true, WebParameter.MANDATORY);
    }

    /**
     * Converts result object to JSON structure and write to WebScript response
     * stream
     *
     * @param result   response result document
     * @param response data source response
     * @return JSON string
     */
    protected String writeResponse(WebScriptResponse response, Object result) {
        String json;
        try {
            json = OBJECT_MAPPER.writeValueAsString(result);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(getClass().getName() + " webscript returned: " + json);
            }
            response.setContentEncoding("UTF-8");
            response.getWriter().write(json);
            response.setHeader("Content-Type", "application/json; charset=utf-8");
        } catch (Exception e) {
            throw new WebScriptException("Unable to write response Json", e);
        }

        return json;
    }

    /**
     * Converts result object to JSON structure and write to WebScript response
     * stream
     *
     * @param response   Webscript response
     * @param result     response result document
     * @param httpStatus HTTP status code
     * @return JSON string
     */
    protected String writeResponse(WebScriptResponse response, Object result, int httpStatus) {
        response.setStatus(httpStatus);
        return writeResponse(response, result);
    }

    /**
     * Serialize and write to Response generic object
     *
     * @param aResponse webscript response
     * @param toWrite   generic serializable object instance
     * @param status    response status code
     * @param <T>       Response class
     */
    protected <T> void responseToWrite(WebScriptResponse aResponse, T toWrite, int status) {
        writeResponse(aResponse, new RestDataSourceResponse<T>(toWrite), status);
    }

    /**
     * Gets node reference from request URL by the specific request parameter name
     *
     * @param aRequest          webscript request
     * @param templateParameter template parameter
     * @param nodeService       node service
     * @return node reference
     */
    protected NodeRef getNodeRefFromRequestByName(WebScriptRequest aRequest, String templateParameter,
                                                  NodeService nodeService) {

        // get node ref from request URL
        String nodeRefStr = aRequest.getParameter(templateParameter);
        if (nodeRefStr == null) {
            // invalid parameters
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST,
                    "Invalid parameters: " + templateParameter + " = null");
        }

        // construct node ref
        NodeRef result = new NodeRef(nodeRefStr);
        if (result == null || !nodeService.exists(result)) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND,
                    "Node ref: " + result + " doesn't exist in repository");
        }

        return result;
    }

    protected NodeRef extractNodeRefFromRequest(WebScriptRequest request) {
        return extractNodeRefFromRequest(request, PARAMETER_NODE_REF);
    }

    protected NodeRef getNodeRefFromRequest(WebScriptRequest request) {
        NodeRef nodeRef = extractNodeRefFromRequest(request);
        if (nodeRef == null) {
            throw new PreconditionFailedException("NodeRef is not specified");
        }

        return nodeRef;
    }

    @Nullable
    protected NodeRef extractNodeRefFromRequest(WebScriptRequest request, String parameterNodRef) {
        String parameterValue = request.getParameter(parameterNodRef);
        if (StringUtils.isEmpty(parameterValue)) {
            return null;
        }

        NodeRef nodeRef = new NodeRef(parameterValue);
        if (nodeService.exists(nodeRef)) {
            return nodeRef;
        }

        LOGGER.warn("NodeRef is not exists " + parameterValue);
        throw new NodeRefNotFoundException();
    }

    protected NodeRef getNodeRefFromString(String nodeRefStr) {
        if (StringUtils.isEmpty(nodeRefStr)) {
            throw new IllegalArgumentException("nodeRef is null");
        }

        NodeRef nodeRef = new NodeRef(nodeRefStr);
        if (nodeService.exists(nodeRef)) {
            return nodeRef;
        }

        LOGGER.warn("NodeRef is not exists " + nodeRefStr);
        throw new NodeRefNotFoundException();
    }

    /**
     * Returns string value from request json object.
     *
     * @param jsonObject json object.
     * @param paramName  name of json object parameter.
     * @return sjon object parameter value.
     */
    protected static String getJsonParameter(JSONObject jsonObject, String paramName) throws JSONException {
        String param = jsonObject.getString(paramName);
        if (StringUtils.isBlank(param)) {
            String message = paramName + " parameter is not set";
            LOGGER.error(message);
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, message);
        }
        return param;
    }

    /**
     * Gets content from Web Script Request.
     *
     * @param aRequest  Web Script Request.
     * @param fieldData name of field with data.
     * @return Content
     * @throws WebScriptParameterException thrown if mandatory parameter is empty.
     */
    protected Content getContentFromRequest(WebScriptRequest aRequest, String fieldData) {
        return WebScriptUtils.getFormFieldContent((FormData) aRequest.parseContent(), fieldData, true);
    }

}
