/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.exception;

import org.alfresco.error.AlfrescoRuntimeException;
import org.apache.commons.lang.StringUtils;
import org.springframework.extensions.surf.util.I18NUtil;

import java.io.Serializable;


/**
 * Exception thrown when an illegal web script parameter is detected.
 */
public class WebScriptParameterException extends AlfrescoRuntimeException {
	
	private static final long serialVersionUID = 1876083397810863862L;
	private WebCallErrorResponse responseObjects;
    private int responseStatus;

    /**
     * Response bean for the webscript at Error case.
     */
    public static class WebCallErrorResponse implements Serializable {

        private static final long serialVersionUID = 1125634917230251041L;
        private String paramName;
        private String reasonLabelId;

        /**
         * Custom Constructor
         *
         * @param paramName
         *            Web script request parameter name
         * @param reasonLabelId
         *            Message budle if of reason
         */
        public WebCallErrorResponse(String paramName, String reasonLabelId) {
            super();
            this.paramName = paramName;
            this.reasonLabelId = reasonLabelId;
        }

        /**
         * @return the paramName
         */
        public String getParamName() {
            return paramName;
        }

        /**
         * @return the localized reason string
         */
        public String getReason() {
            return StringUtils.isEmpty(I18NUtil.getMessage(reasonLabelId)) ? reasonLabelId
                    : I18NUtil.getMessage(reasonLabelId);
        }
    }

    /**
     * Custom Constructor
     *
     * @param msgId
     *            Message to use
     * @param paramName
     *            Web script request parameter name
     * @param reasonLabelId
     *            Message budle if of reason
     * @param responseStatus
     *            HTTPS Response status
     */
    public WebScriptParameterException(String msgId, String paramName, String reasonLabelId, int responseStatus) {
        super(msgId);
        this.responseObjects = new WebCallErrorResponse(paramName, reasonLabelId);
        this.responseStatus = responseStatus;
    }

    /**
     * @return the responseObjects
     */
    public WebCallErrorResponse getResponseObjects() {
        return responseObjects;
    }

    /**
     * @return the responseStatus
     */
    public int getResponseStatus() {
        return responseStatus;
    }
}
