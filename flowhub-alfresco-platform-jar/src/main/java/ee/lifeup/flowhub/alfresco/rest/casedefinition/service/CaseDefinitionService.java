/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casedefinition.service;

import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.model.CaseDefinition;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.model.CaseDefinitionRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import org.alfresco.service.cmr.repository.NodeRef;

/**
 * Service for case definition.
 */
public interface CaseDefinitionService {

    /**
     * Create case definition.
     *
     * @param caseDefinitionRef     case definition reference in Flowable.
     * @param caseDefinitionRequest case definition request.
     */
    NodeRef create(String caseDefinitionRef, CaseDefinitionRequest caseDefinitionRequest);

    /**
     * Return case definition.
     *
     * @param nodeRef case definition reference.
     */
    CaseDefinition get(NodeRef nodeRef);


    /**
     * Update case definition.
     *
     * @param nodeRef           folder node reference.
     * @param caseDefinitionRef case definition reference in Flowable.
     * @param updateRequest     case definition request.
     */
    void update(NodeRef nodeRef, String caseDefinitionRef, CaseDefinitionRequest updateRequest);

    /**
     * Return list of case definitions.
     *
     * @param listRequest request with sorting, filtering and pagination.
     */
    PageList<CaseDefinition> list(ListRequest listRequest);


}
