/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.codehaus.jackson.JsonNode;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Implementation of dataset content node service.
 */
@Service
public class DatasetContentNodeServiceImpl extends AbstractServiceImpl implements DatasetContentNodeService {

    @Override
    public NodeRef create(NodeRef datasetNodeRef, DatasetRequest datasetRequest) {
        log.info("Going to create case dataset content node");
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        Preconditions.checkNotNull(datasetRequest, "dataset request must not be null");

        FileInfo fileInfo = fileFolderService.create(datasetNodeRef, LifeupConst.DATASET_CONTENT_NODE, ContentModel.TYPE_CONTENT);
        NodeRef contentNodeRef = fileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        createOrUpdateProperties(datasetRequest, properties);

        nodeService.addAspect(contentNodeRef, LifeupContentModel.ASPECT_CASE_DATASET_CONTENT_NODE, properties);
        nodeService.addAspect(contentNodeRef, ContentModel.ASPECT_VERSIONABLE, null);
        return contentNodeRef;
    }

    @Override
    public JsonNode getDataPropertyValue(NodeRef datasetNodeRef) {
        log.info("Going to read value of property \"data\" of dataset content node");
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");

        NodeRef contentNodeRef = getDatasetContentNodeRef(datasetNodeRef);
        return ParseUtils.getJsonNode(nodeService.getProperty(contentNodeRef, LifeupContentModel.PROPERTY_CASE_DATASET_CONTENT_NODE_DATA));
    }

    @Override
    public void update(NodeRef datasetNodeRef, DatasetRequest updateRequest) {
        log.info("Going to create case dataset content node");
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        Preconditions.checkNotNull(updateRequest, "dataset request must not be null");

        NodeRef contentNodeRef = getDatasetContentNodeRef(datasetNodeRef);
        assertNodeIsDatasetContentNode(contentNodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(contentNodeRef);
        createOrUpdateProperties(updateRequest, properties);
        nodeService.setProperties(contentNodeRef, properties);
    }

    @Override
    public NodeRef getDatasetContentNodeRef(NodeRef datasetNodeRef) {
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        return nodeService.getChildByName(datasetNodeRef, ContentModel.ASSOC_CONTAINS, LifeupConst.DATASET_CONTENT_NODE);
    }

    private void createOrUpdateProperties(DatasetRequest datasetRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_NAME, LifeupConst.DATASET_CONTENT_NODE);
        properties.put(ContentModel.PROP_TITLE, LifeupConst.DATASET_CONTENT_NODE);
        properties.put(ContentModel.PROP_DESCRIPTION, LifeupConst.DATASET_CONTENT_NODE);
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_CONTENT_NODE_DATA, datasetRequest.getDatasetData().toString());
    }

    private void assertNodeIsDatasetContentNode(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_CASE_DATASET_CONTENT_NODE)) {
            throw new IllegalArgumentException("NodeRef should be a case dataset content node");
        }
    }

}
