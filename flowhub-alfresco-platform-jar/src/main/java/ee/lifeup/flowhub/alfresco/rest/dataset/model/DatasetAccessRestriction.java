/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.model;

import org.codehaus.jackson.JsonNode;

import java.util.Objects;

/**
 * Dataset access restriction model.
 */
public class DatasetAccessRestriction {
    private String datasetAccessRight;
    private JsonNode datasetAccessData;

    public String getDatasetAccessRight() {
        return datasetAccessRight;
    }

    public void setDatasetAccessRight(String datasetAccessRight) {
        this.datasetAccessRight = datasetAccessRight;
    }

    public JsonNode getDatasetAccessData() {
        return datasetAccessData;
    }

    public void setDatasetAccessData(JsonNode datasetAccessData) {
        this.datasetAccessData = datasetAccessData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatasetAccessRestriction)) return false;
        DatasetAccessRestriction that = (DatasetAccessRestriction) o;
        return Objects.equals(getDatasetAccessRight(), that.getDatasetAccessRight()) &&
                Objects.equals(getDatasetAccessData(), that.getDatasetAccessData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDatasetAccessRight(), getDatasetAccessData());
    }
}
