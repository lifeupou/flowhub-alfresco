/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.utils;

import ee.lifeup.flowhub.alfresco.exception.WebScriptParameterException;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.servlet.FormData;
import org.springframework.extensions.webscripts.servlet.FormData.FormField;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Web Utility class for common REST service operations.
 * legacy
 */
// NOSONAR
public class WebScriptUtils {
    private static final Logger LOGGER = Logger.getLogger(WebScriptUtils.class);

    /**
     * Utility class private constructor.
     */
    private WebScriptUtils() {
    }

    /**
     * Returns request parameter for given name
     *
     * @param request
     *            Web Request Instance
     * @param parameterName
     *            Parameter name
     * @return Request value as string
     */
    public static String getParameterValue(WebScriptRequest request, String parameterName) {

        return getParameterValue(request, parameterName, WebParameter.NON_MANDATORY);
    }

    /**
     * Returns request parameter for given name
     *
     * @param request
     *            Web Request Instance
     * @param parameterName
     *            Parameter name
     * @param presence
     *            Parameter presence at Request
     * @return Request value as string
     */
    public static String getParameterValue(WebScriptRequest request, String parameterName, WebParameter presence) {
        String value = request.getParameter(parameterName);
        if (StringUtils.isEmpty(value) && request.getServiceMatch().getTemplateVars().containsKey(parameterName)) {
            value = request.getServiceMatch().getTemplateVars().get(parameterName);
        }

        if (StringUtils.isEmpty(value) && WebParameter.MANDATORY.equals(presence)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Parametr mandatory,but missing:" + parameterName);
            }

            throw new WebScriptParameterException("Mandatory Parameter missing", parameterName,
                    "webscripts.common.missing-parameter", HttpStatus.SC_BAD_REQUEST);

        }
        return value;
    }

    /**
     * Returns request parameter for given name
     *
     * @param request
     *            Web Request Instance
     * @param parameterName
     *            Parameter name
     * @return Request value as string array
     */
    public static String[] getParameterValues(WebScriptRequest request, String parameterName) {
        String[] values = request.getParameterValues(parameterName);
        if (values == null
                || values.length == 0 && request.getServiceMatch().getTemplateVars().containsKey(parameterName)) {
            String valueSingle = request.getServiceMatch().getTemplateVars().get(parameterName);
            if (!StringUtils.isEmpty(valueSingle)) {
                values = new String[] { valueSingle };
            }
        }
        return values;
    }

    /**
     * Returns form data parameter for given name
     *
     * @param formData
     *            Form data
     * @param fieldName
     *            Parameter name
     * @param mandatory
     *            True if mandatory (if this parameter is true and request
     *            parameter is not specified, a WebScriptExcpetion with
     *            BAD_REQUEST as status will be thrown)
     * @return Form data value as string
     */
    public static String getParameterValue(FormData formData, String fieldName, boolean mandatory) {
        String value = getFormFieldValue(formData, fieldName);
        if (mandatory) {
            checkMandatoryParameter(fieldName, value);
        }
        return value;
    }

    /**
     * Returns form data parameter values for given name
     *
     * @param formData
     *            Form data
     * @param fieldName
     *            Parameter name
     * @param mandatory
     *            True if mandatory (if this parameter is true and request
     *            parameter is not specified, a WebScriptExcpetion with
     *            BAD_REQUEST as status will be thrown)
     * @return Form data value as string array
     */
    public static String[] getParameterValues(FormData formData, String fieldName, boolean mandatory) {
        String[] values = getFormFieldValues(formData, fieldName);
        if (mandatory) {
            checkMandatoryParameter(fieldName, values);
        }
        return values;
    }

    /**
     * Gets value for field name
     *
     * @param formData
     *            Form data
     * @param fieldName
     *            Field name
     * @return value for field name
     */
    protected static String getFormFieldValue(FormData formData, String fieldName) {
        String paramValue = null;
        FormField[] fields = formData.getFields();
        for (FormField field : fields) {
            if (field.getName().equals(fieldName)) {
                paramValue = field.getValue();
                break;
            }
        }
        return paramValue;
    }

    /**
     * Gets content from FormData field
     *
     * @param formData
     *            FormData with file inside
     * @param fieldName
     *            Field name
     * @param mandatory
     *            True if mandatory (if this parameter is true and request
     *            parameter is not specified, a WebScriptExcpetion with
     *            BAD_REQUEST as status will be thrown)
     * @throws WebScriptParameterException
     *             thrown if mandatory parameter is empty
     * @return Content
     */
    public static Content getFormFieldContent(FormData formData, String fieldName, boolean mandatory) {

        Content documentContent = null;

        for (FormData.FormField formField : formData.getFields()) {

            final String formFieldName = formField.getName();
            if (formFieldName.equals(fieldName)) {
                documentContent = formField.getContent();
                break;
            }
        }

        if (mandatory && documentContent == null) {
            LOGGER.error("Missing mandatory field content: " + fieldName);
            throw new WebScriptParameterException("Mandatory Parameter missing", fieldName,
                    "webscripts.common.missing-parameter", HttpStatus.SC_BAD_REQUEST);
        }

        return documentContent;
    }

    /**
     * Gets values for field name
     *
     * @param formData
     *            Form data
     * @param fieldName
     *            Field name
     * @return values for field name
     */
    public static String[] getFormFieldValues(FormData formData, String fieldName) {
        final Map<String, String[]> parameters = formData.getParameters();
        if (parameters != null) {
            return parameters.get(fieldName);
        } else {
            return new String[0];
        }
    }

    /**
     * Checks parameter for given value with mandatory check
     *
     * @param paramerName
     *            Parameter name
     * @param value
     *            Value to parse
     * @throws WebScriptParameterException
     *             thrown if mandatory parameter is empty
     */
    protected static void checkMandatoryParameter(String paramerName, String value) {
        boolean empty = StringUtils.isEmpty(value);
        if (empty) {
            LOGGER.error("Missing mandatory parameter: " + paramerName);
            throw new WebScriptParameterException("Mandatory Parameter missing", paramerName,
                    "webscripts.common.missing-parameter", HttpStatus.SC_BAD_REQUEST);
        }
    }

    /**
     * Checks parameter for given value with mandatory check
     *
     * @param parameterName Parameter name
     * @param value         Value to parse
     * @throws WebScriptParameterException thrown if mandatory parameter is empty
     */
    protected static void checkMandatoryParameter(String parameterName, String[] value) {
        boolean empty = value == null || value.length == 0;
        if (empty) {
            LOGGER.error("Missing mandatory parameter: " + parameterName);
            throw new WebScriptParameterException("Mandatory Parameter missing", parameterName,
                    "webscripts.common.missing-parameter", HttpStatus.SC_BAD_REQUEST);
        }
    }

    /**
     * De-Serializes sent JSON object.
     *
     * @param request
     *            web-script request
     * @param type
     *            Type of the value
     * @param <T>
     *            Class of the return type
     * @param objectMapper
     *            instance of object serialization mapper
     * @return instance of parameter type
     */
    public static <T> T getParameterJSONBean(WebScriptRequest request, Class<T> type,
                                                            ObjectMapper objectMapper) {
        try {
            return objectMapper.readValue(request.getContent().getInputStream(), type);
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error", HttpStatus.SC_BAD_REQUEST);
        }
    }

    /**
     * De-Serializes sent JSON object as Map of properties.
     *
     * @param request
     *            web-script request
     * @param objectMapper
     *            instance of object serialization mapper
     * @return metadata properties map
     */
    public static Map<String, Object> getParameterJSONAsMap(WebScriptRequest request, ObjectMapper objectMapper) {

        final Map<String, Object> metadataPropertyMap;
        try {
            metadataPropertyMap = objectMapper.readValue(request.getContent().getInputStream(),
                    objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class));
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error", HttpStatus.SC_BAD_REQUEST);
        }

        return metadataPropertyMap;
    }

    /**
     * De-Serializes sent JSON object as Map of properties.
     *
     * @param jsonNode
     *            jsonNode
     * @param objectMapper
     *            instance of object serialization mapper
     * @return metadata properties map
     */
    public static Map<String, Object> getParameterJSONAsMap(JsonNode jsonNode, ObjectMapper objectMapper) {

        final Map<String, Object> metadataPropertyMap;
        try {
            metadataPropertyMap = objectMapper.readValue(jsonNode,
                    objectMapper.getTypeFactory().constructMapType(HashMap.class, String.class, Object.class));
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error", HttpStatus.SC_BAD_REQUEST);
        }

        return metadataPropertyMap;
    }

    /**
     * De-Serializes JSON object as List of properties.
     *
     * @param request      web-script request.
     * @param objectMapper instance of object serialization mapper.
     * @param type         type class of properties.
     * @param fieldName    field name.
     * @return metadata properties list.
     */
    public static <T> List<T> getParameterJSONAsList(WebScriptRequest request, String fieldName, Class<T> type, ObjectMapper objectMapper) {
        try {
            JsonNode tree = objectMapper.readTree(request.getContent().getInputStream());
            JsonNode items = tree.get(fieldName);
            return objectMapper.readValue(items,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error. " + e.getLocalizedMessage(), HttpStatus.SC_BAD_REQUEST);
        }
    }

    /**
     * De-Serializes JSON object as List of properties.
     *
     * @param jsonNode     jsonNode.
     * @param objectMapper instance of object serialization mapper.
     * @param type         type class of properties.
     * @param fieldName    field name.
     * @return metadata properties list.
     */
    public static <T> List<T> getParameterJSONAsList(JsonNode jsonNode, String fieldName, Class<T> type, ObjectMapper objectMapper, boolean mandatory) {
        try {
            JsonNode items = jsonNode.get(fieldName);
            if (items == null) {
                if (mandatory) {
                    throw new IllegalArgumentException("Field " + fieldName + " not specified");
                } else {
                    return Collections.emptyList();
                }
            }

            return objectMapper.readValue(items,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error. " + e.getLocalizedMessage(), HttpStatus.SC_BAD_REQUEST);
        }
    }

    /**
     * De-Serializes String JSON object as List of properties.
     *
     * @param content      JSON content in a string.
     * @param objectMapper instance of object serialization mapper.
     * @param type         type class of properties.
     * @param fieldName    field name.
     * @return metadata properties list.
     */
    public static <T> List<T> getParameterJSONAsList(String content, String fieldName, Class<T> type, ObjectMapper objectMapper, boolean mandatory) {
        try {
            JsonNode tree = objectMapper.readTree(content);
            JsonNode items = tree.get(fieldName);
            if (items == null) {
                if (mandatory) {
                    throw new IllegalArgumentException("Field " + fieldName + " not specified");
                } else {
                    return Collections.emptyList();
                }
            }

            return objectMapper.readValue(items,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, type));
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error. " + e.getLocalizedMessage(), HttpStatus.SC_BAD_REQUEST);
        }
    }

    public static JsonNode getParameterJSONAsNode(WebScriptRequest request, ObjectMapper objectMapper) {
        try {
            return objectMapper.readTree(request.getContent().getInputStream());
        } catch (Exception e) {
            LOGGER.warn("Unable to read JSON from request body:" + e.getLocalizedMessage());
            throw new WebScriptParameterException("Unable to read Request body as JSON", "request body",
                    "webscripts.common.request-body-error. " + e.getLocalizedMessage(), HttpStatus.SC_BAD_REQUEST);
        }
    }

}
