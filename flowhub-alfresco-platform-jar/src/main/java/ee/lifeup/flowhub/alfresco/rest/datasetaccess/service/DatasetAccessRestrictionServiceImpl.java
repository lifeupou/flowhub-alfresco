/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetaccess.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.datasetaccess.model.DatasetAccessRestriction;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Map;

/**
 * DatasetAccessRestrictionService implementation.
 */
@Service
public class DatasetAccessRestrictionServiceImpl extends AbstractServiceImpl implements DatasetAccessRestrictionService {

    @Override
    public DatasetAccessRestriction get(NodeRef datasetNodeRef) {
        log.info("Going to read dataset access restrictions for dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        assertNodeIsDatasetAccessRestriction(datasetNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(datasetNodeRef);
        return createDatasetAccessRestriction(properties);
    }

    @Override
    public void update(NodeRef datasetNodeRef, DatasetAccessRestriction updateRequest) {
        log.info("Going to update dataset access restrictions for dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        Preconditions.checkNotNull(updateRequest, "dataset access restrictions request must not be null");
        assertNodeIsDatasetAccessRestriction(datasetNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(datasetNodeRef);
        createProperties(updateRequest, properties);
        nodeService.setProperties(datasetNodeRef, properties);
    }

    private DatasetAccessRestriction createDatasetAccessRestriction(Map<QName, Serializable> properties) {
        DatasetAccessRestriction accessRestriction = new DatasetAccessRestriction();
        accessRestriction.setDatasetAccessRight(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DATASET_ACCESS_RIGHT));
        accessRestriction.setDatasetAccessData(ParseUtils.getJsonNode(properties, LifeupContentModel.PROPERTY_CASE_DATASET_ACCESS_DATA));
        return accessRestriction;
    }

    private void createProperties(DatasetAccessRestriction accessRestriction, Map<QName, Serializable> properties) {
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_ACCESS_RIGHT, accessRestriction.getDatasetAccessRight());
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_ACCESS_DATA, accessRestriction.getDatasetAccessData().toString());
    }

    private void assertNodeIsDatasetAccessRestriction(NodeRef datasetNodeRef) {
        assertNodeExists(datasetNodeRef);
        if (!hasAllAspects(datasetNodeRef,
                LifeupContentModel.ASPECT_CASE_DATASET,
                LifeupContentModel.ASPECT_CASE_DATASET_ACCESS_RESTRICTION)) {
            throw new IllegalArgumentException("NodeRef should be a case dataset and a dataset access restriction");
        }
    }
}
