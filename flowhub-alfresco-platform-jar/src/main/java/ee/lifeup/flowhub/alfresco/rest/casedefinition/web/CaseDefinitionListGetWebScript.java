/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casedefinition.web;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.model.CaseDefinition;
import ee.lifeup.flowhub.alfresco.rest.casedefinition.service.CaseDefinitionService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Web script for listing the case definitions.
 */
@Component("webscript.lifeup.flowhub.casedefinition.casedefinitions.get")
public class CaseDefinitionListGetWebScript extends AbstractBaseWebScript {

    @Autowired
    private CaseDefinitionService caseDefinitionService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        ListRequest listRequest = new ListRequest(LifeupContentModel.ASPECT_CASE_DEFINITION);
        listRequest.parse(request);
        PageList<CaseDefinition> caseDefinitionPageList = caseDefinitionService.list(listRequest);
        writeResponse(response, caseDefinitionPageList);
    }
}
