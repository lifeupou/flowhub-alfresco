/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casefile.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFile;
import ee.lifeup.flowhub.alfresco.rest.casefile.model.CaseFileRequest;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.service.CaseInstanceService;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
 * Implementation of case file service.
 */
@Service
public class CaseFileServiceImpl extends AbstractServiceImpl implements CaseFileService {

    @Autowired
    private CaseInstanceService caseInstanceService;

    @Override
    public NodeRef create(String caseRef, CaseFileRequest caseInstanceRequest) {
        log.info("We've got a case file creation request");
        Preconditions.checkNotNull(caseInstanceRequest, "case file request must not be null");
        Preconditions.checkArgument(StringUtils.isNotBlank(caseRef), "case reference must not be null or empty");

        NodeRef caseInstanceNodeRef = caseInstanceService.getCaseInstanceNodeRef(caseRef);
        NodeRef caseAttachmentsNodeRef = caseInstanceService.getCaseInstanceAttachmentsNodeRef(caseInstanceNodeRef);

        FileInfo caseFileInfo = fileFolderService.create(caseAttachmentsNodeRef, caseInstanceRequest.getFileName(), ContentModel.TYPE_CONTENT);
        NodeRef caseFileNodeRef = caseFileInfo.getNodeRef();
        nodeService.createAssociation(caseInstanceNodeRef, caseFileNodeRef, LifeupContentModel.ASSOCIATION_CASE_INSTANCE_FILE);

        Map<QName, Serializable> properties = new HashMap<>();
        createOrUpdateProperties(caseInstanceRequest, properties);

        nodeService.addAspect(caseFileNodeRef, LifeupContentModel.ASPECT_CASE_FILE, properties);
        nodeService.addAspect(caseFileNodeRef, ContentModel.ASPECT_AUDITABLE, Collections.emptyMap());
        return caseFileNodeRef;
    }

    @Override
    public CaseFile get(NodeRef caseFileRef) {
        log.info("Going to read case file " + caseFileRef);
        Preconditions.checkNotNull(caseFileRef, "case file node reference must not be null");
        assertNodeIsCaseFile(caseFileRef);
        Map<QName, Serializable> properties = nodeService.getProperties(caseFileRef);
        return createCaseFile(caseFileRef, properties);
    }

    @Override
    public void update(NodeRef fileNodeRef, CaseFileRequest updateRequest) {
        log.info("Going to update case file " + fileNodeRef);
        Preconditions.checkNotNull(updateRequest, "case file request must not be null");
        Preconditions.checkNotNull(fileNodeRef, "case file node reference must not be null");
        assertNodeIsCaseFile(fileNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(fileNodeRef);
        createOrUpdateProperties(updateRequest, properties);
        nodeService.setProperties(fileNodeRef, properties);
    }

    @Override
    public List<CaseFile> list(String caseRef) {
        log.info("Going to read attachment files and linked files for case instance " + caseRef);
        Preconditions.checkArgument(StringUtils.isNotBlank(caseRef), "case reference must not be null or empty");

        NodeRef caseNodeRef = caseInstanceService.getCaseInstanceNodeRef(caseRef);
        List<CaseFile> caseFiles = new ArrayList<>();
        List<AssociationRef> caseFileAssocs = nodeService.getTargetAssocs(caseNodeRef, RegexQNamePattern.MATCH_ALL);
        caseFileAssocs.forEach(caseFileAssoc -> {
            NodeRef caseFileRef = caseFileAssoc.getTargetRef();
            Map<QName, Serializable> properties = nodeService.getProperties(caseFileRef);
            CaseFile caseFile = createCaseFile(caseFileRef, properties);
            caseFiles.add(caseFile);
        });
        return caseFiles;
    }


    private void createOrUpdateProperties(CaseFileRequest caseInstanceRequest, Map<QName, Serializable> properties) {
        properties.put(LifeupContentModel.PROPERTY_CASE_FILE_TYPE, caseInstanceRequest.getFileType());
        properties.put(ContentModel.PROP_NAME, caseInstanceRequest.getFileName());
        properties.put(ContentModel.PROP_TITLE, caseInstanceRequest.getFileTitle());
        properties.put(ContentModel.PROP_DESCRIPTION, caseInstanceRequest.getFileDescription());
    }

    private CaseFile createCaseFile(NodeRef caseFileRef, Map<QName, Serializable> properties) {
        CaseFile caseFile = new CaseFile();
        caseFile.setFileIdentifier(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        caseFile.setFileRef(caseFileRef.toString());
        caseFile.setFilePath(getDisplayPathByNodeRef(caseFileRef));
        caseFile.setFileName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        caseFile.setFileTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        caseFile.setFileDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));
        caseFile.setFileType(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_FILE_TYPE));
        fillAuditFields(properties, caseFile);
        return caseFile;
    }

    private void assertNodeIsCaseFile(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_CASE_FILE)) {
            throw new IllegalArgumentException("NodeRef should be a case file");
        }
    }
}
