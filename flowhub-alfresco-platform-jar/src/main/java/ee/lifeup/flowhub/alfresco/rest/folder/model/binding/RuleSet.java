/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.model.binding;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Aleksandr Koltakov on 20.07.2018
 */

public class RuleSet {
    private final Set<String> valuesIncluding = new HashSet<>();
    private BigDecimal lessThan;
    private BigDecimal moreThan;
    private boolean allItems;

    public void addAll(Collection<Rule> rules) {
        rules.forEach(this::add);
    }

    public void add(Rule rule) {
        Condition condition = rule.getCondition();
        switch (condition) {
            case ALL:
                allItems = true;
                break;

            case EQUALS:
                valuesIncluding.addAll(Arrays.asList(rule.getValues()));
                break;


            case LESS_THAN:
                BigDecimal lessThanValue = getNumericValue(rule);
                if (lessThanValue != null && (lessThan == null || lessThanValue.compareTo(lessThan) < 0)) {
                    lessThan = lessThanValue;
                }
                break;

            case MORE_THAN:
                BigDecimal moreThanValue = getNumericValue(rule);
                if (moreThanValue != null && (moreThan == null || moreThanValue.compareTo(moreThan) > 0)) {
                    moreThan = moreThanValue;
                }
        }
    }

    private BigDecimal getNumericValue(Rule rule) {
        try {
            return new BigDecimal(rule.getValues()[0]);
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isIntersect(RuleSet other) {
        if (allItems || other.allItems) {
            return true;
        }

        Boolean intersects =  null;
        if (isValuesExists(other)) {
            intersects = isIntersectValues(other);
        }


        if (isRangeExists(other)) {
            intersects = intersects == null ? isIntersectRanges(other) : intersects && isIntersectRanges(other);
        }

        return intersects == null ? false : intersects;
    }

    private boolean isIntersectValues(RuleSet other) {
        Set<String> intersection = new HashSet<>(valuesIncluding);
        intersection.retainAll(other.valuesIncluding);
        return !intersection.isEmpty();
    }

    private boolean isRangeExists(RuleSet other) {
        return !(lessThan == null && moreThan == null || other.lessThan == null && other.moreThan == null);
    }

    private boolean isValuesExists(RuleSet other) {
        return !(valuesIncluding.isEmpty() && other.valuesIncluding.isEmpty());
    }

    private boolean isIntersectRanges(RuleSet other) {
        if (lessThan == null && moreThan == null || other.lessThan == null && other.moreThan == null) {
            return false;
        }

        if (lessThan != null && other.moreThan != null) {
            return lessThan.compareTo(other.moreThan) > 0;
        }

        if (moreThan != null && other.lessThan != null) {
            return moreThan.compareTo(other.lessThan) < 0;
        }

        return true;
    }

    public static RuleSet build(Collection<Rule> rules) {
        RuleSet ruleSet = new RuleSet();
        ruleSet.addAll(rules);
        return ruleSet;
    }
}
