/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.profile.service;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.model.search.PageList;
import ee.lifeup.flowhub.alfresco.rest.profile.model.DocumentProfile;
import ee.lifeup.flowhub.alfresco.rest.profile.model.DocumentProfileRequest;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
 * Created by Aleksandr Koltakov on 13.07.2018
 */

@Service
public class DocumentProfileServiceImpl extends AbstractServiceImpl implements DocumentProfileService {

    @Value("${flowhub.document.profile.root.path}")
    private String profileRootFolderPath;

    @Override
    public NodeRef create(DocumentProfileRequest documentProfileRequest) {
        String profileName = documentProfileRequest.getDocumentProfileName();
        log.info("We've got new document profile creation request with name " + profileName);

        NodeRef rootNodeRef = getRootNodeRef(profileRootFolderPath);
        FileInfo fileInfo = fileFolderService.create(rootNodeRef, profileName, ContentModel.TYPE_CONTENT);
        NodeRef profileNodeRef = fileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        addOrUpdateProperties(documentProfileRequest, properties);

        nodeService.addAspect(profileNodeRef, LifeupContentModel.ASPECT_DOCUMENT_PROFILE, properties);
        nodeService.addAspect(profileNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);
        return profileNodeRef;
    }

    private void addOrUpdateProperties(DocumentProfileRequest request, Map<QName, Serializable> properties) {
        assertMandatoryParametersExists(request);

        properties.put(ContentModel.PROP_NAME, request.getDocumentProfileName());
        properties.put(ContentModel.PROP_DESCRIPTION, request.getDocumentProfileDescription());

        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_NAME, request.getDocumentProfileName());
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DESCRIPTION, request.getDocumentProfileDescription());
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DATA_SET, request.getDocumentProfileDataSet());
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_VIEW_FORM, request.getDocumentProfileViewForm());
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_EDIT_FORM, request.getDocumentProfileEditForm());
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DOCUMENT_CATEGORY, request.getDocumentProfileDocumentCategory());

        String[] templates = request.getDocumentProfileFileTemplates();
        List<String> templatesList = templates == null ? ListUtils.EMPTY_LIST : Arrays.asList(templates);
        properties.put(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_FILE_TEMPLATES, (Serializable) templatesList);
    }

    @Override
    public DocumentProfile get(NodeRef nodeRef) {
        log.info("Going to read document profile " + nodeRef);
        assertDocumentProfileExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);

        DocumentProfile documentProfile = new DocumentProfile();
        documentProfile.setNodeRef(nodeRef.toString());
        documentProfile.setDocumentProfileName(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_NAME));
        documentProfile.setDocumentProfileDescription(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DESCRIPTION));
        documentProfile.setDocumentProfileDocumentCategory(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DOCUMENT_CATEGORY));
        documentProfile.setDocumentProfileDataSet(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_DATA_SET));
        documentProfile.setDocumentProfileViewForm(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_VIEW_FORM));
        documentProfile.setDocumentProfileEditForm(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_EDIT_FORM));

        List<String> templates = (List<String>) properties.get(LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_FILE_TEMPLATES);
        if (CollectionUtils.isNotEmpty(templates)) {
            documentProfile.setDocumentProfileFileTemplates(templates.toArray(new String[]{}));
        }

        fillAuditFields(properties, documentProfile);
        return documentProfile;
    }

    private void assertMandatoryParametersExists(DocumentProfileRequest request) {
        if (StringUtils.isEmpty(request.getDocumentProfileDocumentCategory())
                || StringUtils.isEmpty(request.getDocumentProfileDataSet())
                || StringUtils.isEmpty(request.getDocumentProfileViewForm())) {
            throw new IllegalArgumentException("Not all mandatory parameters exists");
        }
    }

    @Override
    public DocumentProfile get(String documentProfileName) {
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_DOCUMENT_PROFILE);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_DOCUMENT_PROFILE_NAME, documentProfileName);
        NodeRef documentProfileNodeRef = getSingleNode(query);
        return get(documentProfileNodeRef);
    }

    @Override
    public void update(NodeRef nodeRef, DocumentProfileRequest documentProfileRequest) {
        log.info("Going to update document profile " + nodeRef);
        assertDocumentProfileExists(nodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        addOrUpdateProperties(documentProfileRequest, properties);
        nodeService.setProperties(nodeRef, properties);
    }

    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete document profile " + nodeRef);
        assertDocumentProfileExists(nodeRef);

        //TODO check no documents linked with this profile
        nodeService.deleteNode(nodeRef);
    }

    @Override
    public PageList<DocumentProfile> list(ListRequest listRequest) {
        return getPageList(listRequest, this::get);
    }

    private void assertDocumentProfileExists(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_DOCUMENT_PROFILE)) {
            throw new IllegalArgumentException("NodeRef should be a document profile");
        }
    }

    @Override
    public List<NodeRef> getAssociatedFolders(NodeRef nodeRef) {
        assertDocumentProfileExists(nodeRef);
        List<AssociationRef> associationRefs = nodeService.getTargetAssocs(nodeRef, LifeupContentModel.ASSOCIATION_DOCUMENT_PROFILE_FOLDER);
        List<NodeRef> nodeRefs = new ArrayList<>(associationRefs.size());
        associationRefs.forEach(associationRef -> nodeRefs.add(associationRef.getTargetRef()));
        return nodeRefs;
    }
}
