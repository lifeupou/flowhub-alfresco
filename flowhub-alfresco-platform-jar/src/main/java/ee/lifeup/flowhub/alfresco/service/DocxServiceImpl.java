/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.service;

import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.repository.ContentReader;
import org.alfresco.service.cmr.repository.ContentService;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.io.IOException;
import java.io.InputStream;

@Service
public class DocxServiceImpl implements DocxService {

    @Autowired
    private NodeService nodeService;

    @Autowired
    private ContentService contentService;

    @Override
    public XWPFDocument readDocxContentFromNodeRef(NodeRef nodeRef) throws IOException {
        checkNodeRef(nodeRef);
        ContentReader reader = contentService.getReader(nodeRef, ContentModel.PROP_CONTENT);
        checkReader(reader, nodeRef);
        InputStream docStream = reader.getContentInputStream();
        return new XWPFDocument(docStream);
    }

    private void checkNodeRef(NodeRef nodeRef) {
        Assert.notNull(nodeRef, "fileTemplateNodeRef");
        if (!nodeService.getType(nodeRef).equals(ContentModel.TYPE_CONTENT)) {
            throw new IllegalArgumentException("NodeRef " + nodeRef.toString() + " must be content");
        }
    }

    private void checkReader(ContentReader reader, NodeRef nodeRef) {
        Assert.notNull(reader, "reader");
        Assert.notNull(reader.getMimetype(), "reader's mimetype");
        if (!MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING.equals(reader.getMimetype())) {
            throw new IllegalArgumentException("NodeRef " + nodeRef.toString() + " must be docx content");
        }
    }
}