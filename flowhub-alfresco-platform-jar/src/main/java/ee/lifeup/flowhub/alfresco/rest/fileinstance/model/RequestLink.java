/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.fileinstance.model;


public class RequestLink {

    private final static String START_MARKER = "[";

    private final static String END_MARKER = "]";

    protected String linkPlaceholder;

    protected String linkCaption;

    protected String linkValue;

    public String getLinkPlaceholder() {
        return linkPlaceholder;
    }

    public String getLinkValue() {
        return linkValue;
    }

    public String getActualCaption() {
        return linkCaption == null ? linkValue : linkCaption;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequestLink that = (RequestLink) o;

        if (linkPlaceholder != null ? !linkPlaceholder.equals(that.linkPlaceholder) : that.linkPlaceholder != null)
            return false;
        if (linkCaption != null ? !linkCaption.equals(that.linkCaption) : that.linkCaption != null) return false;
        return linkValue != null ? linkValue.equals(that.linkValue) : that.linkValue == null;
    }

    @Override
    public int hashCode() {
        int result = linkPlaceholder != null ? linkPlaceholder.hashCode() : 0;
        result = 31 * result + (linkCaption != null ? linkCaption.hashCode() : 0);
        result = 31 * result + (linkValue != null ? linkValue.hashCode() : 0);
        return result;
    }

    public String getLinkCaption() {
        return linkCaption;
    }

    public static boolean isTextPlaceholder(String linkValue) {
        return linkValue != null && linkValue.length() > 2 && linkValue.startsWith(START_MARKER) && linkValue.endsWith(END_MARKER);
    }

    public static String getTextFromPlaceholder(String placeholder) {
        return placeholder.substring(START_MARKER.length(), placeholder.length() - END_MARKER.length());
    }
}