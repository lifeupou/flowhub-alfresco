/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.profile.model;

/**
 * Created by Aleksandr Koltakov on 13.07.2018
 */

public class DocumentProfileRequest {
    private String documentProfileName;
    private String documentProfileDescription;
    private String documentProfileDocumentCategory;
    private String documentProfileDataSet;
    private String documentProfileViewForm;
    private String documentProfileEditForm;
    private String[] documentProfileFileTemplates;

    public String getDocumentProfileName() {
        return documentProfileName;
    }

    public void setDocumentProfileName(String documentProfileName) {
        this.documentProfileName = documentProfileName;
    }

    public String getDocumentProfileDescription() {
        return documentProfileDescription;
    }

    public void setDocumentProfileDescription(String documentProfileDescription) {
        this.documentProfileDescription = documentProfileDescription;
    }

    public String getDocumentProfileDocumentCategory() {
        return documentProfileDocumentCategory;
    }

    public void setDocumentProfileDocumentCategory(String documentProfileDocumentCategory) {
        this.documentProfileDocumentCategory = documentProfileDocumentCategory;
    }

    public String getDocumentProfileDataSet() {
        return documentProfileDataSet;
    }

    public void setDocumentProfileDataSet(String documentProfileDataSet) {
        this.documentProfileDataSet = documentProfileDataSet;
    }

    public String getDocumentProfileViewForm() {
        return documentProfileViewForm;
    }

    public void setDocumentProfileViewForm(String documentProfileViewForm) {
        this.documentProfileViewForm = documentProfileViewForm;
    }

    public String getDocumentProfileEditForm() {
        return documentProfileEditForm;
    }

    public void setDocumentProfileEditForm(String documentProfileEditForm) {
        this.documentProfileEditForm = documentProfileEditForm;
    }

    public String[] getDocumentProfileFileTemplates() {
        return documentProfileFileTemplates;
    }

    public void setDocumentProfileFileTemplates(String[] documentProfileFileTemplates) {
        this.documentProfileFileTemplates = documentProfileFileTemplates;
    }
}
