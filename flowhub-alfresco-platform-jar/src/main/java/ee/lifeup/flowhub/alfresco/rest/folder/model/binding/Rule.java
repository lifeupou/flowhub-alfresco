/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.folder.model.binding;

import ee.lifeup.flowhub.alfresco.exception.PreconditionFailedException;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Created by Aleksandr Koltakov on 20.07.2018
 */

public class Rule {
    private String element;
    private Condition condition;
    private String[] values;

    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public Condition getCondition() {
        return condition;
    }

    public void setCondition(Condition condition) {
        this.condition = condition;
    }

    public String[] getValues() {
        return values;
    }

    public void setValues(String[] values) {
        this.values = values;
    }

    public boolean isMatch(String counterValue) {
        if (condition == Condition.ALL) {
            return true;
        }

        if (counterValue == null) {
            return false;
        }

        Comparable comparableCounterValue = parse(counterValue);
        if (condition == Condition.EQUALS) {
            for (String value : values) {
                Comparable comparableValue = parse(value);
                if (comparableValue.compareTo(comparableCounterValue) == 0) {
                    return true;
                }
            }

            return false;
        }

        Comparable comparableValue = parse(values[0]);

        if (condition == Condition.LESS_THAN) {
            return comparableCounterValue.compareTo(comparableValue) < 0;
        }

        if (condition == Condition.MORE_THAN) {
            return comparableCounterValue.compareTo(comparableValue) > 0;
        }

        throw new PreconditionFailedException("Unknown condition type " + condition);
    }

    private Comparable parse(String value) {
        try {
            return new BigDecimal(value);
        } catch (NumberFormatException e) {
            return value;
        }
    }

    @Override
    public String toString() {
        return new StringBuilder(element == null ? StringUtils.EMPTY : element)
                .append(' ')
                .append(condition)
                .append(':')
                .append(values == null ? StringUtils.EMPTY : Arrays.toString(values))
                .toString();
    }
}
