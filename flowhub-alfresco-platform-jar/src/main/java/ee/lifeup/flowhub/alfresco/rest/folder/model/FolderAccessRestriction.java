package ee.lifeup.flowhub.alfresco.rest.folder.model;

import org.codehaus.jackson.JsonNode;

import java.util.Objects;

/**
 * Created by Aleksandr Koltakov on 28.08.2018
 */

public class FolderAccessRestriction {
    private String folderAccessRight;
    private JsonNode folderAccessData;

    public String getFolderAccessRight() {
        return folderAccessRight;
    }

    public void setFolderAccessRight(String folderAccessRight) {
        this.folderAccessRight = folderAccessRight;
    }

    public JsonNode getFolderAccessData() {
        return folderAccessData;
    }

    public void setFolderAccessData(JsonNode folderAccessData) {
        this.folderAccessData = folderAccessData;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FolderAccessRestriction)) return false;
        FolderAccessRestriction that = (FolderAccessRestriction) o;
        return Objects.equals(getFolderAccessRight(), that.getFolderAccessRight()) &&
                Objects.equals(getFolderAccessData(), that.getFolderAccessData());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFolderAccessRight(), getFolderAccessData());
    }
}
