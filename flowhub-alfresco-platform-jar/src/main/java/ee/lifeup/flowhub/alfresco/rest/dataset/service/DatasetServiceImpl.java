/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.service.CaseInstanceService;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.Dataset;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetRequest;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetStatus;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.*;

/**
 * Implementation of dataset service.
 */
@Service
public class DatasetServiceImpl extends AbstractServiceImpl implements DatasetService {

    @Autowired
    private CaseInstanceService caseInstanceService;
    @Autowired
    private DatasetContentNodeService contentNodeService;

    @Override
    public NodeRef create(String caseInstanceRef, DatasetRequest datasetRequest) {
        log.info("Going to create case dataset");
        Preconditions.checkNotNull(datasetRequest, "dataset request must not be null");
        Preconditions.checkArgument(StringUtils.isNotBlank(caseInstanceRef), "case instance reference must not be null or empty");

        NodeRef datasetParentNodeRef = caseInstanceService.getCaseInstanceDatasetsNodeRef(caseInstanceRef);
        return createByDataSetParentNodeRef(datasetParentNodeRef, datasetRequest);
    }

    @Override
    public NodeRef create(NodeRef caseInstanceNodeRef, DatasetRequest datasetRequest) {
        NodeRef datasetParentNodeRef = caseInstanceService.getCaseInstanceDatasetsNodeRef(caseInstanceNodeRef);
        return createByDataSetParentNodeRef(datasetParentNodeRef, datasetRequest);
    }

    private NodeRef createByDataSetParentNodeRef(NodeRef datasetParentNodeRef, DatasetRequest datasetRequest) {
        FileInfo fileInfo = fileFolderService.create(datasetParentNodeRef, datasetRequest.getDatasetName(), ContentModel.TYPE_FOLDER);
        NodeRef datasetNodeRef = fileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        createOrUpdateProperties(datasetRequest, properties);

        nodeService.addAspect(datasetNodeRef, LifeupContentModel.ASPECT_CASE_DATASET, properties);
        nodeService.addAspect(datasetNodeRef, LifeupContentModel.ASPECT_CASE_DATASET_ACCESS_RESTRICTION, MapUtils.EMPTY_MAP);
        nodeService.addAspect(datasetNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);

        contentNodeService.create(datasetNodeRef, datasetRequest);
        return datasetNodeRef;
    }

    @Override
    public Dataset get(NodeRef datasetNodeRef) {
        log.info("Going to read case dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        assertNodeIsCaseDataset(datasetNodeRef);
        return createDataset(datasetNodeRef);
    }

    @Override
    public void update(NodeRef datasetNodeRef, DatasetRequest updateRequest) {
        log.info("Going to update case dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");
        Preconditions.checkNotNull(updateRequest, "dataset request must not be null");

        assertDraftStatus(datasetNodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(datasetNodeRef);
        createOrUpdateProperties(updateRequest, properties);
        nodeService.setProperties(datasetNodeRef, properties);
        contentNodeService.update(datasetNodeRef, updateRequest);
    }

    @Override
    public List<Dataset> list(String caseInstanceRef) {
        log.info("Going to read datasets of case instance " + caseInstanceRef);
        Preconditions.checkArgument(StringUtils.isNotBlank(caseInstanceRef), "case instance reference must not be null or empty");
        NodeRef datasetsNodeRef = caseInstanceService.getCaseInstanceDatasetsNodeRef(caseInstanceRef);
        return listByDataSetNodeRef(datasetsNodeRef);
    }

    @Override
    public List<Dataset> list(NodeRef caseInstanceNodeRef) {
        log.info("Going to read datasets of case instance with nodeRef " + caseInstanceNodeRef);
        NodeRef datasetsNodeRef = caseInstanceService.getCaseInstanceDatasetsNodeRef(caseInstanceNodeRef);
        return listByDataSetNodeRef(datasetsNodeRef);
    }

    private List<Dataset> listByDataSetNodeRef(NodeRef datasetsNodeRef) {
        List<ChildAssociationRef> datasetChildAssocs = nodeService.getChildAssocs(datasetsNodeRef);
        List<Dataset> datasetList = new ArrayList<>();
        datasetChildAssocs.stream()
                .filter(childRef -> fileFolderService.getFileInfo(childRef.getChildRef()).isFolder() &&
                        hasAnyAspect(childRef.getChildRef(), LifeupContentModel.ASPECT_CASE_DATASET))
                .forEach(childRef -> {
                    Dataset dataset = createDataset(childRef.getChildRef());
                    datasetList.add(dataset);
                });
        return datasetList;
    }

    @Override
    public void delete(NodeRef datasetNodeRef) {
        log.info("Going to delete case dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "dataset node reference must not be null");

        assertDraftStatus(datasetNodeRef);
        nodeService.setProperty(datasetNodeRef, LifeupContentModel.PROPERTY_CASE_DATASET_STATUS, DatasetStatus.DELETED);
    }

    @Override
    public NodeRef getDatasetNodeRef(String datasetRef) {
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_CASE_DATASET);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_CASE_DATASET_DEFINITION_REF, datasetRef);
        return getSingleNode(query);
    }

    @Override
    public NodeRef getDatasetNodeRef(NodeRef caseInstanceNodeRef, String datasetRef) {
        NodeRef dataSetRootNodeRef = caseInstanceService.getCaseInstanceDatasetsNodeRef(caseInstanceNodeRef);
        List<ChildAssociationRef> childAssociationRefs = nodeService.getChildAssocs(dataSetRootNodeRef);
        for (ChildAssociationRef childAssociationRef : childAssociationRefs) {
            Object dataSetValue = nodeService.getProperty(childAssociationRef.getChildRef(), LifeupContentModel.PROPERTY_CASE_DATASET_DEFINITION_REF);
            if (Objects.equals(datasetRef, dataSetValue)) {
                return childAssociationRef.getChildRef();
            }
        }
        return null;
    }

    private Dataset createDataset(NodeRef datasetNodeRef) {
        Map<QName, Serializable> properties = nodeService.getProperties(datasetNodeRef);

        Dataset dataset = new Dataset();
        dataset.setDatasetIdentifier(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        dataset.setNodeRef(datasetNodeRef.toString());
        dataset.setDatasetName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        dataset.setDatasetTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        dataset.setDatasetDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));
        dataset.setDatasetDefinitionRef(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DATASET_DEFINITION_REF));
        dataset.setDatasetType(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DATASET_TYPE));
        dataset.setDatasetData(contentNodeService.getDataPropertyValue(datasetNodeRef));
        dataset.setStatus(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DATASET_STATUS));
        fillAuditFields(properties, dataset);
        return dataset;
    }

    private void createOrUpdateProperties(DatasetRequest datasetRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_NAME, datasetRequest.getDatasetName());
        properties.put(ContentModel.PROP_TITLE, datasetRequest.getDatasetTitle());
        properties.put(ContentModel.PROP_DESCRIPTION, datasetRequest.getDatasetDescription());
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_DEFINITION_REF, datasetRequest.getDatasetDefinitionRef());
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_TYPE, datasetRequest.getDatasetType());
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_DATA, datasetRequest.getDatasetData().toString());
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_STATUS, DatasetStatus.DRAFT);
    }

    private void assertNodeIsCaseDataset(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_CASE_DATASET)) {
            throw new IllegalArgumentException("NodeRef should be a case dataset");
        }
    }

    private void assertDraftStatus(NodeRef nodeRef) {
        assertNodeIsCaseDataset(nodeRef);
        boolean deletable = hasAnyState(nodeRef,
                LifeupContentModel.PROPERTY_CASE_DATASET_STATUS,
                DatasetStatus.class,
                DatasetStatus.DRAFT);
        if (!deletable) {
            throw new IllegalArgumentException("Case dataset should be in the following states [DRAFT]");
        }
    }

}
