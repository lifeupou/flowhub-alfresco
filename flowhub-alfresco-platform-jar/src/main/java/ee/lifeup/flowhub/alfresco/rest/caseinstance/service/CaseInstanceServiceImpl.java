/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.caseinstance.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.model.CaseInstance;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.model.CaseInstanceRequest;
import ee.lifeup.flowhub.alfresco.rest.caseinstance.model.CaseStatus;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Case instance service implementation.
 */
@Service
public class CaseInstanceServiceImpl extends AbstractServiceImpl implements CaseInstanceService {

    @Override
    public NodeRef create(String caseDefinitionRef, CaseInstanceRequest caseInstanceRequest) {
        log.info("We've got a case instance creation request");
        Preconditions.checkNotNull(caseInstanceRequest, "case instance request must not be null");
        Preconditions.checkArgument(StringUtils.isNotBlank(caseDefinitionRef), "case definiton reference must not be null or empty");

        String caseId = caseInstanceRequest.getId();
        NodeRef parentCaseDefinitonNodeRef = getParentCaseDefinitonNodeRef(caseDefinitionRef);

        FileInfo caseFileInfo = fileFolderService.create(parentCaseDefinitonNodeRef, caseId, ContentModel.TYPE_FOLDER);
        NodeRef caseNodeRef = caseFileInfo.getNodeRef();

        Map<QName, Serializable> properties = new HashMap<>();
        addOrUpdateProperties(caseId, caseInstanceRequest, properties);

        nodeService.addAspect(caseNodeRef, LifeupContentModel.ASPECT_CASE_INSTANCE, properties);
        nodeService.addAspect(caseNodeRef, ContentModel.ASPECT_AUDITABLE, MapUtils.EMPTY_MAP);

        createAttachmentsFolder(caseNodeRef);
        createDatasetsFolder(caseNodeRef);
        return caseNodeRef;
    }

    @Override
    public CaseInstance get(NodeRef nodeRef) {
        log.info("Going to read case instance " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case instance node reference must not be null");
        assertNodeIsCaseInstance(nodeRef);
        return getCaseInstance(nodeRef);
    }

    @Override
    public CaseInstance get(String caseInstanceRef) {
        NodeRef nodeRef = getCaseInstanceNodeRef(caseInstanceRef);
        return get(nodeRef);
    }

    @Override
    public void update(NodeRef nodeRef, CaseInstanceRequest updateRequest) {
        log.info("Going to update case instance " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case instance node reference must not be null");
        Preconditions.checkNotNull(updateRequest, "case instance request must not be null");
        assertCaseInstanceIsEditable(nodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        addOrUpdateProperties(updateRequest.getId(), updateRequest, properties);
        nodeService.setProperties(nodeRef, properties);
    }

    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete case instance " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case instance node reference must not be null");
        assertCaseInstanceIsDeletable(nodeRef);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS, CaseStatus.DELETED);
    }

    @Override
    public void close(NodeRef nodeRef) {
        log.info("Going to close case instance " + nodeRef);
        Preconditions.checkNotNull(nodeRef, "case instance node reference must not be null");
        assertCaseInstanceIsClosable(nodeRef);
        nodeService.setProperty(nodeRef, LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS, CaseStatus.CLOSED);
    }

    @Override
    public List<CaseInstance> list(String caseDefinitionRef) {
        log.info("Going to read case instances for case definition " + caseDefinitionRef);
        Preconditions.checkArgument(StringUtils.isNotBlank(caseDefinitionRef), "case definiton reference must not be null or empty");

        NodeRef parentCaseNodeRef = getParentCaseDefinitonNodeRef(caseDefinitionRef);
        List<ChildAssociationRef> caseChildAssocs = nodeService.getChildAssocs(parentCaseNodeRef);
        List<CaseInstance> caseInstances = new ArrayList<>();
        caseChildAssocs.stream()
                .filter(childRef -> fileFolderService.getFileInfo(childRef.getChildRef()).isFolder() &&
                        hasAnyAspect(childRef.getChildRef(), LifeupContentModel.ASPECT_CASE_INSTANCE))
                .forEach(childRef -> {
                    CaseInstance caseInstance = getCaseInstance(childRef.getChildRef());
                    caseInstances.add(caseInstance);
                });
        return caseInstances;
    }

    @Override
    public NodeRef getCaseInstanceNodeRef(String caseRef) {
        log.info("Going to find case instance nodeRef by it's flowhub ref " + caseRef);
        Preconditions.checkArgument(StringUtils.isNotBlank(caseRef), "case instance reference must not be null or empty");

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_CASE_INSTANCE);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_CASE_INSTANCE_REF, caseRef);
        return getSingleNode(query);
    }

    @Override
    public NodeRef getCaseInstanceAttachmentsNodeRef(NodeRef caseInstanceNodeRef) {
        Preconditions.checkNotNull(caseInstanceNodeRef, "case instance node reference must not be null ");
        return nodeService.getChildByName(caseInstanceNodeRef, ContentModel.ASSOC_CONTAINS, LifeupConst.FOLDER_ATTACHMENTS);
    }

    @Override
    public NodeRef getCaseInstanceDatasetsNodeRef(String caseInstanceRef) {
        Preconditions.checkArgument(StringUtils.isNotBlank(caseInstanceRef), "case instance reference must not be null or empty");

        NodeRef caseInstanceNodeRef = getCaseInstanceNodeRef(caseInstanceRef);
        return getCaseInstanceDatasetsNodeRef(caseInstanceNodeRef);
    }

    @Override
    public NodeRef getCaseInstanceDatasetsNodeRef(NodeRef caseInstanceNodeRef) {
        return nodeService.getChildByName(caseInstanceNodeRef, ContentModel.ASSOC_CONTAINS, LifeupConst.FOLDER_DATASETS);
    }

    private void createDatasetsFolder(NodeRef caseNodeRef) {
        fileFolderService.create(caseNodeRef, LifeupConst.FOLDER_DATASETS, ContentModel.TYPE_FOLDER);
    }

    private CaseInstance getCaseInstance(NodeRef nodeRef) {
        Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
        CaseInstance caseInstance = new CaseInstance();
        caseInstance.setCaseIdentifier(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        caseInstance.setNodeRef(nodeRef.toString());
        caseInstance.setId(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_INSTANCE_REF));
        caseInstance.setCaseTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        caseInstance.setVariables(ParseUtils.getJsonNode(properties, LifeupContentModel.PROPERTY_CASE_INSTANCE_DATA));
        caseInstance.setState(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS));
        fillAuditFields(properties, caseInstance);
        return caseInstance;
    }

    private NodeRef getParentCaseDefinitonNodeRef(String caseDefinitionRef) {
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addAspectMatchClause(query, LifeupContentModel.ASPECT_CASE_DEFINITION);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, LifeupContentModel.PROPERTY_CASE_DEFINITION_REF, caseDefinitionRef);
        return getSingleNode(query);
    }

    private void addOrUpdateProperties(String caseId, CaseInstanceRequest caseInstanceRequest, Map<QName, Serializable> properties) {
        properties.put(ContentModel.PROP_NAME, caseId);
        properties.put(ContentModel.PROP_TITLE, caseId);
        properties.put(ContentModel.PROP_DESCRIPTION, caseId);
        properties.put(LifeupContentModel.PROPERTY_CASE_INSTANCE_REF, caseId);
        properties.put(LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS, CaseStatus.OPENED);
        properties.put(LifeupContentModel.PROPERTY_CASE_INSTANCE_DATA, caseInstanceRequest.getVariables().toString());
    }

    private void assertNodeIsCaseInstance(NodeRef nodeRef) {
        assertNodeExists(nodeRef);
        if (!hasAnyAspect(nodeRef, LifeupContentModel.ASPECT_CASE_INSTANCE)) {
            throw new IllegalArgumentException("NodeRef should be a case instance");
        }
    }

    private void assertCaseInstanceIsEditable(NodeRef nodeRef) {
        assertNodeIsCaseInstance(nodeRef);
        //todo add others status checks
        boolean editable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS,
                CaseStatus.class,
                CaseStatus.OPENED);
        if (!editable) {
            throw new IllegalArgumentException("Case instance be in the following states [OPENED]");
        }
    }

    private void assertCaseInstanceIsDeletable(NodeRef nodeRef) {
        assertNodeIsCaseInstance(nodeRef);
        //todo add others status checks
        boolean deletable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS,
                CaseStatus.class,
                CaseStatus.OPENED);
        if (!deletable) {
            throw new IllegalArgumentException("Case instance should be in the following states [OPENED]");
        }
    }

    private void assertCaseInstanceIsClosable(NodeRef nodeRef) {
        assertNodeIsCaseInstance(nodeRef);
        //todo add others status checks
        boolean closable = hasAnyState(
                nodeRef,
                LifeupContentModel.PROPERTY_CASE_INSTANCE_STATUS,
                CaseStatus.class,
                CaseStatus.OPENED);
        if (!closable) {
            throw new IllegalArgumentException("Case instance should be in the following state [OPENED]");
        }
    }
}
