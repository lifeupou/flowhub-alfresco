/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.dataset.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.dataset.model.DatasetRequest;
import ee.lifeup.flowhub.alfresco.rest.dataset.service.DatasetService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import org.alfresco.service.cmr.repository.DuplicateChildNodeNameException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Web script for creating a dataset.
 */
@Component("webscript.lifeup.flowhub.dataset.dataset.post")
public class DatasetCreateWebScript extends AbstractBaseWebScript {

    @Autowired
    private DatasetService datasetService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            String caseInstanceRef = getParameter(request, LifeupConst.CASE_INSTANCE_REF_PARAMETER);
            String caseInstanceNodeRef = getParameter(request, LifeupConst.CASE_INSTANCE_NODE_REF_PARAMETER);

            if (StringUtils.isEmpty(caseInstanceRef) && StringUtils.isEmpty(caseInstanceNodeRef)) {
                throw new IllegalArgumentException("Either caseInstanceRef or caseInstanceNodeRef parameter should be specified");
            }

            DatasetRequest creationRequest = getParameterJSONBean(request, DatasetRequest.class);
            NodeRef datasetNodeRef = StringUtils.isEmpty(caseInstanceRef)
                    ? datasetService.create(new NodeRef(caseInstanceNodeRef), creationRequest)
                    : datasetService.create(caseInstanceRef, creationRequest);

            writeResponse(response, datasetNodeRef.toString(), HttpStatus.SC_CREATED);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create case dataset", e);
        } catch (DuplicateChildNodeNameException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create case dataset, duplicate name", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to create case dataset, parent nodeRef not found", e);
        }
    }
}
