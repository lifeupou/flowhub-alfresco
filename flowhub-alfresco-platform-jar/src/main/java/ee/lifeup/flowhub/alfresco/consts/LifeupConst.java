/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.consts;

/**
 * Flowhub constants.
 */
public class LifeupConst {
	private LifeupConst() {
        //do not call me
    }

    public static final String PARENT_NODE_REF_PARAMETER = "parentRef";
    public static final String ORGANIZATION_NODE_REF_PARAMETER = "organizationRef";
    public static final String QUERY_PARAMETER = "query";

    public static final String CASE_DEFINITION_REF_PARAMETER = "caseDefinitionRef";
    public static final String CASE_INSTANCE_REF_PARAMETER = "caseInstanceRef";
    public static final String CASE_INSTANCE_NODE_REF_PARAMETER = "caseInstanceNodeRef";

    public static final String FOLDER_ATTACHMENTS = "attachments";
    public static final String FOLDER_DATASETS = "datasets";

    public static final String DATASET_CONTENT_NODE = "content node";
    public static final String FILE_DATA_PARAMETER = "fileData";
    public static final String DOCUMENT_PROFILE_NAME_PARAMETER = "documentProfileName";
}