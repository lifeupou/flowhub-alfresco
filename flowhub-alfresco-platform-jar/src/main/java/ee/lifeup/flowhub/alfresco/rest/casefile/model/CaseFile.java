/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.casefile.model;

import ee.lifeup.flowhub.alfresco.model.Auditable;

import java.util.Objects;

/**
 * Case file model.
 */
public class CaseFile extends CaseFileRequest implements Auditable {
    private String fileIdentifier;
    private String fileRenditionURI;
    private String fileRef;
    private String filePath;
    private String created;
    private String creator;
    private String modified;
    private String modifier;

    public String getFileIdentifier() {
        return fileIdentifier;
    }

    public void setFileIdentifier(String fileIdentifier) {
        this.fileIdentifier = fileIdentifier;
    }

    public String getFileRef() {
        return fileRef;
    }

    public void setFileRef(String fileRef) {
        this.fileRef = fileRef;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileRenditionURI() {
        return fileRenditionURI;
    }

    public void setFileRenditionURI(String fileRenditionURI) {
        this.fileRenditionURI = fileRenditionURI;
    }

    public String getCreated() {
        return created;
    }

    @Override
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModified() {
        return modified;
    }

    @Override
    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CaseFile)) return false;
        if (!super.equals(o)) return false;
        CaseFile caseFile = (CaseFile) o;
        return Objects.equals(getFileIdentifier(), caseFile.getFileIdentifier()) &&
                Objects.equals(getFileRef(), caseFile.getFileRef()) &&
                Objects.equals(getFilePath(), caseFile.getFilePath()) &&
                Objects.equals(getFileRenditionURI(), caseFile.getFileRenditionURI()) &&
                Objects.equals(getCreated(), caseFile.getCreated()) &&
                Objects.equals(getCreator(), caseFile.getCreator()) &&
                Objects.equals(getModified(), caseFile.getModified()) &&
                Objects.equals(getModifier(), caseFile.getModifier());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFileIdentifier(), getFileRef(), getFilePath(), getFileRenditionURI(), getCreated(), getCreator(), getModified(), getModifier());
    }
}
