/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.category.service;

import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.model.search.ExactSearchFilter;
import ee.lifeup.flowhub.alfresco.rest.category.model.LocalizedNode;
import ee.lifeup.flowhub.alfresco.rest.category.model.MultiLanguageCategory;
import ee.lifeup.flowhub.alfresco.rest.category.model.TopCategory;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.LocalizationUtils;
import ee.lifeup.flowhub.alfresco.utils.LuceneQueryUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.node.MLPropertyInterceptor;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.MLText;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.CategoryService;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.Serializable;
import java.util.*;

/**
 * Created by Aleksandr Koltakov on 02.07.2018
 */
@Service
public class FlowhubCategoryServiceImpl extends AbstractServiceImpl implements FlowhubCategoryService {
    private static final Map<TopCategory, String> TOP_CATEGORIES = new EnumMap(TopCategory.class);

    @Autowired
    private CategoryService categoryService;

    @Value("${flowhub.category.root.name}")
    private String flowhubCategoryRootName;

    @Value("${flowhub.category.organization.name}")
    private String flowhubCategoryOrganizationName;

    @Value("${flowhub.category.retention-periods.name}")
    private String flowhubCategoryRetentionPeriodsName;

    @Value("${flowhub.category.activation.name}")
    private String flowhubCategoryActivationName;

    @Value("${flowhub.category.content-types.name}")
    private String flowhubCategoryContentTypesName;

    @PostConstruct
    private void init() {
        TOP_CATEGORIES.put(TopCategory.ORGANIZATIONS, flowhubCategoryOrganizationName);
        TOP_CATEGORIES.put(TopCategory.RETENTION_PERIODS, flowhubCategoryRetentionPeriodsName);
        TOP_CATEGORIES.put(TopCategory.ACTIVATION, flowhubCategoryActivationName);
        TOP_CATEGORIES.put(TopCategory.CONTENT_TYPES, flowhubCategoryContentTypesName);
    }

    @Override
    public NodeRef getRootCategoryNodeRef() {
        boolean createIfNotExists = true;
        Collection<ChildAssociationRef> nodeRefs = categoryService.getRootCategories(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE,
                ContentModel.ASPECT_GEN_CLASSIFIABLE,
                flowhubCategoryRootName,
                createIfNotExists);
        return getOneChildNodeRef(nodeRefs);
    }


    @Override
    public NodeRef create(NodeRef parent, MultiLanguageCategory category) {
        if (parent == null) {
            parent = getRootCategoryNodeRef();
        } else {
            assertNodeIsCategoryOrSubcategory(parent);
        }

        log.info("Going to create category " + category.getName() + " within nodeRef " + parent);

        NodeRef nodeRef = categoryService.createCategory(parent, category.getName());
        updateCategory(nodeRef, category);
        return nodeRef;
    }

    @Override
    public NodeRef create(String parentNodeName, MultiLanguageCategory category) {
        NodeRef parentNodeRef = findCategoryNodeRefByName(parentNodeName);
        return create(parentNodeRef, category);
    }

    @Override
    public MultiLanguageCategory get(NodeRef nodeRef) {
        assertNodeIsCategoryOrSubcategory(nodeRef);
        MultiLanguageCategory category = new MultiLanguageCategory();
        boolean mlAwareState = MLPropertyInterceptor.setMLAware(true);
        try {
            Map<QName, Serializable> properties = nodeService.getProperties(nodeRef);
            category.setName(properties.get(ContentModel.PROP_NAME).toString());

            MLText titles = (MLText) properties.get(ContentModel.PROP_TITLE);
            if (titles != null) {
                category.setTitles(LocalizationUtils.createLocalizedValues(titles));
            }

            MLText descriptions = (MLText) properties.get(ContentModel.PROP_DESCRIPTION);
            if (descriptions != null) {
                category.setDescriptions(LocalizationUtils.createLocalizedValues(descriptions));
            }
        } finally {
            MLPropertyInterceptor.setMLAware(mlAwareState);
        }
        return category;
    }

    @Override
    public MultiLanguageCategory get(String nodeName) {
        NodeRef nodeRef = findCategoryNodeRefByName(nodeName);
        return get(nodeRef);
    }

    @Override
    public void update(NodeRef nodeRef, MultiLanguageCategory category) {
        assertNodeIsCategoryOrSubcategory(nodeRef);
        updateCategory(nodeRef, category);
    }

    private void updateCategory(NodeRef nodeRef, MultiLanguageCategory category) {
        nodeService.addProperties(nodeRef, createMultiLanguageProperties(category));
    }

    private Map<QName, Serializable> createMultiLanguageProperties(MultiLanguageCategory categoryRequest) {
        Map<QName, Serializable> properties = new HashMap<>(3);
        properties.put(ContentModel.PROP_NAME, categoryRequest.getName());
        properties.put(ContentModel.PROP_TITLE, LocalizationUtils.createMultiLanguageText(categoryRequest.getTitles()));
        properties.put(ContentModel.PROP_DESCRIPTION, LocalizationUtils.createMultiLanguageText(categoryRequest.getDescriptions()));
        return properties;
    }

    @Override
    public List<LocalizedNode> list(NodeRef parent, Locale locale) {
        if (parent == null) {
            parent = getRootCategoryNodeRef();
        } else {
            assertNodeIsCategoryOrSubcategory(parent);
        }

        if (log.isDebugEnabled()) {
            log.debug("Going to list child categories of node " + parent + ", locale : " + locale);
        }

        List<LocalizedNode> result = new ArrayList<>();
        boolean mlAwareState = MLPropertyInterceptor.setMLAware(true);
        try {
            nodeService.getChildAssocs(parent)
                    .stream()
                    .filter(childAssociationRef -> Objects.equals(childAssociationRef.getTypeQName(), (ContentModel.ASSOC_SUBCATEGORIES)))
                    .forEach(childAssociationRef -> {
                        NodeRef child = childAssociationRef.getChildRef();
                        Map<QName, Serializable> properties = nodeService.getProperties(child);
                        LocalizedNode localizedNode = new LocalizedNode();
                        localizedNode.setNodeRef(child.toString());
                        localizedNode.setName(properties.get(ContentModel.PROP_NAME).toString());
                        localizedNode.setTitle(LocalizationUtils.getLocalizedPropertyValue(properties, ContentModel.PROP_TITLE, locale));
                        localizedNode.setDescription(LocalizationUtils.getLocalizedPropertyValue(properties, ContentModel.PROP_DESCRIPTION, locale));
                        localizedNode.setHasChildren(nodeService.countChildAssocs(child, true) > 0);
                        result.add(localizedNode);
                    });
            return result;
        } finally {
            MLPropertyInterceptor.setMLAware(mlAwareState);
        }
    }

    @Override
    public List<LocalizedNode> list(String parentNodeName, Locale locale) {
        NodeRef parentNodeRef = findCategoryNodeRefByName(parentNodeName);
        return list(parentNodeRef, locale);
    }

    private NodeRef findCategoryNodeRefByName(String nodeName) {
        log.debug("Going to find category by name " + nodeName);
        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addTypeMatchClause(query, ContentModel.TYPE_CATEGORY);
        LuceneQueryUtils.addAndClause(query);
        LuceneQueryUtils.addPropertyMatchClause(query, ContentModel.PROP_NAME, nodeName);

        List<NodeRef> nodeRefs = findNodeRefsByLuceneQuery(query, new ExactSearchFilter() {
            @Override
            protected boolean isMatch(NodeRef nodeRef) {
                return isChildOfRootCategory(nodeRef);
            }
        });

        if (CollectionUtils.isEmpty(nodeRefs)) {
            log.debug("Unable to find category be name " + nodeName);
            throw new NodeRefNotFoundException();
        }

        return nodeRefs.get(0);
    }


    @Override
    public void delete(NodeRef nodeRef) {
        log.info("Going to delete category with nodeRef " + nodeRef);
        assertNodeIsCategoryOrSubcategory(nodeRef);

        List<ChildAssociationRef> childAssociationRefs = nodeService.getChildAssocs(nodeRef);
        if (CollectionUtils.isNotEmpty(childAssociationRefs)) {
            throw new IllegalArgumentException("Unable to delete category with children");
        }

        StringBuilder query = new StringBuilder();
        LuceneQueryUtils.addPropertyMatchClause(query, ContentModel.PROP_CATEGORIES, nodeRef.toString());
        List<NodeRef> linkedNodeRefs = findNodeRefsByLuceneQuery(query);
        if (CollectionUtils.isEmpty(linkedNodeRefs)) {
            nodeService.deleteNode(nodeRef);
        } else {
            if (log.isDebugEnabled()) {
                log.debug("Unable to delete category, it is linked with nodeRef(s) " + linkedNodeRefs);
            }
            throw new IllegalArgumentException("Unable to delete linked category");
        }
    }

    @Override
    public NodeRef findByNameWithinCategory(TopCategory category, String name) {
        if (log.isDebugEnabled()) {
            log.debug("Going to find subcategory of " + category + " by name " + name);
        }
        NodeRef categoryRootNodeRef = findByNameWithinTopLevel(TOP_CATEGORIES.get(category));
        if (categoryRootNodeRef == null) {
            log.debug("No " + category + " category exists");
            return null;
        }

        NodeRef nodeRef = nodeService.getChildByName(categoryRootNodeRef, ContentModel.ASSOC_SUBCATEGORIES, name);
        if (nodeRef == null) {
            log.debug("No subcategory with specified name found");
        } else {
            log.debug("Found subcategory with nodeRef " + nodeRef);
        }

        return nodeRef;
    }

    private NodeRef findByNameWithinTopLevel(String name) {
        log.debug("Going to find subcategory " + name + " within top level");
        ChildAssociationRef childAssociationRef = categoryService.getCategory(getRootCategoryNodeRef(), ContentModel.ASSOC_SUBCATEGORIES, name);
        if (childAssociationRef == null) {
            log.debug("Nothing found");
            return null;
        }
        NodeRef nodeRef = childAssociationRef.getChildRef();
        log.debug("NodeRef found " + nodeRef);
        return nodeRef;
    }

    private void assertNodeIsCategoryOrSubcategory(NodeRef nodeRef) {
        ChildAssociationRef childAssociationRef = nodeService.getPrimaryParent(nodeRef);
        if (!isCategoryOrSubcategory(childAssociationRef.getTypeQName()) || !isChildOfRootCategory(nodeRef)) {
            throw new IllegalArgumentException("NodeRef should be a category or a subcategory");
        }
    }

    private boolean isCategoryOrSubcategory(QName qName) {
        return Objects.equals(qName, ContentModel.ASSOC_SUBCATEGORIES) || Objects.equals(qName, ContentModel.ASSOC_CATEGORIES);
    }

    private boolean isChildOfRootCategory(NodeRef nodeRef) {
        ChildAssociationRef childAssociationRef = nodeService.getPrimaryParent(nodeRef);
        if (childAssociationRef == null) {
            return false;
        }
        NodeRef parentNodeRef = childAssociationRef.getParentRef();
        if (parentNodeRef == null) {
            return false;
        } else if (nodeService.getType(parentNodeRef).equals(ContentModel.TYPE_CATEGORY)) {
            return true;
        }

        return Objects.equals(parentNodeRef, getRootCategoryNodeRef()) || isChildOfRootCategory(parentNodeRef);
    }
}
