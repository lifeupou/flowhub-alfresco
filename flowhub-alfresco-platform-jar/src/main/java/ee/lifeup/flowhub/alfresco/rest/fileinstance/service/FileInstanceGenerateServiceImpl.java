/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.fileinstance.service;

import ee.lifeup.flowhub.alfresco.rest.fileinstance.model.*;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.service.DocxService;
import ee.lifeup.flowhub.alfresco.service.FileUploadService;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.content.MimetypeMap;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.poi.POIXMLProperties;
import org.apache.poi.xwpf.usermodel.*;
import org.openxmlformats.schemas.officeDocument.x2006.customProperties.CTProperty;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTTcPr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.util.List;

@Service
public class FileInstanceGenerateServiceImpl extends AbstractServiceImpl implements FileInstanceGenerateService  {

    @Autowired
    private DocxService docxService;

    @Autowired
    private FileUploadService fileUploadService;

    @Override
    public NodeRef updateFileInstance(FileInstanceUpdateRequest request) throws IOException {
        XWPFDocument document = processRequest(request);
        ByteArrayOutputStream outputStream = saveDocumentToStream(document);

        fileUploadService.uploadFileToNodeRefWithMimeType(
                request.getDestinationNodeRef(),
                MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING,
                new ByteArrayInputStream(outputStream.toByteArray()));
        return request.getDestinationNodeRef();
    }

    private ByteArrayOutputStream saveDocumentToStream(XWPFDocument document) throws IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        document.enforceUpdateFields();
        document.write(outputStream);
        return outputStream;
    }

    private XWPFDocument processRequest(FileInstanceBaseRequest request) throws IOException {
        XWPFDocument document = docxService.readDocxContentFromNodeRef(request.getFileTemplateNodeRef());
        processFields(document, request);
        processLinks(document, request);
        return document;
    }

    @Override
    public NodeRef createFileInstance(FileInstanceCreateRequest request) throws IOException {
        FileInfo fileInfo = fileFolderService.create(
                request.getDestinationFolderNodeRef(),
                request.getFileInstanceName(),
                ContentModel.TYPE_CONTENT);

        XWPFDocument document = processRequest(request);
        ByteArrayOutputStream outputStream = saveDocumentToStream(document);
        NodeRef fileInfoNodeRef = fileInfo.getNodeRef();
        fileUploadService.uploadFileToNodeRefWithMimeType(
                fileInfoNodeRef,
                MimetypeMap.MIMETYPE_OPENXML_WORDPROCESSING,
                new ByteArrayInputStream(outputStream.toByteArray()));
        return fileInfoNodeRef;
    }

    private void processFields(XWPFDocument document, FileInstanceBaseRequest request) {
        if (request.isRequestFieldsEmpty()) {
            return;
        }

        POIXMLProperties docProps = document.getProperties();
        POIXMLProperties.CustomProperties customProperties = docProps.getCustomProperties();
        if (customProperties == null) {
            return;
        }

        List<CTProperty> ctProperties = customProperties.getUnderlyingProperties().getPropertyList();
        if (ctProperties == null) {
            return;
        }

        ctProperties.stream()
                .filter(ctProperty -> request.isTemplateFieldPresent(ctProperty.getName()))
                .forEach(ctProperty -> processField(ctProperty, request));
    }

    private void processField(CTProperty ctProperty, FileInstanceBaseRequest request) {
        RequestField requestField = request.getFieldInfoByName(ctProperty.getName());
        if (requestField != null && !requestField.isValueEmpty()) {
            ctProperty.setLpwstr(requestField.getFieldValue());
        }
    }

    private void processLinks(XWPFDocument document, FileInstanceBaseRequest request) {
        if (request.isRequestLinksEmpty()) {
            return;
        }

        for (XWPFParagraph paragraph : document.getParagraphs()) {
            List<XWPFRun> runs = paragraph.getRuns();
            if (runs == null) {
                continue;
            }

            runs.stream()
                    .filter(r -> r instanceof XWPFHyperlinkRun)
                    .forEach(lr -> processLink(paragraph, (XWPFHyperlinkRun)lr, request));
        }
    }

    private void processLink(XWPFParagraph paragraph, XWPFHyperlinkRun linkRun, FileInstanceBaseRequest baseRequest) {
        String linkText = linkRun.getText(0);
        if (RequestLink.isTextPlaceholder(linkText)) {
            RequestLink requestLink = baseRequest.getLinkInfoByName(RequestLink.getTextFromPlaceholder(linkText));
            if (requestLink != null) {
                String oldLinkId = linkRun.getCTHyperlink().getId();
                // removing temporary link from link placeholder
                if (oldLinkId != null && paragraph.getDocument().getPackagePart().getRelationship(oldLinkId) != null) {
                    paragraph.getDocument().getPackagePart().removeRelationship(oldLinkId);
                }

                linkRun.setText(requestLink.getActualCaption(), 0);
                // adding new link to link storage
                String id = paragraph.getDocument().getPackagePart().addExternalRelationship(requestLink.getLinkValue(), XWPFRelation.HYPERLINK.getRelation()).getId();
                // setting added link to link object in document content
                linkRun.getCTHyperlink().setId(id);
            }
        }
    }

    private void addTable(XWPFDocument document) {
        // inches in one millimeter
        double inchInMm = 0.0393700787402;
        // width in millimeters
        long width = 20;
        // width in twips. 1 twip = 1/1440 inch
        long result = (long) (width*inchInMm*1440);

        //create table and set width of columns in twips
        XWPFTable table = document.createTable(4, 5);
        table.getCTTbl().addNewTblGrid().addNewGridCol().setW(BigInteger.valueOf(result));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(3200));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(1000));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(1000));
        table.getCTTbl().getTblGrid().addNewGridCol().setW(BigInteger.valueOf(1105));

        // cell with custom formating (bold centered text)
        XWPFTableCell cell = table.getRow(0).getCell(0);
        cell.removeParagraph(0);
        XWPFParagraph addParagraph = cell.addParagraph();
        addParagraph.setAlignment(ParagraphAlignment.CENTER);
        XWPFRun run = addParagraph.createRun();
        run.setBold(true);
        run.setText("1. Energeetikateaduskond");

        // cells with simple text
        table.getRow(1).getCell(0).setText("Energia- ja geotehnika (statsionaarne ope)");
        table.getRow(1).getCell(3).setText("AAED02/09");
        table.getRow(2).getCell(0).setText("1.");
        table.getRow(2).getCell(1).setText("Mari Loper");
        table.getRow(2).getCell(2).setText("168355AAED");
        // cell with two paragraphs
        table.getRow(2).getCell(3).removeParagraph(0);
        table.getRow(2).getCell(3).addParagraph().createRun().setText("juhendaja: dorsent Jako Kilter", 0);
        table.getRow(2).getCell(3).addParagraph().createRun().setText("juhendaja: dorsent Jako Kilter", 0);
        // cells with simple text
        table.getRow(2).getCell(4).setText("AE");
        table.getRow(3).getCell(0).setText("2.");
        table.getRow(3).getCell(1).setText("Henri Manninen");
        table.getRow(3).getCell(2).setText("168355AAED");
        table.getRow(3).getCell(3).setText("juhendaja: dorsent Jako Kilter");
        table.getRow(3).getCell(4).setText("AE");

        // disable spliting rows across pages
        table.getRows().forEach(r -> r.setCantSplitRow(true));

        // merging cells in row
        mergeCellHorizontally(table, 0, 0, 4);
        mergeCellHorizontally(table, 1, 3, 4);
        mergeCellHorizontally(table, 1, 0, 2);
    }

    static void mergeCellHorizontally(XWPFTable table, int row, int fromCol, int toCol) {
        XWPFTableCell cell = table.getRow(row).getCell(fromCol);
        // Try getting the TcPr. Not simply setting an new one every time.
        CTTcPr tcPr = cell.getCTTc().getTcPr();
        if (tcPr == null) tcPr = cell.getCTTc().addNewTcPr();
        // The first merged cell has grid span property set
        if (tcPr.isSetGridSpan()) {
            tcPr.getGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        } else {
            tcPr.addNewGridSpan().setVal(BigInteger.valueOf(toCol-fromCol+1));
        }
        // Cells which join (merge) the first one, must be removed
        for(int colIndex = toCol; colIndex > fromCol; colIndex--) {
            table.getRow(row).getCtRow().removeTc(colIndex);
        }
    }
}