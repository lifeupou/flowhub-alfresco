/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.exception.IllegalSearchResultSizeException;
import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.model.Auditable;
import ee.lifeup.flowhub.alfresco.model.search.*;
import ee.lifeup.flowhub.alfresco.rest.utils.ListRequest;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.repo.security.authentication.AuthenticationUtil;
import org.alfresco.repo.transaction.RetryingTransactionHelper;
import org.alfresco.service.cmr.model.FileFolderService;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.cmr.repository.Path;
import org.alfresco.service.cmr.search.ResultSet;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;
import org.alfresco.service.cmr.security.AccessPermission;
import org.alfresco.service.cmr.security.AccessStatus;
import org.alfresco.service.cmr.security.PermissionService;
import org.alfresco.service.namespace.NamespaceService;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.transaction.TransactionService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;
import java.util.*;
import java.util.function.Function;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */
public abstract class AbstractServiceImpl {
    protected final Logger log = Logger.getLogger(getClass());

    protected static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    @Autowired
    protected SearchService searchService;
    @Autowired
    protected NodeService nodeService;
    @Autowired
    protected FileFolderService fileFolderService;
    @Autowired
    protected NamespaceService namespaceService;
    @Autowired
    private TransactionService transactionService;
    @Autowired
    protected PermissionService permissionService;

    protected List<NodeRef> findNodeRefsByLuceneQuery(StringBuilder query) {
        return findNodeRefsByLuceneQuery(query.toString());
    }


    protected List<NodeRef> findNodeRefsByLuceneQuery(String query) {
        return findNodeRefsByLuceneQuery(query, new DefaultSearchFilter(), 0);
    }

    protected List<NodeRef> findNodeRefsByLuceneQuery(StringBuilder query, ExactSearchFilter filter) {
        return findNodeRefsByLuceneQuery(query.toString(), filter, 0);
    }

    protected List<NodeRef> findNodeRefsByLuceneQuery(StringBuilder query, int limit) {
        return findNodeRefsByLuceneQuery(query.toString(), new DefaultSearchFilter(), limit);
    }

    private List<NodeRef> findNodeRefsByLuceneQuery(String query, ExactSearchFilter exactSearchFilter, int limit) {
        SearchParameters parameters = SearchParametersFactory.create(query);
        if (limit > 0) {
            parameters.setLimit(limit);
        }
        ResultSet resultSet = null;
        long time = 0;

        if (log.isDebugEnabled()) {
            log.debug("Going to execute lucene query : " + query);
            time = System.currentTimeMillis();
        }

        try {
            resultSet = searchService.query(parameters);

            if (log.isDebugEnabled()) {
                log.debug("Found " + resultSet.length() + " elements in " + (System.currentTimeMillis() - time) + " ms");
            }

            List<NodeRef> result = exactSearchFilter.filter(nodeService, resultSet);

            if (log.isDebugEnabled()) {
                log.debug("Filtered result size : " + result.size() + ", full execution time " + (System.currentTimeMillis() - time) + " ms");
            }

            return result;
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
    }


    protected PageListNodeRef findNodeRefsByLuceneQuery(ListRequest listRequest) {
        return findNodeRefsByLuceneQuery(listRequest.getQuery(), listRequest.getSortDefinitions(), listRequest.getPagingDefinition());
    }

    protected PageListNodeRef findNodeRefsByLuceneQuery(StringBuilder query, List<SearchParameters.SortDefinition> sortDefinitions, PagingDefinition pagingDefinition) {
        return findNodeRefsByLuceneQuery(query, sortDefinitions, pagingDefinition, new DefaultPagingSearchFilter());
    }

    protected PageListNodeRef findNodeRefsByLuceneQuery(StringBuilder query, List<SearchParameters.SortDefinition> sortDefinitions, PagingDefinition pagingDefinition, ExactPagingSearchFilter filter) {
        SearchParameters parameters = SearchParametersFactory.create(query);

        if (CollectionUtils.isNotEmpty(sortDefinitions)) {
            for (SearchParameters.SortDefinition definition : sortDefinitions) {
                parameters.addSort(definition);
            }
        }

        ResultSet resultSet = null;
        long time = 0;

        if (log.isDebugEnabled()) {
            log.debug("Going to execute lucene query (paging) : " + query);
            time = System.currentTimeMillis();
        }

        try {
            resultSet = searchService.query(parameters);

            if (log.isDebugEnabled()) {
                log.debug("Found " + resultSet.length() + " elements in " + (System.currentTimeMillis() - time) + " ms");
            }

            PageListNodeRef pageList = filter.filter(nodeService, resultSet, pagingDefinition);

            if (log.isDebugEnabled()) {
                log.debug("Filtered result size : " + pageList.getNodeRefs().size() + ", full execution time " + (System.currentTimeMillis() - time) + " ms");
            }

            return pageList;
        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
    }

    protected NodeRef getSingleNode(StringBuilder query) {
        return getSingleNode(query.toString());
    }

    protected NodeRef getSingleNode(String query) {
        if (log.isDebugEnabled()) {
            log.debug("Going to execute lucene query (expecting single node) : " + query);
        }

        ResultSet resultSet = null;

        try {
            SearchParameters searchParameters = SearchParametersFactory.create(query);
            resultSet = searchService.query(searchParameters);
            long numberFound = resultSet.getNumberFound();

            if (numberFound == 1) {
                NodeRef result = resultSet.getNodeRef(0);

                if (!nodeService.exists(result)) {
                    String errorMessage = "Found nodeRef, but it is already not exists : " + result;
                    log.error(errorMessage);
                    throw new NodeRefNotFoundException();
                }

                if (log.isDebugEnabled()) {
                    log.debug("Found nodeRef " + result);
                }

                return result;
            }

            String errorMessage = String.format("Query %s returned %d results, however exactly one result is expected", query, numberFound);
            log.error(errorMessage);
            throw numberFound == 0 ? new NodeRefNotFoundException() : new IllegalSearchResultSizeException();

        } finally {
            if (resultSet != null) {
                resultSet.close();
            }
        }
    }

    protected NodeRef getCompanyHome() {
        String query = "PATH:\"/app:company_home\"";
        return getSingleNode(query);
    }

    protected boolean hasAnyAspect(NodeRef nodeRef, QName... aspects) {
        if (log.isDebugEnabled()) {
            log.debug("Checking if node " + nodeRef + " has any of the aspects " + Arrays.toString(aspects));
        }

        Set<QName> nodeAspects = nodeService.getAspects(nodeRef);
        for (QName aspect : aspects) {
            if (nodeAspects.contains(aspect)) {
                log.debug("It has aspect "+aspect);
                return true;
            }
        }

        log.debug("Node has none of specified aspects");
        return false;
    }

    protected boolean hasAllAspects(NodeRef nodeRef, QName... aspects) {
        if (log.isDebugEnabled()) {
            log.debug("Checking if node " + nodeRef + " has all aspects " + Arrays.toString(aspects));
        }

        Set<QName> nodeAspects = nodeService.getAspects(nodeRef);
        return nodeAspects.containsAll(Arrays.asList(aspects));
    }

    protected boolean hasAllAspects(NodeRef nodeRef, List<QName> aspects) {
        if (log.isDebugEnabled()) {
            log.debug("Checking if node " + nodeRef + " has all aspects " + aspects);
        }

        Set<QName> nodeAspects = nodeService.getAspects(nodeRef);
        return nodeAspects.containsAll(aspects);
    }

    protected void assertNodeExists(NodeRef nodeRef) {
        if (!nodeService.exists(nodeRef)) {
            throw new NodeRefNotFoundException();
        }
    }

    protected NodeRef getRootNodeRef(String path) {
        return getRootNodeRef(path, true);
    }

    private NodeRef getRootNodeRef(String path, boolean createIfNotExist) {
        NodeRef rootNodeRef = getCompanyHome();
        if (StringUtils.isEmpty(path)) {
            return rootNodeRef;
        }
        String[] folders = path.split("/");
        for (String folder : folders) {
            if (StringUtils.isNotEmpty(folder)) {
                rootNodeRef = createIfNotExist ?
                        getOrCreateFolder(rootNodeRef, folder) :
                        getFolder(rootNodeRef, folder);
            }
        }

        if (log.isDebugEnabled()) {
            log.debug("Folder has been either located or created " + path);
        }
        return rootNodeRef;
    }

    protected NodeRef getExistingRootNodeRef(String path) {
        return getRootNodeRef(path, false);
    }

    private NodeRef getOrCreateFolder(final NodeRef parent, final String folderName) {
        final NodeRef resultFolder = fileFolderService.searchSimple(parent, folderName);
        return resultFolder == null ? createFolder(parent, folderName) : resultFolder;
    }

    private NodeRef getFolder(final NodeRef parent, final String folderName) {
        return fileFolderService.searchSimple(parent, folderName);
    }

    private NodeRef createFolder(final NodeRef parent, final String nodeName) {
        if (log.isDebugEnabled()) {
            log.debug("Going to create folder with name " + nodeName + " at " + nodeService.getPath(parent));
        }
        FileInfo fileInfo = fileFolderService.create(parent, nodeName, ContentModel.TYPE_FOLDER);
        return fileInfo.getNodeRef();
    }

    protected void fillAuditFields(Map<QName, Serializable> properties, Auditable auditable) {
        auditable.setCreator(ParseUtils.getString(properties, ContentModel.PROP_CREATOR));
        auditable.setCreated(ParseUtils.getFormattedDateTime(properties, ContentModel.PROP_CREATED));
        auditable.setModifier(ParseUtils.getString(properties, ContentModel.PROP_MODIFIER));
        auditable.setModified(ParseUtils.getFormattedDateTime(properties, ContentModel.PROP_MODIFIED));
    }

    protected String getPathByNodeRef(NodeRef nodeRef) {
        Path path = nodeService.getPath(nodeRef);
        return path.toPrefixString(namespaceService);
    }

    protected String getDisplayPathByNodeRef(NodeRef nodeRef) {
        Path path = nodeService.getPath(nodeRef);
        return path.toDisplayPath(nodeService, permissionService);
    }

    protected void doInTransaction(Runnable runnable) {
        RetryingTransactionHelper transactionHelper = transactionService.getRetryingTransactionHelper();
        transactionHelper.setMaxRetries(1);
        transactionHelper.doInTransaction(() -> {
            runnable.run();
            return null;
        });
    }

    protected void assertNodeIsAuditable(NodeRef nodeRef) {
        if (!hasAnyAspect(nodeRef, ContentModel.ASPECT_AUDITABLE)) {
            throw new IllegalArgumentException("NodeRef should be auditable");
        }
    }

    protected NodeRef getOneChildNodeRef(Collection<ChildAssociationRef> nodeRefs) {
        return CollectionUtils.isEmpty(nodeRefs) ? null : nodeRefs.iterator().next().getChildRef();
    }

    /**
     * Checks node statuses.
     *
     * @param nodeRef        node reference.
     * @param statusProperty property which contains status.
     * @param statuses       statuses.
     * @param enumType       the object of the enum type from which to return a constant.
     */
    @SafeVarargs
    protected final <E extends Enum<E>> boolean hasAnyState(NodeRef nodeRef, QName statusProperty, Class<E> enumType, E... statuses) {
        Serializable value = nodeService.getProperty(nodeRef, statusProperty);
        if (value == null) {
            return false;
        }

        E currentStatus = E.valueOf(enumType, value.toString());
        for (E status : statuses) {
            if (currentStatus == status) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates attachments folder.
     *
     * @param nodeRef the parent node.
     */
    protected void createAttachmentsFolder(NodeRef nodeRef) {
        fileFolderService.create(nodeRef, LifeupConst.FOLDER_ATTACHMENTS, ContentModel.TYPE_FOLDER);
    }

    protected <T> PageList<T> getPageList(ListRequest listRequest, Function<NodeRef, T> itemLoader) {
        PageListNodeRef listNodeRef = findNodeRefsByLuceneQuery(listRequest);
        PageList<T> resultPageList = new PageList<>(listNodeRef);
        listNodeRef.getNodeRefs().forEach(nodeRef -> resultPageList.getData().add(itemLoader.apply(nodeRef)));
        return resultPageList;
    }

    /**
     * Adds all permissions from source nodeRef to target nodeRef
     * @param sourceNodeRef
     * @param targetNodeRef
     */
    protected void inheritPermissions(NodeRef sourceNodeRef, NodeRef targetNodeRef) {
        AuthenticationUtil.runAsSystem(() -> {
            Set<AccessPermission> permissions = permissionService.getPermissions(sourceNodeRef);
            permissions.forEach(permission ->
                    permissionService.setPermission(targetNodeRef, permission.getAuthority(), permission.getPermission(), permission.getAccessStatus() == AccessStatus.ALLOWED));
            return null;
        });
    }
}
