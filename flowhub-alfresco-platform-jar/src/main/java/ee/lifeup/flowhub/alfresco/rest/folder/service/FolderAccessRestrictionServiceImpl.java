package ee.lifeup.flowhub.alfresco.rest.folder.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.folder.model.FolderAccessRestriction;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

/**
 * Created by Aleksandr Koltakov on 28.08.2018
 * based on DatasetAccessRestrictionServiceImpl
 */
@Service
public class FolderAccessRestrictionServiceImpl extends AbstractServiceImpl implements FolderAccessRestrictionService {

    @Override
    public FolderAccessRestriction get(NodeRef folderNodeRef) {
        log.info("Going to read folder access restrictions for folder " + folderNodeRef);
        Preconditions.checkNotNull(folderNodeRef, "folder nodeRef must not be null");
        assertNodeIsFolderAccessRestriction(folderNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(folderNodeRef);
        return getDatasetAccessRestriction(properties);
    }

    @Override
    public void update(NodeRef folderNodeRef, FolderAccessRestriction updateRequest) {
        log.info("Going to update folder access restrictions for folder " + folderNodeRef);
        Preconditions.checkNotNull(folderNodeRef, "folder nodeRef must not be null");
        Preconditions.checkNotNull(updateRequest, "folder access restrictions request must not be null");
        assertNodeIsFolderAccessRestriction(folderNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(folderNodeRef);
        addProperties(updateRequest, properties);
        nodeService.setProperties(folderNodeRef, properties);
    }

    private FolderAccessRestriction getDatasetAccessRestriction(Map<QName, Serializable> properties) {
        FolderAccessRestriction accessRestriction = new FolderAccessRestriction();
        accessRestriction.setFolderAccessRight(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_FOLDER_ACCESS_RIGHT));
        accessRestriction.setFolderAccessData(ParseUtils.getJsonNode(properties, LifeupContentModel.PROPERTY_FOLDER_ACCESS_DATA));
        return accessRestriction;
    }

    private void addProperties(FolderAccessRestriction accessRestriction, Map<QName, Serializable> properties) {
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ACCESS_RIGHT, accessRestriction.getFolderAccessRight());
        properties.put(LifeupContentModel.PROPERTY_FOLDER_ACCESS_DATA, accessRestriction.getFolderAccessData().toString());
    }

    private void assertNodeIsFolderAccessRestriction(NodeRef folderNodeRef) {
        assertNodeExists(folderNodeRef);
        Set<QName> nodeAspects = nodeService.getAspects(folderNodeRef);
        if (!nodeAspects.contains(LifeupContentModel.ASPECT_FOLDER)) {
            throw new IllegalArgumentException("NodeRef should be a folder");
        }

        if (!nodeAspects.contains(LifeupContentModel.ASPECT_FOLDER_ACCESS_RESTRICTION)) {
            nodeService.addAspect(folderNodeRef, LifeupContentModel.ASPECT_FOLDER_ACCESS_RESTRICTION, MapUtils.EMPTY_MAP);
        }
    }
}
