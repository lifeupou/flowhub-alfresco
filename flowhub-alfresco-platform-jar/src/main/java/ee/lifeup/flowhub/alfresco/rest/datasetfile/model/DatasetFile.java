/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.model;

import ee.lifeup.flowhub.alfresco.model.Auditable;

import java.util.Objects;

/**
 * Dataset file model.
 */
public class DatasetFile extends DatasetFileRequest implements Auditable {
    private String fileIdentifier;
    private String fileRenditionURI;
    private String fileRef;
    private String filePath;
    private String created;
    private String creator;
    private String modified;
    private String modifier;

    public String getCreated() {
        return created;
    }

    @Override
    public void setCreated(String created) {
        this.created = created;
    }

    public String getCreator() {
        return creator;
    }

    @Override
    public void setCreator(String creator) {
        this.creator = creator;
    }

    public String getModified() {
        return modified;
    }

    @Override
    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getModifier() {
        return modifier;
    }

    @Override
    public void setModifier(String modifier) {
        this.modifier = modifier;
    }

    public String getFileIdentifier() {
        return fileIdentifier;
    }

    public void setFileIdentifier(String fileIdentifier) {
        this.fileIdentifier = fileIdentifier;
    }

    public String getFileRenditionURI() {
        return fileRenditionURI;
    }

    public void setFileRenditionURI(String fileRenditionURI) {
        this.fileRenditionURI = fileRenditionURI;
    }

    public String getFileRef() {
        return fileRef;
    }

    public void setFileRef(String fileRef) {
        this.fileRef = fileRef;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DatasetFile)) return false;
        if (!super.equals(o)) return false;
        DatasetFile that = (DatasetFile) o;
        return Objects.equals(getFileIdentifier(), that.getFileIdentifier()) &&
                Objects.equals(getFileRenditionURI(), that.getFileRenditionURI()) &&
                Objects.equals(getFileRef(), that.getFileRef()) &&
                Objects.equals(getFilePath(), that.getFilePath()) &&
                Objects.equals(getCreated(), that.getCreated()) &&
                Objects.equals(getCreator(), that.getCreator()) &&
                Objects.equals(getModified(), that.getModified()) &&
                Objects.equals(getModifier(), that.getModifier());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFileIdentifier(), getFileRenditionURI(), getFileRef(), getFilePath(), getCreated(), getCreator(), getModified(), getModifier());
    }
}
