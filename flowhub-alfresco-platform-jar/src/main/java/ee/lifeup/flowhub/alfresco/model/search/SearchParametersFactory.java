/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.model.search;

import org.alfresco.service.cmr.repository.StoreRef;
import org.alfresco.service.cmr.search.LimitBy;
import org.alfresco.service.cmr.search.SearchParameters;
import org.alfresco.service.cmr.search.SearchService;

/**
 * Created by Aleksandr Koltakov on 10.07.2018
 */

public class SearchParametersFactory {
    private static final int SEARCH_LIMIT = 100000;

    public SearchParametersFactory() {
        //do not call me
    }

    private static SearchParameters createInternal() {
        SearchParameters searchParameters = new SearchParameters();
        searchParameters.addStore(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        searchParameters.setLanguage(SearchService.LANGUAGE_LUCENE);
        searchParameters.setLimit(SEARCH_LIMIT);

        searchParameters.setLimitBy(LimitBy.UNLIMITED);

        searchParameters.setMaxPermissionChecks(SEARCH_LIMIT);
        searchParameters.setMaxPermissionCheckTimeMillis(SEARCH_LIMIT);
        searchParameters.setMaxItems(-1);
        return searchParameters;
    }

    public static SearchParameters create() {
        return createInternal();
    }

    public static SearchParameters create(StringBuilder query) {
        return create(query.toString());
    }

    public static SearchParameters create(String query) {
        SearchParameters searchParameters = create();
        searchParameters.setQuery(query);
        return searchParameters;
    }


}
