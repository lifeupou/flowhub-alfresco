/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.utils;

import com.google.common.net.HttpHeaders;
import ee.lifeup.flowhub.alfresco.model.localization.LocalizedValue;
import org.alfresco.service.cmr.repository.MLText;
import org.alfresco.service.namespace.QName;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.extensions.webscripts.WebScriptRequest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

/**
 * Сontains utilitarian methods for localization.
 */
public class LocalizationUtils {
    public static final Locale DEFAULT_LOCALE = Locale.getDefault();
    private static final String PARAMETER_LANGUAGE = "lang";

    public static MLText createMultiLanguageText(LocalizedValue[] localizedValues) {
        MLText mlText = new MLText();
        Stream.of(localizedValues).forEach(localizedValue -> mlText.addValue(Locale.forLanguageTag(localizedValue.getLang()), localizedValue.getValue()));
        return mlText;
    }

    public static MLText createMultiLanguageText(Map<String, String> localizedValues) {
        MLText mlText = new MLText();
        localizedValues.forEach((locale, value) -> mlText.addValue(Locale.forLanguageTag(locale), value));
        return mlText;
    }

    public static LocalizedValue[] createLocalizedValues(MLText mlText) {
        List<LocalizedValue> values = new ArrayList<>(mlText.size());
        mlText.getLocales().forEach(locale -> {
            LocalizedValue localizedValue = new LocalizedValue();
            localizedValue.setLang(locale.getLanguage());
            localizedValue.setValue(mlText.getValue(locale));
            values.add(localizedValue);
        });

        return values.toArray(new LocalizedValue[]{});
    }

    public static String getLocalizedPropertyValue(Map<QName, Serializable> properties, QName qName) {
        return getLocalizedPropertyValue(properties, qName, DEFAULT_LOCALE);
    }

    public static String getLocalizedPropertyValue(Map<QName, Serializable> properties, QName qName, Locale locale) {
        MLText mlText = (MLText) properties.get(qName);
        return mlText == null ? null : mlText.get(locale);
    }

    public static Locale detectLocale(WebScriptRequest request) {
        String languageParameter = request.getParameter(PARAMETER_LANGUAGE);
        return StringUtils.isEmpty(languageParameter) ? parseLocaleFromHeader(request) : Locale.forLanguageTag(languageParameter);
    }

    public static Locale parseLocaleFromHeader(WebScriptRequest request) {
        String[] values = request.getHeaderValues(HttpHeaders.ACCEPT_LANGUAGE);
        if (values == null || values.length == 0) {
            return DEFAULT_LOCALE;
        }

        List<Locale.LanguageRange> languageRanges = Locale.LanguageRange.parse(values[0]);
        if (CollectionUtils.isEmpty(languageRanges)) {
            return DEFAULT_LOCALE;
        }

        Locale exactLocale = Locale.forLanguageTag(languageRanges.get(0).getRange());
        return Locale.forLanguageTag(exactLocale.getLanguage());
    }
}
