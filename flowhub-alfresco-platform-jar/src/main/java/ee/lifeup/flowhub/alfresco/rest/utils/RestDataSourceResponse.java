/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Response object suitable for YUI rest datasource
 * legacy!
 * @param <E>
 *            Element of the data array returned
 * legacy
 */
// NOSONAR
public class RestDataSourceResponse<E> {

    protected RestDataSourceResponseBody<List<E>> data;

    /**
     * Response with single data element
     *
     * @param items
     *            Data to be sent back
     */
    public RestDataSourceResponse(E items) {

        List<E> listData = new ArrayList<E>();
        listData.add(items);

        this.data = new RestDataSourceResponseBody<List<E>>();
        this.data.setItems(listData);
    }

    /**
     * Response with list data element
     *
     * @param items
     *            Data
     */
    public RestDataSourceResponse(List<E> items) {

        this.data = new RestDataSourceResponseBody<List<E>>();
        this.data.setItems(items);
    }

    /**
     * @return the response
     */
    public RestDataSourceResponseBody<List<E>> getData() {
        return data;
    }

    /**
     * @param data
     *            the response to set
     */
    public void setResponse(RestDataSourceResponseBody<List<E>> data) {
        this.data = data;
    }
}

/**
 * @param <T>
 *            response body type
 */
class PagedRestDataSourceResponseBody<T extends List<?>> extends RestDataSourceResponseBody<T> {

    protected int startRow;
    protected int endRow;
    protected int totalRows;

    /**
     * @return the startRow
     */
    public int getStartRow() {
        return startRow;
    }

    /**
     * @param startRow
     *            the startRow to set
     */
    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    /**
     * @return the endRow
     */
    public int getEndRow() {
        return endRow;
    }

    /**
     * @param endRow
     *            the endRow to set
     */
    public void setEndRow(int endRow) {
        this.endRow = endRow;
    }

    /**
     * @return the totalRows
     */
    public int getTotalRows() {
        return totalRows;
    }

    /**
     * @param totalRows
     *            the totalRows to set
     */
    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

}

/**
 * Rest data source for response body
 *
 * @param <T>
 *            Type parameter
 */
class RestDataSourceResponseBody<T extends List<?>> {

    protected int status;
    protected T items;

    /**
     * @return the status
     */
    public int getStatus() {
        return status;
    }

    /**
     * @param status
     *            the status to set
     */
    public void setStatus(int status) {
        this.status = status;
    }

    /**
     * @return the data
     */
    public T getItems() {
        return items;
    }

    /**
     * @param items
     *            the data to set
     */
    public void setItems(T items) {
        this.items = items;
    }
}