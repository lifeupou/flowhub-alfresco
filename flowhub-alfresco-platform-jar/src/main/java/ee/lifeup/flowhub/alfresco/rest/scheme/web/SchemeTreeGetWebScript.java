/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.exception.NodeRefNotFoundException;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.SchemeNodeDto;
import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.rest.utils.RestDataSourceResponse;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Web script for listing the Classification Schemes and contained folders in a tree structure.
 */
@Component("webscript.lifeup.flowhub.scheme.scheme.tree.get")
public class SchemeTreeGetWebScript extends AbstractBaseWebScript {

    @Autowired
    private SchemeService schemeService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) {
        try {
            NodeRef parentNodeRef = extractNodeRefFromRequest(request, LifeupConst.PARENT_NODE_REF_PARAMETER);
            NodeRef organizationNodeRef = extractNodeRefFromRequest(request, LifeupConst.ORGANIZATION_NODE_REF_PARAMETER);
            NodeRef rootSchemeNodeRef = schemeService.getRootSchemeNodeRef();

            boolean searchByOrganizationRef = parentNodeRef == null || parentNodeRef.equals(rootSchemeNodeRef);

            List<SchemeNodeDto> schemeNodesDto = searchByOrganizationRef ?
                    schemeService.getSchemeNodesByOrganization(organizationNodeRef) : schemeService.getSchemeNodes(parentNodeRef);
            writeResponse(response, new RestDataSourceResponse<>(schemeNodesDto), HttpStatus.SC_OK);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to read scheme, invalid nodeRef specified", e);
        } catch (NodeRefNotFoundException e) {
            throw new WebScriptException(HttpStatus.SC_NOT_FOUND, "Unable to get schemes tree, nodeRef not found", e);
        }
    }
}
