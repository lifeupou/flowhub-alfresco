/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.scheme.mapper;

import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.scheme.model.SchemeNodeDto;

import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.NodeService;
import org.alfresco.service.namespace.QName;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Mapper for scheme.
 */
@Component
public class SchemeMapper {

    @Autowired
    protected NodeService nodeService;

	public SchemeNodeDto mapToDto(NodeRef schemeNodeRef) {
		SchemeNodeDto schemeNodeDto = new SchemeNodeDto();
		schemeNodeDto.setNodeRef(schemeNodeRef.toString());
		schemeNodeDto.setTitle(createSchemeNodeTitle(schemeNodeRef));
		boolean hasChildren = false;
		// ensures folder has children of required types
		if (nodeService.countChildAssocs(schemeNodeRef, true) > 0) {
			List<ChildAssociationRef> children = nodeService.getChildAssocs(schemeNodeRef);
			for (ChildAssociationRef child : children) {
				if (nodeService.hasAspect(child.getChildRef(), LifeupContentModel.ASPECT_FOLDER)
						|| nodeService.hasAspect(child.getChildRef(), LifeupContentModel.ASPECT_SCHEME)) {
					hasChildren = true;
					break;
				}
			}
		}
		schemeNodeDto.setHasChildren(hasChildren);
		return schemeNodeDto;
	}

    /**
     * Create scheme node title.
     *
     * @param schemeNodeRef folder node reference.
     */
    private String createSchemeNodeTitle(NodeRef schemeNodeRef) {
        Map<QName, Serializable> properties = nodeService.getProperties(schemeNodeRef);
        String propertyItemId = (String) properties.get(LifeupContentModel.PROPERTY_SCHEME_ID);
        String propertyItemName = (String) properties.get(LifeupContentModel.PROPERTY_SCHEME_NAME);
        if (nodeService.hasAspect(schemeNodeRef, LifeupContentModel.ASPECT_FOLDER)) {
        	propertyItemId = (String) properties.get(LifeupContentModel.PROPERTY_FOLDER_ID);
        	propertyItemName = (String) properties.get(LifeupContentModel.PROPERTY_FOLDER_NAME);
        }
        return new StringBuilder()
                .append(propertyItemId)
                .append(" ")
                .append(propertyItemName)
                .toString();
    }
}
