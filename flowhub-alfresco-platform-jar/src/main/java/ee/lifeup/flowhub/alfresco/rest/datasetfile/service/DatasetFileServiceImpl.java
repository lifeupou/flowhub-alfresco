/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.service;

import com.google.common.base.Preconditions;
import ee.lifeup.flowhub.alfresco.model.LifeupContentModel;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.ContainerMetadata;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFile;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileRequest;
import ee.lifeup.flowhub.alfresco.service.AbstractServiceImpl;
import ee.lifeup.flowhub.alfresco.utils.ParseUtils;
import org.alfresco.model.ContentModel;
import org.alfresco.service.cmr.model.FileInfo;
import org.alfresco.service.cmr.repository.AssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.namespace.QName;
import org.alfresco.service.namespace.RegexQNamePattern;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.*;

/**
 * Implementation of dataset file service.
 */
@Component
public class DatasetFileServiceImpl extends AbstractServiceImpl implements DatasetFileService {

    @Override
    public NodeRef create(NodeRef datasetNodeRef, DatasetFileRequest createRequest) {
        log.info("We've got a case dataset file creation request");
        Preconditions.checkNotNull(createRequest, "dataset file request must not be null");
        Preconditions.checkNotNull(datasetNodeRef, "case dataset node reference must not be null");

        FileInfo datasetFileInfo = fileFolderService.create(datasetNodeRef, createRequest.getDatasetFileName(), ContentModel.TYPE_CONTENT);
        NodeRef datasetFileNodeRef = datasetFileInfo.getNodeRef();
        nodeService.createAssociation(datasetNodeRef, datasetFileNodeRef, LifeupContentModel.ASSOCIATION_CASE_DATASET_FILE);

        Map<QName, Serializable> properties = new HashMap<>();
        createOrUpdateProperties(createRequest, properties);

        addFileMetadataAspect(datasetFileNodeRef, createRequest);
        nodeService.addAspect(datasetFileNodeRef, LifeupContentModel.ASPECT_CASE_DATASET_FILE, properties);
        nodeService.addAspect(datasetFileNodeRef, ContentModel.ASPECT_AUDITABLE, Collections.emptyMap());
        return datasetFileNodeRef;
    }

    @Override
    public DatasetFile get(NodeRef fileNodeRef) {
        log.info("Going to read case dataset file " + fileNodeRef);
        Preconditions.checkNotNull(fileNodeRef, "dataset file node reference must not be null");

        assertNodeIsDatasetFile(fileNodeRef);
        Map<QName, Serializable> properties = nodeService.getProperties(fileNodeRef);
        return createDatasetFile(fileNodeRef, properties);
    }

    @Override
    public void update(NodeRef fileNodeRef, DatasetFileRequest updateRequest) {
        log.info("Going to update case dataset file " + fileNodeRef);
        Preconditions.checkNotNull(updateRequest, "dataset file request must not be null");
        Preconditions.checkNotNull(fileNodeRef, "dataset file node reference must not be null");
        assertNodeIsDatasetFile(fileNodeRef);

        Map<QName, Serializable> properties = nodeService.getProperties(fileNodeRef);
        createOrUpdateProperties(updateRequest, properties);
        nodeService.setProperties(fileNodeRef, properties);
    }

    @Override
    public List<DatasetFile> list(NodeRef datasetNodeRef) {
        log.info("Going to read files for dataset " + datasetNodeRef);
        Preconditions.checkNotNull(datasetNodeRef, "case dataset node reference must not be null");

        List<DatasetFile> datasetFiles = new ArrayList<>();
        List<AssociationRef> datasetFileAssocs = nodeService.getTargetAssocs(datasetNodeRef, RegexQNamePattern.MATCH_ALL);
        datasetFileAssocs.forEach(datasetFileAssoc -> {
            NodeRef datasetFileRef = datasetFileAssoc.getTargetRef();
            Map<QName, Serializable> properties = nodeService.getProperties(datasetFileRef);
            DatasetFile datasetFile = createDatasetFile(datasetFileRef, properties);
            datasetFiles.add(datasetFile);
        });
        return datasetFiles;
    }

    private void createOrUpdateProperties(DatasetFileRequest datasetFileRequest, Map<QName, Serializable> properties) {
        properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_FILE_TYPE, datasetFileRequest.getDatasetFileType());
        properties.put(ContentModel.PROP_NAME, datasetFileRequest.getDatasetFileName());
        properties.put(ContentModel.PROP_TITLE, datasetFileRequest.getDatasetFileTitle());
    }

    private DatasetFile createDatasetFile(NodeRef fileNodeRef, Map<QName, Serializable> properties) {
        DatasetFile datasetFile = new DatasetFile();
        datasetFile.setFileIdentifier(ParseUtils.getString(properties, ContentModel.PROP_NODE_UUID));
        datasetFile.setFileRef(fileNodeRef.toString());
        datasetFile.setFilePath(getDisplayPathByNodeRef(fileNodeRef));
        datasetFile.setDatasetFileName(ParseUtils.getString(properties, ContentModel.PROP_NAME));
        datasetFile.setDatasetFileTitle(ParseUtils.getString(properties, ContentModel.PROP_TITLE));
        datasetFile.setDatasetFileDescription(ParseUtils.getString(properties, ContentModel.PROP_DESCRIPTION));
        datasetFile.setDatasetFileType(ParseUtils.getString(properties, LifeupContentModel.PROPERTY_CASE_DATASET_FILE_TYPE));
        fillAuditFields(properties, datasetFile);
        return datasetFile;
    }

    private void addFileMetadataAspect(NodeRef datasetFileNodeRef, DatasetFileRequest datasetFileRequest) {
        ContainerMetadata datasetFileMetadata = datasetFileRequest.getDatasetFileMetadata();
        if (datasetFileMetadata != null) {
            List<String> fileNames = datasetFileRequest.getDatasetFileMetadata().getFileNames();
            List<ContainerMetadata.SignatureMetadata> signatureMetadata = datasetFileRequest.getDatasetFileMetadata().getSignatureMetadata();

            Map<QName, Serializable> properties = new HashMap<>();
            properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_FILE_BDOC_FILES, (Serializable) fileNames);
            properties.put(LifeupContentModel.PROPERTY_CASE_DATASET_FILE_BDOC_SIGNATURES,  ParseUtils.getString(signatureMetadata));
            nodeService.addAspect(datasetFileNodeRef, LifeupContentModel.ASPECT_CASE_DATASET_FILE_METADATA, properties);
        }
    }

    private void assertNodeIsDatasetFile(NodeRef fileNodeRef) {
        assertNodeExists(fileNodeRef);
        if (!hasAnyAspect(fileNodeRef, LifeupContentModel.ASPECT_CASE_DATASET_FILE)) {
            throw new IllegalArgumentException("NodeRef should be a case dataset file");
        }
    }

}
