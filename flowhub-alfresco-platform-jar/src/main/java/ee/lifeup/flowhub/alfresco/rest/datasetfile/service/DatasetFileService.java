/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.datasetfile.service;

import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFile;
import ee.lifeup.flowhub.alfresco.rest.datasetfile.model.DatasetFileRequest;
import org.alfresco.service.cmr.repository.NodeRef;

import java.util.List;

/**
 * Service for dataset files.
 */
public interface DatasetFileService {

    /**
     * Create dataset file.
     *
     * @param datasetNodeRef dataset node reference.
     * @param createRequest  dataset file request.
     */
    NodeRef create(NodeRef datasetNodeRef, DatasetFileRequest createRequest);

    /**
     * Return dataset file.
     *
     * @param datasetFileNodeRef dataset file node reference.
     */
    DatasetFile get(NodeRef datasetFileNodeRef);

    /**
     * Update dataset file.
     *
     * @param datasetFileNodeRef   dataset file node reference.
     * @param updateRequest dataset file request.
     */
    void update(NodeRef datasetFileNodeRef, DatasetFileRequest updateRequest);

    /**
     * Return list of dataset files.
     *
     * @param datasetNodeRef dataset node reference.
     */
    List<DatasetFile> list(NodeRef datasetNodeRef);
}
