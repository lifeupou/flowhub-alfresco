package ee.lifeup.flowhub.alfresco.rest.document.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.rest.document.service.DocumentService;
import ee.lifeup.flowhub.alfresco.rest.utils.AbstractBaseWebScript;
import ee.lifeup.flowhub.alfresco.rest.utils.WebParameter;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

/**
 * Created by Aleksandr Koltakov on 22.08.2018
 */
@Component("webscript.lifeup.flowhub.document.create.put")
public class DocumentCreationWebScript extends AbstractBaseWebScript {

    @Autowired
    private DocumentService documentService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        String documentProfileName = getParameter(request, LifeupConst.DOCUMENT_PROFILE_NAME_PARAMETER, WebParameter.MANDATORY);
        NodeRef caseInstanceNodeRef = new NodeRef(getParameter(request, LifeupConst.CASE_INSTANCE_NODE_REF_PARAMETER, WebParameter.MANDATORY));
        try {
            NodeRef datasetNodeRef = documentService.createDocument(documentProfileName, caseInstanceNodeRef);
            writeResponse(response, datasetNodeRef.toString());
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "Unable to create document, invalid data specified", e);
        }
    }

}
