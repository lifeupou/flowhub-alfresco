/*
 * #%L
 * Flowhub extension
 * %%
 * Copyright (C) 2018 LifeUp OÜ
 * %%
 * This file is part of the Flowhub software.
 * If the software was purchased under a paid Flowhub license, the terms of
 * the paid license agreement will prevail.  Otherwise, the software is
 * provided under the following open source license terms:
 *
 * Flowhub is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Flowhub is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with Flowhub. If not, see <http://www.gnu.org/licenses/>.
 * #L%
 */

package ee.lifeup.flowhub.alfresco.rest.filetemplate.web;

import ee.lifeup.flowhub.alfresco.consts.LifeupConst;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.model.FileTemplateRequest;
import ee.lifeup.flowhub.alfresco.rest.filetemplate.service.FileTemplateService;
import ee.lifeup.flowhub.alfresco.service.FileUploadService;
import org.alfresco.service.cmr.model.FileExistsException;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.httpclient.HttpStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.extensions.surf.util.Content;
import org.springframework.extensions.webscripts.WebScriptException;
import org.springframework.extensions.webscripts.WebScriptRequest;
import org.springframework.extensions.webscripts.WebScriptResponse;
import org.springframework.stereotype.Component;

@Component("webscript.lifeup.flowhub.filetemplate.filetemplate.post")
public class FileTemplateCreateWebScript extends FileTemplateBaseWebScript {


    @Autowired
    private FileTemplateService fileTemplateService;

    @Autowired
    private FileUploadService fileUploadService;

    @Override
    protected void executeImpl(WebScriptRequest request, WebScriptResponse response) throws Exception {
        try {
            FileTemplateRequest fileTemplateRequest = createFileTemplateRequest(request);
            NodeRef fileTemplateNodeRef = fileTemplateService.createFileTemplate(fileTemplateRequest);
            Content content = getContentFromRequest(request, LifeupConst.FILE_DATA_PARAMETER);
            fileUploadService.uploadFileToNodeRef(
                    fileTemplateNodeRef,
                    fileTemplateRequest.getName(),
                    content.getInputStream());
            writeResponse(response, fileTemplateNodeRef.toString(), HttpStatus.SC_CREATED);
        } catch (IllegalArgumentException e) {
            throw new WebScriptException(HttpStatus.SC_BAD_REQUEST, "Unable to create file template, invalid data specified", e);
        } catch (FileExistsException e) {
            throw new WebScriptException(HttpStatus.SC_CONFLICT, "Unable to create file template, duplicate name", e);
        }
    }
}