::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
::      Dev environment startup script for Alfresco Community     ::
::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
@ECHO OFF

IF "%MAVEN_OPTS%" == "" (
    ECHO The environment variable 'MAVEN_OPTS' is not set, setting it for you
    SET MAVEN_OPTS=-Xms400m -Xmx4G -Xdebug -Xrunjdwp:transport=dt_socket,address=8000,server=y,suspend=n
)
ECHO MAVEN_OPTS is set to '%MAVEN_OPTS%'

mvn clean install alfresco:run -Dmaven.test.skip=true