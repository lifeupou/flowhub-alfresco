package ee.lifeup.flowhub.alfresco.rest;

import ee.lifeup.flowhub.alfresco.rest.category.service.FlowhubCategoryService;
import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import org.alfresco.rad.test.AbstractAlfrescoIT;
import org.alfresco.rad.test.AlfrescoTestRunner;
import org.alfresco.service.cmr.repository.ChildAssociationRef;
import org.alfresco.service.cmr.repository.NodeRef;
import org.apache.commons.codec.binary.Base64;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by Aleksandr Koltakov on 04.07.2018
 */
@RunWith(value = AlfrescoTestRunner.class)
public abstract class AbstractWebScriptTest extends AbstractAlfrescoIT {
    private static final String WEB_SERVICE_URL = "http://localhost:8080/alfresco/service/";

    private RestTemplate restTemplate;

    @Before
    public void before() {
        restTemplate = new RestTemplate();
    }

    protected ResponseEntity doGet(String requestUrl, Class<?> clazz) throws Exception {
        return restTemplate.exchange(WEB_SERVICE_URL + requestUrl, HttpMethod.GET, new HttpEntity(createAuthorizationHeaders()), clazz);
    }

    protected HttpHeaders createAuthorizationHeaders(){
        return new HttpHeaders() {{
            String auth = "admin:admin";
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes());
            String authHeader = "Basic " + new String(encodedAuth);
            set("Authorization", authHeader );
        }};
    }

    protected void deleteAllCategories() {
        FlowhubCategoryService flowhubCategoryService = (FlowhubCategoryService) getApplicationContext().getBean("flowhubCategoryServiceImpl");
        NodeRef rootCategoryNodeRef = flowhubCategoryService.getRootCategoryNodeRef();
        List<ChildAssociationRef> children =  getServiceRegistry().getNodeService().getChildAssocs(rootCategoryNodeRef);
        for (ChildAssociationRef child : children) {
            try {
            	flowhubCategoryService.delete(child.getChildRef());
            } catch (Exception e) {
                //skip it silently
            }

        }

    }

    protected void deleteAllSchemes() {
        SchemeService schemeService = (SchemeService) getApplicationContext().getBean("schemeService");
//        NodeRef rootSchemeNodeRef = schemeService.getRootSchemeNodeRef();
//        getServiceRegistry().getNodeService().deleteNode(rootSchemeNodeRef);
    }
}
