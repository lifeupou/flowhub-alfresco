package ee.lifeup.flowhub.alfresco.rest;

import ee.lifeup.flowhub.alfresco.rest.scheme.service.SchemeService;
import org.junit.After;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Aleksandr Koltakov on 06.07.2018
 */

public class SchemeWebScriptIT extends AbstractWebScriptTest {

    @Autowired
    private SchemeService schemeService;

    @Test
    public void testCreateIncorrect() {

    }

    @After
    public void after() {
        deleteAllSchemes();
        deleteAllCategories();
    }
}
