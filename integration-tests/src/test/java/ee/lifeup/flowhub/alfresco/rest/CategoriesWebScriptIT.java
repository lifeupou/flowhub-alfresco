package ee.lifeup.flowhub.alfresco.rest;


import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ee.lifeup.flowhub.alfresco.rest.category.model.LocalizedNode;
import org.alfresco.service.cmr.repository.NodeRef;
import org.alfresco.service.cmr.repository.StoreRef;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;

import java.util.List;

/**
 * Created by Aleksandr Koltakov on 04.07.2018
 */

public class CategoriesWebScriptIT extends AbstractWebScriptTest {
    private static final String CATEGORIES_URL = "flowhub/categories";

    @After
    public void after() {
        deleteAllCategories();
    }

    @Test
    public void testGetEmptyCategories() throws Exception {
        ResponseEntity<String> responseEntity = doGet(CATEGORIES_URL, String.class);
        Assert.assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        //We have to do this magic, because RestTemplate do not know how to deserialize RestDataSourceResponse :(
        String json = responseEntity.getBody();
        Assert.assertTrue(StringUtils.isNotEmpty(json));

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(responseEntity.getBody());
        JsonNode items = root.path("data").path("items");
        List<LocalizedNode> nodes = mapper.treeToValue(items, List.class);

        Assert.assertNotNull(nodes);
        Assert.assertTrue(nodes.isEmpty());
    }

    @Test
    public void testGetCategoriesUnknownNodeRef() throws Exception {
        String nonExistingNodeRef = "workspace://SpacesStore/64f28c8e-66xx-46yy-a529-32a388ad42dc";
        try {
            doGet(CATEGORIES_URL + "?nodeRef=" + nonExistingNodeRef, Object.class);
            Assert.fail("We should not be here");
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            Assert.assertEquals(HttpStatus.NOT_FOUND, e.getStatusCode());
        }

    }

    @Test
    public void testGetCategoriesIllegalParentNodeRef() throws Exception {
        NodeRef rootNode = getServiceRegistry().getNodeService().getRootNode(StoreRef.STORE_REF_WORKSPACE_SPACESSTORE);
        try {
            doGet(CATEGORIES_URL + "?nodeRef=" + rootNode, Object.class);
        } catch (HttpClientErrorException | HttpServerErrorException e) {
            Assert.assertEquals(HttpStatus.CONFLICT, e.getStatusCode());
        }
    }
}
